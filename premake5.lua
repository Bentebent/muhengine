solution "MUHENGINE"
	configurations { "Debug", "Release" }
		flags{ "Unicode", "NoPCH" }
		libdirs { "lib" }
		includedirs { "include" }
		
		local location_path = "solution"
		
		if _ACTION then
	        defines { "_CRT_SECURE_NO_WARNINGS", "NOMINMAX" }

            location_path = location_path .. "/" .. _ACTION
            location ( location_path )
            location_path = location_path .. "/projects"
        end
		
		defines { "GENERATE_LUA_API_FILE"}
		
		configuration { "Debug" }
        defines { "DEBUG" }
        flags { "Symbols" }
		architecture "x64"
        
		configuration { "Release" }
        defines { "NDEBUG", "RELEASE" }
        flags { "Optimize", "FloatFast" }
		architecture "x64"

		
		configuration { "Debug" }
        targetdir ( "bin/" .. "/debug" )

		configuration { "Release" } 
        targetdir ( "bin/" .. "/release" )   
		
		project "MUHENGINE"
				targetname "MUHENGINE"
				debugdir ""
				location (location_path)
				language "C++"
				kind "ConsoleApp"
				files { "src/core/**.cpp", "src/core/**.hpp", "src/core/**.h", "src/shared/**.cpp", "src/shared/**.hpp", "src/shared/**.h", "scripts/**.lua"}
				includedirs { "src/core", "include", "src/shared" }
				links{ "lua51", "zlibwapi", "gfx" }
				
				configuration {"Debug"}
						links { "sfml-audio-d", "sfml-graphics-d", "sfml-main-d", "sfml-network-d", "sfml-system-d", "sfml-window-d"}
						
				configuration {"Release"}
						links {"sfml-audio", "sfml-graphics", "sfml-main", "sfml-network", "sfml-system", "sfml-window"}
						
		project "gfx"
				location ( location_path )
				language "C++"
				kind "SharedLib"
				files { "src/gfx/**.hpp", "src/gfx/**.h", "src/gfx/**.cpp", "src/shared/**.cpp", "src/shared/**.hpp", "src/shared/**.h", "shaders/**.glsl" }
				includedirs { "src/gfx", "include", "src/shared" }
				defines { "GFX_DLL_EXPORT" }
				links { }

				configuration {"Debug"}
						links { "sfml-audio-d", "sfml-graphics-d", "sfml-main-d", "sfml-network-d", "sfml-system-d", "sfml-window-d"}
						
				configuration {"Release"}
						links {"sfml-audio", "sfml-graphics", "sfml-main", "sfml-network", "sfml-system", "sfml-window"}