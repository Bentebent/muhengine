#include <iostream>
#include <Utility/WindowHandler.hpp>
#include <Utility/WindowModes.hpp>
#include <Utility/InputHandler.hpp>
#include <World.hpp>
#include <Utility/Timer.hpp>
#include <Utility/StringUtility.hpp>

#include <gfx/GFXInterface.hpp>

#ifdef _WIN64
	#include <Windows.h>
#elif _WIN32
	#include <Windows.h>
#endif

#include <lua/lua.hpp>
#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>

#include <bitmask/bitmask_system.hpp>
#include <bitmask/bitmask_types.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>

#include <ContentManagement/Loaders/TextureLoader.hpp>
#include <ContentManagement/Loaders/FontLoader.hpp>
#include <ContentManagement/Loaders/AnimationLoader.hpp>

#include <ContentManagement/ContentManager.hpp>

#include <Animation/AnimationManager.hpp>

#include <Map/TiledParser.hpp>
#include <Map/TiledMap.hpp>
int main()
{
	bool running = true;
	Core::world.InitWorld();
	Core::world.m_luaCore->Initialize();

	Core::InputHandler* inputHandler = Core::world.m_constantHeap.NewObject<Core::InputHandler>();// new Core::InputHandler();
	Core::WindowHandler* windowHandler = Core::world.m_constantHeap.NewObject<Core::WindowHandler>();// new Core::WindowHandler();
	Core::world.m_inputHandler = inputHandler;
	Core::world.m_windowHandler = windowHandler;

	int resX = 1024;
	int resY = 768;

	windowHandler->Initialize(resX, resY, "MUHENGINE", inputHandler);

	gfx::Initialize(resX, resY, (void*)windowHandler->GetWindow());

	Core::HighresTimer hrTimer;

	long long lastFrameTime = hrTimer.GetTotal();
	long long thisFrame = hrTimer.GetTotal();
	double timeAccumulator = 0.0;
	const double MAXIMUM_TIME_STEP = 1.0 / 15.0;
	const double MINIMUM_TIME_STEP = 1.0 / 120.0;

	const int FPS_COUNTERS_SIZE = 20;
	double fpsCounters[FPS_COUNTERS_SIZE];
	int fpsCounterIndex = 0;
	double averageFPS = 0;

	//myBitmask = 0
	//myBitmask = SetBitmaskValue(myBitmask, 0, 0)
	//myBitmask = SetBitmaskValue(myBitmask, 2, 5)
	//myBitmask = SetBitmaskValue(myBitmask, 1, 1)
	//myBitmask = SetBitmaskValue(myBitmask, 3, paddleTexture)


	//DEBUG
	//Core::TiledParser* tp = new Core::TiledParser();
	//Core::TiledMap* map = tp->LoadMap("content/maps/mockup/mockup_one.tmx");
	//
	//int tx, ty;
	//
	//map->GetTilePosition(0, 0, tx, ty);
	//map->GetTilePosition(16, 16, tx, ty);
	//
	//map->GetTilePosition(32, 16, tx, ty);
	//
	//std::cout << tx << ty << std::endl;

	//Core::TiledMap* map = tp->LoadMap("content/maps/overheadmap/layer_properties.tmx");

	//sf::View v;
	//v.setCenter(320, 240);
	//v.setSize(640, 480);
	//gfx::view::SetView((void*)&v, 0);
	//Core::contentManager.Load("content/animations/sheetTest.anim", Core::ContentLoadPolicy::BLOCKING);
	//
	//Core::AnimationData* ad = (Core::AnimationData*)Core::contentManager.GetContentData("content/animations/sheetTest.anim");
	//Core::contentManager.Free("content/animations/sheetTest.anim");

	//Core::RenderingComponent rc;
	//Core::TransformationComponent tc;
	//Core::AnimationComponent ac;
	//
	//rc.bitmask = 0;
	//rc.bitmask = Core::world.m_bitmaskSystem.Encode(rc.bitmask, gfx::COMMAND_TYPES::RENDER, MaskType::COMMAND);
	//rc.bitmask = Core::world.m_bitmaskSystem.Encode(rc.bitmask, gfx::OBJECT_TYPES::SPRITE, MaskType::TYPE);
	//rc.bitmask = Core::world.m_bitmaskSystem.Encode(rc.bitmask, ad->textureID, MaskType::TEXTURE);
	//
	//rc.origin[0] = 101;
	//rc.origin[1] = 136;
	//
	//tc.position[0] = 320;
	//tc.position[1] = 240;
	//
	//ac.loop = false;
	//ac.speed = 2;
	//Core::Entity e = Core::world.m_entityHandler.CreateLevelEntity(rc, tc, ac);
	//Core::AnimationManagerInstance().PlayAnimation(e, "greyguy_crouch_down", false, true);
	//Core::AnimationManagerInstance().QueueAnimation(e, "greyguy_walk", true, false);

	//Core::world.m_luaCore->LoadFile("scripts/Test/BridgeTest.lua", true);

	std::string current = "scripts/mockup/init_mockup.lua";
	std::string currentMap = "content/maps/mockup/mixels.tmx";

	Core::world.m_luaCore->LoadFile(current, true);

	//Core::world.m_luaCore->LoadFile("scripts/userdatatest.lua", true);
	//Core::world.m_map = Core::world.m_tiledParser->LoadMap(currentMap);
	

	while (running)
	{
		if (inputHandler->SingleButtonPress(sf::Keyboard::Escape))
			break;

		int windowResult =  windowHandler->WindowEvents();

		if (windowResult == WINDOW_CLOSED)
			break;
		//else if (windowResult == WINDOW_LOST_FOCUS)
		//	continue;

		// calc delta time
		thisFrame		= hrTimer.GetTotal();
		double delta	= (thisFrame - lastFrameTime) / 1000.0;
		lastFrameTime	= thisFrame;

		//This is actually a setting in Kravall
		double minimumTimeStep = MINIMUM_TIME_STEP;
		double maximumTimeStep = MAXIMUM_TIME_STEP;

		timeAccumulator += delta;

		if (timeAccumulator > maximumTimeStep)
		{
			timeAccumulator = maximumTimeStep;
		}

		if (timeAccumulator > minimumTimeStep)
		{
			Core::world.m_systemHandler.Update(static_cast<float>(timeAccumulator));

			//sf::FloatRect rect = sf::FloatRect(926, 1, 48, 48);
			//
			//Core::world.m_collisionHandler->MapCollision(rect, Core::world.m_map->GetCollisionLayer(), -1, 1);
			//
			//gfx::debug::RenderRectangle(rect.left, rect.top, rect.width, rect.height, 155, 155, 155, 100);

			//gfx::debug::RenderRectangle(900, rect.top, 8, 8, 0, 0, 255, 100);

			////DEBUG
			//if (inputHandler->SingleButtonPress(sf::Keyboard::Return))
			//{
			//	//Core::AnimationManagerInstance().PauseAnimation(e);
			//	
			//}
			
			//if (inputHandler->SingleButtonPress(sf::Keyboard::BackSpace))
			//{
			//	Core::AnimationManagerInstance().ResumeAnimation(e);
			//}
			 
			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::R))
			{
				Core::contentManager.Reload();
				Core::world.m_entityHandler.ClearEntities();
				Core::world.m_luaCore->ClearChildren();
				Core::world.m_luaCore->LoadFile(current, true);

				if (currentMap.length() > 0)
					Core::world.m_tiledParser->LoadMap(currentMap, Core::world.m_map);

				std::cout << "Reloaded map" << std::endl;
			}

			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::W))
			{
				if (currentMap.length() > 0)
					Core::world.m_tiledParser->LoadMap(currentMap, Core::world.m_map);
			}

			/*
			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::Num0))
			{
				current = "scripts/Test/BridgeTest.lua";
				currentMap = "";
				Core::world.m_entityHandler.ClearEntities();
				Core::world.m_luaCore->ClearChildren();
				Core::world.m_luaCore->LoadFile(current, true);

				Core::world.m_levelHeap.Rewind();
			}

			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::Num1))
			{
				current = "scripts/arcadeclones/asteroids/LoadAsteroids.lua";
				currentMap = "";

				Core::contentManager.Reload();
				Core::world.m_entityHandler.ClearEntities();
				Core::world.m_luaCore->ClearChildren();
				Core::world.m_luaCore->LoadFile(current, true);
				Core::world.m_levelHeap.Rewind();
			}

			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::Num2))
			{
				current = "scripts/arcadeclones/breakout/LoadBreakout.lua";
				currentMap = "";

				Core::contentManager.Reload();
				Core::world.m_entityHandler.ClearEntities();
				Core::world.m_luaCore->ClearChildren();
				Core::world.m_luaCore->LoadFile(current, true);
				Core::world.m_levelHeap.Rewind();
			}
			 */

			if (inputHandler->SingleButtonPress(sf::Keyboard::Key::Num3))
			{
				current = "scripts/mockup/init_mockup.lua";
				currentMap = "content/maps/mockup/mockup_one.tmx";
				Core::contentManager.Reload();
				Core::world.m_entityHandler.ClearEntities();
				Core::world.m_luaCore->ClearChildren();
				Core::world.m_luaCore->LoadFile(current, true);
				Core::world.m_tiledParser->LoadMap(currentMap, Core::world.m_map);
			}
			 
			///END DEBUG

			//Render yo shit
			windowHandler->Clear(sf::Color::Black);

			///DEBUG
			int height = 0;
			std::vector<std::pair<const char*, std::chrono::microseconds>> systemTimes = Core::world.m_systemHandler.GetFrameTime();
			for (int i = 0; i < systemTimes.size(); i++)
			{
				std::string systemTime = systemTimes[i].first;
				systemTime.append(": ");
				systemTime.append(Core::ToString<double>(systemTimes[i].second.count() / 1000.0f));
				systemTime.append(" ms");
				gfx::debug::RenderText(0, resY - i * 13, systemTime.c_str(), 12, 255, 255, 255, 255);
				height = -i * 13;
			}

			gfx::debug::RenderRectangle(0, resY + height, 175, -height, 147, 147, 147, 147, true);

			std::string fpsString = Core::ToString<double>(averageFPS);

			if (fpsString.size() > 5)
				fpsString.resize(fpsString.size() - 2);
			fpsString = "FPS: " + fpsString;
			gfx::debug::RenderText(resX - 60, 0, fpsString.c_str(), 12, 255, 255, 255, 255);
			///END DEBUG

			gfx::Render(timeAccumulator);

			windowHandler->SwapBuffers();

			Core::contentManager.HandleEvents();

			timeAccumulator = 0.0;
			Core::world.m_frameHeap.Rewind();
		}

		fpsCounters[fpsCounterIndex++] = 1.0 / delta;
		if (fpsCounterIndex == FPS_COUNTERS_SIZE)
		{
			averageFPS = 0.0;
			for (int i = 0; i < FPS_COUNTERS_SIZE; ++i)
			{
				averageFPS += fpsCounters[i];
			}

			averageFPS /= FPS_COUNTERS_SIZE;
			fpsCounterIndex = 0;
		}
	}

	gfx::Destroy();

	std::cout << "Goodbye world!" << std::endl;
    return 0;
}