#include "World.hpp"

#define WORLD_MEMORY_SIZE 250000000

#define WORLD_CONSTANT_MEMORY_START_OFFSET	0
#define WORLD_CONSTANT_MEMORY_SIZE			1000000

#define WORLD_LEVEL_MEMORY_START_OFFSET		WORLD_CONSTANT_MEMORY_SIZE
#define WORLD_LEVEL_MEMORY_SIZE				200000000

#define WORLD_FRAME_MEMORY_START_OFFSET		WORLD_CONSTANT_MEMORY_SIZE + WORLD_LEVEL_MEMORY_SIZE
#define WORLD_FRAME_MEMORY_SIZE				(WORLD_MEMORY_SIZE - WORLD_CONSTANT_MEMORY_SIZE - WORLD_LEVEL_MEMORY_SIZE)

namespace Core
{
    World world;

    World::~World()
    {
        delete[] m_worldMemory;
    }

    void World::InitWorld()
    {
        m_worldMemory	= new unsigned char[WORLD_MEMORY_SIZE];

		// constant memory
		m_constantAllocator = LinearAllocator(Core::world.m_worldMemory + WORLD_CONSTANT_MEMORY_START_OFFSET, WORLD_CONSTANT_MEMORY_SIZE);
		m_constantHeap = LinearHeap(Core::world.m_constantAllocator);

		// initial level memory
		m_levelAllocator = LinearAllocator(Core::world.m_worldMemory + WORLD_LEVEL_MEMORY_START_OFFSET, WORLD_LEVEL_MEMORY_SIZE);
		m_levelHeap = LinearHeap(Core::world.m_levelAllocator);

		// frame memory
		m_frameAllocator = Core::LinearAllocator(Core::world.m_worldMemory + WORLD_FRAME_MEMORY_START_OFFSET, WORLD_FRAME_MEMORY_SIZE);
		m_frameHeap = Core::LinearHeap(Core::world.m_frameAllocator);


		m_luaCore	= m_constantHeap.NewObject<LuaCore>();

		//bitmask system
		m_bitmaskSystem.Init();

		//Map
		m_tiledParser = m_constantHeap.NewObject<TiledParser>();

		//Collision
		m_collisionHandler = m_constantHeap.NewObject<CollisionHandler>();
    }
}