#ifndef SRC_CORE_MEMORY_FINALIZER_HPP
#define SRC_CORE_MEMORY_FINALIZER_HPP

#include <functional>

namespace Core
{
	struct Finalizer
	{
		void(*m_destructorCall)(void* poiner);
		Finalizer* m_finalizerChain;
		void* m_dataPointer;
	};

	template<typename T>
	void DestructorCall(void* pointer)
	{
		static_cast<T*>(pointer)->~T();
	}
}

#endif