#ifndef SRC_CORE_SYSTEMDEFS_HPP
#define SRC_CORE_SYSTEMDEFS_HPP

#include <ComponentFramework/EntityHandlerTemplate.hpp>
#include <ComponentFramework/SystemHandlerTemplate.hpp>

/* ALL COMPONENTS GO HERE */
#include <Components/TestComponent.hpp>
#include <Components/ScriptComponent.hpp>
#include <Components/TransformationComponent.hpp>
#include <Components/RenderingComponent.hpp>
#include <Components/TextComponent.hpp>
#include <Components/ShapeComponent.hpp>
#include <Components/AnimationComponent.hpp>
#include <Components/CollisionComponent.hpp>

/* ALL SYSTEMS GO HERE */
#include <Systems/TestSystem.hpp>
#include <Systems/ScriptSystem.hpp>
#include <Systems/RenderingSystem.hpp>
#include <Systems/TextSystem.hpp>
#include <Systems/ShapeSystem.hpp>
#include <Systems/AnimationSystem.hpp>

namespace Core
{
    typedef SystemHandlerTemplate<TestSystem, ScriptSystem, AnimationSystem, RenderingSystem, TextSystem, ShapeSystem> SystemHandler;

    typedef EntityHandlerTemplate<SystemHandler, TestComponent, ScriptComponent, TransformationComponent, RenderingComponent, TextComponent, ShapeComponent, AnimationComponent, CollisionComponent> EntityHandler;
}

#endif //SRC_CORE_SYSTEMDEFS_HPP
