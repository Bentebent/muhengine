#ifndef SRC_CORE_ANIMATION_ANIMATIONDATA_HPP
#define SRC_CORE_ANIMATION_ANIMATIONDATA_HPP

#include <vector>

namespace Core
{
	struct AnimationFrame
	{
		unsigned int id;
		unsigned int time;
		unsigned int x;
		unsigned int y;
		unsigned int width;
		unsigned int height;
	};

	struct Animation
	{
		unsigned int id;
		unsigned int textureID;

		float playTime;

		std::string name;
		std::vector<AnimationFrame> frames;
	};
}

#endif