#include "AnimationManager.hpp"

#include <World.hpp>
#include <SystemDefs.hpp>

#include <iostream>

namespace Core
{
	AnimationManager& AnimationManagerInstance()
	{
		static AnimationManager am;
		return am;
	}

	void AnimationManager::PlayAnimation(const Entity& entity, std::string name, bool looped, bool reverse)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		RenderingComponent* rc = WGETC<RenderingComponent>(entity);

		int animationID = GetAnimationID(name);

		ac->animationID		= animationID;
		ac->currentFrame	= 0;
		ac->currentTime		= 0;
		ac->playing			= true;
		ac->loop			= looped;
		ac->reversed		= reverse;

		if (ac->reversed)
			ac->currentFrame = m_animations[animationID].frames.size() - 1;

		ac->playTime = m_animations[animationID].frames[ac->currentFrame].time;


		rc->sourceRectangle[0] = m_animations[animationID].frames[ac->currentFrame].x;
		rc->sourceRectangle[1] = m_animations[animationID].frames[ac->currentFrame].y;
		rc->sourceRectangle[2] = m_animations[animationID].frames[ac->currentFrame].width;
		rc->sourceRectangle[3] = m_animations[animationID].frames[ac->currentFrame].height;

		rc->origin[0] = rc->sourceRectangle[2] / 2.0f;
		rc->origin[1] = rc->sourceRectangle[3] / 2.0f;
	}

	void AnimationManager::PlayAnimation(const Entity& entity, unsigned int id)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		RenderingComponent* rc = WGETC<RenderingComponent>(entity);

		ac->queuedAnimationID = -1;
		ac->animationID = id;
		ac->currentFrame = 0;
		ac->currentTime = 0;
		ac->playing = true;
		
		ac->loop = ac->loopFuture;
		ac->reversed = ac->reversedFuture;
		ac->loopFuture = false;

		if (ac->reversed)
			ac->currentFrame = m_animations[id].frames.size() - 1;

		ac->playTime = m_animations[id].frames[ac->currentFrame].time;

		rc->sourceRectangle[0] = m_animations[id].frames[0].x;
		rc->sourceRectangle[1] = m_animations[id].frames[0].y;
		rc->sourceRectangle[2] = m_animations[id].frames[0].width;
		rc->sourceRectangle[3] = m_animations[id].frames[0].height;

		rc->origin[0] = rc->sourceRectangle[2] / 2.0f;
		rc->origin[1] = rc->sourceRectangle[3] / 2.0f;
	}

	bool AnimationManager::IsAnimationPlaying(const Entity& entity, std::string name)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		int animationId = GetAnimationID(name);

		if (ac->animationID == animationId)
			return true;
		return false;
	}

	void AnimationManager::QueueAnimation(const Entity& entity, std::string name, bool loop, bool reverse)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		ac->queuedAnimationID = GetAnimationID(name);
		ac->loopFuture = loop;
		ac->reversedFuture = reverse;
	}

	void AnimationManager::StepAnimation(const Entity& entity)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		RenderingComponent* rc = WGETC<RenderingComponent>(entity);

		if (ac->reversed)
			ac->currentFrame--;
		else
			ac->currentFrame++;

		if (ac->currentFrame >= m_animations[ac->animationID].frames.size() || ac->currentFrame < 0)
		{
			if (ac->queuedAnimationID != -1)
				PlayAnimation(entity, ac->queuedAnimationID);
			else if (ac->loop)
			{
				if (ac->reversed)
					ac->currentFrame = m_animations[ac->animationID].frames.size() - 1;
				else
					ac->currentFrame = 0;
			}
			else
			{
				ac->playing = false;
				return;
			}
		}

		ac->currentTime = 0;
		ac->playTime = m_animations[ac->animationID].frames[ac->currentFrame].time;

		rc->sourceRectangle[0] = m_animations[ac->animationID].frames[ac->currentFrame].x;
		rc->sourceRectangle[1] = m_animations[ac->animationID].frames[ac->currentFrame].y;
		rc->sourceRectangle[2] = m_animations[ac->animationID].frames[ac->currentFrame].width;
		rc->sourceRectangle[3] = m_animations[ac->animationID].frames[ac->currentFrame].height;

		rc->origin[0] = rc->sourceRectangle[2] / 2.0f;
		rc->origin[1] = rc->sourceRectangle[3] / 2.0f;
	}

	unsigned int AnimationManager::AddAnimation(Animation anim)
	{
		unsigned int animID = m_animationCounter++;
		m_animations.emplace(animID, anim);

		return animID;
	}

	void AnimationManager::RemoveAnimation(unsigned int id)
	{
		m_animations.erase(id);
	}

	void AnimationManager::ClearAnimations()
	{
		m_animations.clear();
		m_animationCounter = 0;
	}

	void AnimationManager::PauseAnimation(const Entity& entity)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		ac->playing = false;
	}

	void AnimationManager::ResumeAnimation(const Entity& entity)
	{
		AnimationComponent* ac = WGETC<AnimationComponent>(entity);
		ac->playing = true;
	}


	AnimationManager::AnimationManager()
	{

	}

	AnimationManager::~AnimationManager()
	{

	}
}