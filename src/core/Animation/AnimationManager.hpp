#ifndef SRC_CORE_ANIMATION_ANIMATIONMANAGER_HPP
#define SRC_CORE_ANIMATION_ANIMATIONMANAGER_HPP

#include <map>
#include "AnimationData.hpp"
#include <ComponentFramework/SystemTypes.hpp>

namespace Core
{
	class AnimationManager
	{
	public:
		friend AnimationManager& AnimationManagerInstance();

		void PlayAnimation(const Entity& entity, std::string name, bool looped = false, bool reverse = false);
		void PlayAnimation(const Entity& entity, unsigned int id);
		void QueueAnimation(const Entity& entity, std::string name, bool looped = false, bool reverse = false);
		
		void StepAnimation(const Entity& entity);

		void PauseAnimation(const Entity& entity);
		void ResumeAnimation(const Entity& entity);

		bool IsAnimationPlaying(const Entity& entity, std::string name);

		unsigned int AddAnimation(Animation anim);
		void RemoveAnimation(unsigned int id);

		void ClearAnimations();

	private:
		AnimationManager();
		~AnimationManager();

		std::map<unsigned int, Animation> m_animations;
		unsigned long long int m_animationCounter;

		inline int GetAnimationID(std::string name)
		{
			for (std::map<unsigned int, Animation>::iterator it = m_animations.begin(); it != m_animations.end(); it++)
			{
				if (it->second.name == name)
					return it->first;
			}

			return -1;
		}
	};

	AnimationManager& AnimationManagerInstance();
}

#endif