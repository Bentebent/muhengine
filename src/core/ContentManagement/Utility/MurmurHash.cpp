#ifndef CONTENT_MANAGEMENT_MURMURHASH_HPP
#define CONTENT_MANAGEMENT_MURMURHASH_HPP

unsigned int MurmurHash2(const void* key, int len, unsigned int seed);

#endif