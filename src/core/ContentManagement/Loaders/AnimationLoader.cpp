#include "AnimationLoader.hpp"
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>
#include <gfx/GFXInterface.hpp>
#include <Utility/StringUtility.hpp>

#include <Animation/AnimationManager.hpp>

#include <ContentManagement/ContentManager.hpp>
#include "TextureLoader.hpp"

namespace Core
{
	AnimationLoader::AnimationLoader()
	{
		m_acceptList.push_back("anim");
	}

	AnimationLoader::~AnimationLoader()
	{

	}

	void AnimationLoader::Reload()
	{
		AnimationManagerInstance().ClearAnimations();
		std::vector<std::string> paths;

		int size = m_assetContainers.size();

		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			it->references = 1;
			paths.push_back(it->path);
			Free(it->hash);
		}

		for (int i = 0; i < size; i++)
			DoFree();

		for (int i = 0; i < paths.size(); i++)
			contentManager.Load(paths[i], ContentLoadPolicy::BLOCKING);
	}

	void* AnimationLoader::LoadData(std::string type, std::string path)
	{
		std::string line;
		std::ifstream animFile;

		std::ifstream dataFile;

		std::vector<Animation> animations;
		std::string texturePath;
		std::string dataPath;

		std::map<std::string, AnimationFrame> frames;

		animFile.open(path);

		if (animFile.is_open())
		{
			std::getline(animFile, texturePath);
			std::getline(animFile, dataPath);

			dataFile.open(dataPath);

			if (dataFile.is_open())
			{
				while (!dataFile.eof())
				{
					std::getline(dataFile, line);
					std::vector<std::string> substr = SplitString(line, 0x20);

					if (substr.size() == 6)
					{
						unsigned int x = std::stoi(substr[2]);
						unsigned int y = std::stoi(substr[3]);
						unsigned int width = std::stoi(substr[4]);
						unsigned int height = std::stoi(substr[5]);

						AnimationFrame frame;
						frame.x = x;
						frame.y = y;
						frame.width = width;
						frame.height = height;

						frames.emplace(substr[0], frame);
					}
				}
			}

			dataFile.close();

			while (!animFile.eof())
			{
				Animation anim;
				anim.playTime = 0;

				std::getline(animFile, line);

				if (line == "#anim")
				{
					AnimationFrame frame;
					unsigned int idCounter = 0;

					std::string animName;
					std::string frameCount;

					std::getline(animFile, animName);
					std::getline(animFile, frameCount);

					anim.name = animName;
					unsigned int nrOfFrames = std::stoi(frameCount);
					
					for (int i = 0; i < nrOfFrames; i++)
					{
						std::string frameName;
						std::string frameTime;

						std::getline(animFile, frameName);
						std::getline(animFile, frameTime);

						unsigned int msPerFrame = std::stoi(frameTime);

						frame = frames[frameName];

						frame.id	= idCounter++;
						frame.time	= msPerFrame;

						anim.playTime += msPerFrame;

						anim.frames.push_back(frame);
					}
					animations.push_back(anim);
				}
			}
		}
		animFile.close();

		AnimationData* ad = new AnimationData();
		ad->animationIDs.clear();

		for (int i = 0; i < animations.size(); i++)
		{
			unsigned int id = AnimationManagerInstance().AddAnimation(animations[i]);
			ad->animationIDs.push_back(id);
		}

		//Just return what texture to use for these animations, for now
		contentManager.Load(texturePath, ContentLoadPolicy::BLOCKING);
		ad->textureID = ((TextureData*)contentManager.GetContentData(texturePath))->textureID;

		return (void*)ad;
	}

	void AnimationLoader::FinishedLoading(unsigned int hash)
	{
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
			}
		}
	}

	void AnimationLoader::FreeData()
	{
		unsigned int hash = m_removalList.back();
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				contentManager.FreeFromValue(((AnimationData*)it->data)->textureID, "png");
				delete (AnimationData*)it->data;
				return;
			}
		}
	}

	AssetContainer* AnimationLoader::GetAssetFromValue(unsigned int value)
	{
		int size = m_assetContainers.size();
		for (int i = 0; i < size; i++)
		{
		}

		return nullptr;
	}
}