#ifndef SRC_CORE_CONTENTMANAGEMENT_LOADERS_TEXTURELOADER_HPP
#define SRC_CORE_CONTENTMANAGEMENT_LOADERS_TEXTURELOADER_HPP

#include <ContentManagement/Loaders/LoaderBase.hpp>

namespace Core
{
	struct TextureData
	{
		unsigned int textureID;
	};


	class TextureLoader : public LoaderBase
	{
	public:
		TextureLoader();
		~TextureLoader();

		void Reload();

		/*!
		Will load the data to cache and return a pointer to it.
		*/
		void* LoadData(std::string type, std::string path) override;

		/*!
		Will apply the result of all finished loads. Called from the ContentSystem-class.
		*/
		void FinishedLoading(unsigned int hash) override;

		/*!
		Will free one of the hashes in the removal list.
		*/
		void FreeData() override;

		/*
		Will scan the assetContainers for the openglHandle provided and return the hash.
		*/
		AssetContainer* GetAssetFromValue(unsigned int value) override;

	private:
	};
}

#endif