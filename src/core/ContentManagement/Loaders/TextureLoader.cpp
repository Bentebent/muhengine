#include "TextureLoader.hpp"
#include <thread>
#include <chrono>

#include <gfx/GFXInterface.hpp>

namespace Core
{
	TextureLoader::TextureLoader()
	{
		m_acceptList.push_back("png");
		m_acceptList.push_back("bmp");
		m_acceptList.push_back("tga");
		m_acceptList.push_back("jpg");
		m_acceptList.push_back("gif");
		m_acceptList.push_back("hdr");
		m_acceptList.push_back("pic");
	}

	TextureLoader::~TextureLoader()
	{

	}

	void TextureLoader::Reload()
	{
		gfx::content::ReloadTextures();
	}

	void* TextureLoader::LoadData(std::string type, std::string path)
	{
		TextureData* td = new TextureData();
		gfx::content::LoadTexture(td->textureID, path.c_str());

		return (void*)td;
	}

	void TextureLoader::FinishedLoading(unsigned int hash)
	{
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				//Do nothing?
			}
		}
	}

	void TextureLoader::FreeData()
	{
		unsigned int hash = m_removalList.back();
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				gfx::content::DeleteTexture(((TextureData*)it->data)->textureID);
				delete it->data;

				return;
			}
		}
	}

	AssetContainer* TextureLoader::GetAssetFromValue(unsigned int value)
	{
		int size = m_assetContainers.size();
		for (int i = 0; i < size; i++)
		{
			TextureData* data = (TextureData*)m_assetContainers[i].data;
			if (data != nullptr)
			{
				if (data->textureID == value)
				{
					return &m_assetContainers[i];
				}
			}
		}

		return nullptr;
	}
}