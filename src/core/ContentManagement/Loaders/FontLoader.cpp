#include "FontLoader.hpp"
#include <thread>
#include <chrono>

#include <gfx/GFXInterface.hpp>

namespace Core
{
	FontLoader::FontLoader()
	{
		m_acceptList.push_back("ttf");
	}

	FontLoader::~FontLoader()
	{

	}

	void* FontLoader::LoadData(std::string type, std::string path)
	{
		FontData* fd = new FontData();
		gfx::content::LoadFont(fd->fontID, path.c_str());

		return (void*)fd;
	}

	void FontLoader::FinishedLoading(unsigned int hash)
	{
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				//Do nothing?
			}
		}
	}

	void FontLoader::FreeData()
	{
		unsigned int hash = m_removalList.back();
		for (auto it = m_assetContainers.begin(); it != m_assetContainers.end(); it++)
		{
			if (it->hash == hash)
			{
				//gfx::content::Delete(((ObjModelData*)it->data)->modelID);
				//Should delete texture here
				delete it->data;

				return;
			}
		}
	}

	AssetContainer* FontLoader::GetAssetFromValue(unsigned int value)
	{
		int size = m_assetContainers.size();
		for (int i = 0; i < size; i++)
		{
			FontData* data = (FontData*)m_assetContainers[i].data;
			if (data != nullptr)
			{
				if (data->fontID == value)
				{
					return &m_assetContainers[i];
				}
			}
		}

		return nullptr;
	}
}