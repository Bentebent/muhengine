#include "ContentManager.hpp"
#include <ContentManagement/Utility/MurmurHash.hpp>
#include <iostream>
#define INCLUDE_LOADER( loader_class ) Core::contentManager.AddLoader( new loader_class() );


Core::ContenManager Core::contentManager;

/* Include headers for loaders here */
#include <ContentManagement/Loaders/TextureLoader.hpp>
#include <ContentManagement/Loaders/FontLoader.hpp>
#include <ContentManagement/Loaders/AnimationLoader.hpp>

/*!
Defines the loaders handled by the ContentManager.
*/
struct ContentManagerLoaderDefinition
{
	ContentManagerLoaderDefinition()
	{
		/* Add data configuration here */
		INCLUDE_LOADER(Core::TextureLoader);
		INCLUDE_LOADER(Core::FontLoader);
		INCLUDE_LOADER(Core::AnimationLoader);
	}
} cm__;

/***** ContentManager implementation *****/

namespace Core
{

	ContenManager::ContenManager()
	{
	}

	ContenManager::~ContenManager()
	{
		for (int i = 0; i < m_loaders.size(); i++)
		{
			if (m_loaders[i])
			{
				delete m_loaders[i];
			}
		}
		m_loaders.clear();

		m_contentThreadPool.KillThreads();
	}

	void ContenManager::InitializeThreadPool(int nrThreads)
	{
		m_contentThreadPool.Initialize(nrThreads);
	}

	void ContenManager::KillThreads()
	{
		m_contentThreadPool.KillThreads();
	}


	bool ContenManager::Load(std::string path, ContentLoadPolicy policy)
	{
		int dotPos = path.find_last_of(".");
		if (dotPos < 0)
		{
			std::cout << "File path do not contain a file ending, path: \"" << path << "\"" << std::endl;
			return false;
		}

		std::string type = path.substr(dotPos + 1, path.size());
		if (type.size() == 0)
		{
			std::cout << "File path do not contain a file ending, path: \"" << path << "\"" << std::endl;
			return false;
		}

		unsigned int hash = MurmurHash2(path.c_str(), path.size(), path.size());

		int size = m_loaders.size();
		for (int i = 0; i < size; ++i)
		{
			if (m_loaders[i]->AcceptType(type))
			{
				return m_loaders[i]->Load(type, path, hash, policy, m_loaders[i]);;
			}
		}

		std::cout << "No loader is accepting ." << type << " files." << std::endl;
		return false;
	}

	void ContenManager::Free(std::string path)
	{
		int dotPos = path.find_last_of(".");
		if (dotPos < 0)
		{
			std::cout << "File path do not contain a file ending, path: \"" << path << "\"" << std::endl;
			return;
		}

		std::string type = path.substr(dotPos + 1, path.size());
		unsigned int hash = MurmurHash2(path.c_str(), path.size(), path.size());

		int size = m_loaders.size();
		for (int i = 0; i < size; ++i)
		{
			if (m_loaders[i]->AcceptType(type))
			{
				m_loaders[i]->Free(hash);
				return;
			}
		}
	}

	void ContenManager::AddLoader(LoaderBase* loader)
	{
		m_loaders.push_back(loader);
	}

	void ContenManager::AddJobb(std::function<void()> function)
	{
		m_contentThreadPool.Enqueue(function);
	}

	void ContenManager::AddLoadEvent(std::function<void()> function)
	{
		std::lock_guard<std::mutex> guard(m_eventMutex);
		m_eventList.push_back(function);
	}

	void ContenManager::HandleEvents()
	{
		// loading...
		int size = m_eventList.size();
		for (int i = 0; i < size; i++)
		{
			m_eventList[i]();
		}
		m_eventList.clear();

		// deleting...
		size = m_loaders.size();
		for (int i = 0; i < size; i++)
		{
			if (m_loaders[i]->CanFree())
			{
				m_loaders[i]->DoFree();
			}
		}

	}

	void ContenManager::Reload()
	{
		for (int i = 0; i < m_loaders.size(); i++)
		{
			m_loaders[i]->Reload();
		}
	}

	void ContenManager::WaitForOperations()
	{
		m_contentThreadPool.Wait();
	}

	void* ContenManager::GetContentData(std::string path)
	{
		int dotPos = path.find_last_of(".");
		if (dotPos < 0)
		{
			std::cout << "File path do not contain a file ending, path: \"" << path << "\"" << std::endl;
			return nullptr;
		}

		std::string type = path.substr(dotPos + 1, path.size());
		unsigned int hash = MurmurHash2(path.c_str(), path.size(), path.size());

		int size = m_loaders.size();
		for (int i = 0; i < size; ++i)
		{
			if (m_loaders[i]->AcceptType(type))
			{
				return m_loaders[i]->GetContentData(hash);
			}
		}
		return nullptr;
	}

	void ContenManager::FreeFromValue(unsigned int value, std::string tag)
	{

		int size = m_loaders.size();
		for (int i = 0; i < size; ++i)
		{
			if (m_loaders[i]->AcceptType(tag))
			{
				AssetContainer* asset = m_loaders[i]->GetAssetFromValue(value);
				if (asset == nullptr)
				{
					std::cout << "Unable to perform free on value: " << value << " with the tag: " << tag << std::endl;
					return;
				}

				m_loaders[i]->Free(asset->hash);
				return;
			}
		}
	}
}
