#ifndef CONTENT_LOAD_POLICY_STRUCT_HPP
#define CONTENT_LOAD_POLICY_STRUCT_HPP

namespace Core
{
	enum ContentLoadPolicy
	{
		BLOCKING,
		NON_BLOCKING,
	};
}

#endif