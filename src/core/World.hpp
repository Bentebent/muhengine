#ifndef SRC_CORE_WORLD_WORLD_HPP
#define SRC_CORE_WORLD_WORLD_HPP

#include "SystemDefs.hpp"
#include <lua/LuaCore.hpp>
#include <Memory/LinearHeap.hpp>
#include <bitmask/bitmask_system.hpp>
#include <Utility/InputHandler.hpp>
#include <Utility/WindowHandler.hpp>
#include <Utility/CollisionHandler.hpp>

#include <Map/TiledMap.hpp>
#include <Map/TiledParser.hpp>
#include <set>

#define WGETC Core::world.m_entityHandler.GetComponentTmpPointer
#define CONF Core::world.m_config

namespace Core
{
    class World
    {
    public:
        World() : m_systemHandler(), m_entityHandler(&m_systemHandler), 
			m_constantAllocator(Core::LinearAllocator(nullptr, 0)), m_constantHeap(Core::LinearHeap(m_constantAllocator)),
			m_levelAllocator(Core::LinearAllocator(nullptr, 0)), m_levelHeap(Core::LinearHeap(m_levelAllocator)),
			m_frameAllocator(Core::LinearAllocator(nullptr, 0)), m_frameHeap(Core::LinearHeap(m_frameAllocator)){};

        ~World();

        void InitWorld();

        SystemHandler m_systemHandler;
        EntityHandler m_entityHandler;

        unsigned char* m_worldMemory;
		LuaCore* m_luaCore;

		// program memory
		LinearAllocator m_constantAllocator;
		LinearHeap m_constantHeap;

		// level memory
		LinearAllocator m_levelAllocator;
		LinearHeap m_levelHeap;

		// frame memory...
		LinearAllocator m_frameAllocator;
		LinearHeap m_frameHeap;

		//Bitmask system
		BitmaskSystem m_bitmaskSystem;

		//Inputhandler
		InputHandler* m_inputHandler;

		//Level entities
		std::set<Entity> m_levelEntities;

		//Windowhandler
		WindowHandler* m_windowHandler;

		//Map
		TiledParser* m_tiledParser;
		TiledMap* m_map;

		//Collision
		CollisionHandler* m_collisionHandler;

    };

    extern World world;
}

#endif //SRC_CORE_WORLD_WORLD_HPP
