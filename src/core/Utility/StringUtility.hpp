#ifndef SRC_CORE_UTILITY_STRINGUTILITY_HPP
#define SRC_CORE_UTILITY_STRINGUTILITY_HPP

#include <string>
#include <sstream>
#include <vector>
#include <iostream>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>

#include <SFML/Main.hpp>
#include <SFML/Graphics.hpp>

#include <gfx/GFXInterface.hpp>		   

#include <zlib/zlib.h>

namespace Core
{
	template<class T>
	static std::string ToString(T arg)
	{
		std::ostringstream strs;
		strs << arg;
		std::string str = strs.str();
		return str;
	}

	// trim from start
	static inline std::string &TrimStringStart(std::string &s)
	{
		s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
		return s;
	}

	// trim from end
	static inline std::string &TrimStringEnd(std::string &s) 
	{
		s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
		return s;
	}

	// trim from both ends
	static inline std::string &TrimString(std::string &s) 
	{
		return TrimStringStart(TrimStringEnd(s));
	}


	static std::vector<std::string>& SplitString(const std::string &s, char delim, std::vector<std::string> &elems) 
	{
		std::stringstream ss(s);
		std::string item;
		while (std::getline(ss, item, delim)) 
		{
			elems.push_back(item);
		}
		return elems;
	}


	static std::vector<std::string> SplitString(const std::string &s, const char delim)
	{
		std::vector<std::string> elems;
		SplitString(s, delim, elems);
		return elems;
	}

	static sf::String WrapText(sf::String string, unsigned width, const sf::Font &font, unsigned characterSize, bool bold = false)
	{
		unsigned int currentOffset = 0;
		bool firstWord = true;
		int wordStart = 0;

		for (int pos = 0; pos < string.getSize(); pos++)
		{
			char currentChar = string[pos];
			if (currentChar == '\n')
			{
				currentOffset = 0;
				firstWord = true;
				continue;
			}
			else if (currentChar == ' ')
			{
				wordStart = pos;
				firstWord = false;
			}

			auto glyph = font.getGlyph(currentChar, characterSize, bold);
			currentOffset += glyph.advance;

			if (!firstWord && currentOffset > width)
			{
				pos = wordStart;
				string[pos] = '\n';
				firstWord = true;
				currentOffset = 0;
			}
		}

		return string;
	}

	/** Decompress an STL string using zlib and return the original data. */
	static std::string DecompressString(const std::string& str)
	{
		z_stream zs;                        // z_stream is zlib's control structure
		memset(&zs, 0, sizeof(zs));

		if (inflateInit(&zs) != Z_OK)
			throw(std::runtime_error("inflateInit failed while decompressing."));

		zs.next_in = (Bytef*)str.data();
		zs.avail_in = str.size();

		int ret;
		char outbuffer[32768];
		std::string outstring;

		// get the decompressed bytes blockwise using repeated calls to inflate
		do {
			zs.next_out = reinterpret_cast<Bytef*>(outbuffer);
			zs.avail_out = sizeof(outbuffer);

			ret = inflate(&zs, 0);

			if (outstring.size() < zs.total_out) {
				outstring.append(outbuffer,
					zs.total_out - outstring.size());
			}

		} while (ret == Z_OK);

		inflateEnd(&zs);

		if (ret != Z_STREAM_END) {          // an error occurred that was not EOF
			std::ostringstream oss;
			oss << "Exception during zlib decompression: (" << ret << ") "
				<< zs.msg;
			throw(std::runtime_error(oss.str()));
		}

		return outstring;
	}

	static bool DecompressString(const char* source, std::vector<unsigned char>& dest, int inSize, int expectedSize)
	{
		if (!source)
		{
			return false;
		}

		int currentSize = expectedSize;
		std::vector<unsigned char> byteArray(expectedSize / sizeof(unsigned char));
		z_stream stream;
		stream.zalloc = Z_NULL;
		stream.zfree = Z_NULL;
		stream.opaque = Z_NULL;
		stream.next_in = (Bytef*)source;
		stream.avail_in = inSize;
		stream.next_out = (Bytef*)byteArray.data();
		stream.avail_out = expectedSize;

		if (inflateInit2(&stream, 15 + 32) != Z_OK)
		{
			return false;
		}

		int result = 0;
		do
		{
			result = inflate(&stream, Z_SYNC_FLUSH);

			switch (result)
			{
			case Z_NEED_DICT:
			case Z_STREAM_ERROR:
				result = Z_DATA_ERROR;
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
				inflateEnd(&stream);
				return false;
			}

			if (result != Z_STREAM_END)
			{
				int oldSize = currentSize;
				currentSize *= 2;
				std::vector<unsigned char> newArray(currentSize / sizeof(unsigned char));
				std::memcpy(newArray.data(), byteArray.data(), currentSize / 2);
				byteArray = std::move(newArray);

				stream.next_out = (Bytef*)(byteArray.data() + oldSize);
				stream.avail_out = oldSize;

			}
		} while (result != Z_STREAM_END);

		if (stream.avail_in != 0)
		{
			return false;
		}

		const int outSize = currentSize - stream.avail_out;
		inflateEnd(&stream);

		std::vector<unsigned char> newArray(outSize / sizeof(unsigned char));
		std::memcpy(newArray.data(), byteArray.data(), outSize);
		byteArray = std::move(newArray);

		//copy bytes to vector
		int length = currentSize / sizeof(unsigned char);
		dest.insert(dest.begin(), byteArray.begin(), byteArray.end());

		return true;
	}
}

#endif