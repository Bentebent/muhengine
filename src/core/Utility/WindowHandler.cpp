#include "WindowHandler.hpp"
#include "WindowModes.hpp"
#include <World.hpp>
namespace Core
{
	WindowHandler::WindowHandler()
	{
		m_window			= nullptr;
		m_width				= 0;
		m_height			= 0;
		m_windowHasFocus	= false;
	}

	WindowHandler::~WindowHandler()
	{

	}

	void WindowHandler::Initialize(int width, int height, std::string title, InputHandler* inputHandler)
	{
		m_width				= width;
		m_height			= height;
		m_window			= world.m_constantHeap.NewObject<sf::RenderWindow>(sf::VideoMode(width, height), title);//new sf::RenderWindow(sf::VideoMode(width, height), title);
		m_windowHasFocus	= true;

		m_inputHandler		= inputHandler;
	}

	sf::Window* WindowHandler::GetWindow()
	{
		return m_window;
	}


	int WindowHandler::WindowEvents()
	{
		sf::Event event;
		int count = 0;
		while (m_window->pollEvent(event))
		{
			count++;
			switch (event.type)
			{
				case sf::Event::Closed:
				{
					m_window->close();
					return WINDOW_CLOSED;
				}
				break;

				case sf::Event::Resized:
				{
					//Do something here to resize drawing canvas, probably
				}
				break;

				case sf::Event::LostFocus:
				{
					m_windowHasFocus = false;
					//Push some kind of pause menu, maybe, I dunno
				}
				break;

				case sf::Event::GainedFocus:
				{
					m_windowHasFocus = true;
				}
				break;

				case sf::Event::KeyReleased:
				{
					m_inputHandler->SetKeyReleased(event.key.code);
				}
				break;
			}
		}

		if (m_windowHasFocus)
			return WINDOW_GAINED_FOCUS;
		else
			return WINDOW_LOST_FOCUS;
	}

	int WindowHandler::GetWidth()
	{
		return m_width;
	}

	int WindowHandler::GetHeight()
	{
		return m_height;
	}

	void WindowHandler::SetSize(int width, int height)
	{
		m_width		= width;
		m_height	= height;
		m_window->setSize(sf::Vector2u(m_width, m_height));
	}

	void WindowHandler::SwapBuffers()
	{
		m_window->display();
	}

	void WindowHandler::Clear(sf::Color clearColor)
	{
		m_window->clear(clearColor);
	}

}