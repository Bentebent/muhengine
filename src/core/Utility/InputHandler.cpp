#include "InputHandler.hpp"

namespace Core
{
	InputHandler::InputHandler()
	{
		for (int i = 0; i < sf::Keyboard::KeyCount; i++)
		{
			m_keyMap.emplace((sf::Keyboard::Key)i, false);
		}

		m_stringToKeyMap.emplace("A",		sf::Keyboard::Key::A);
		m_stringToKeyMap.emplace("B",		sf::Keyboard::Key::B);
		m_stringToKeyMap.emplace("C",		sf::Keyboard::Key::C);
		m_stringToKeyMap.emplace("D",		sf::Keyboard::Key::D);
		m_stringToKeyMap.emplace("E",		sf::Keyboard::Key::E);
		m_stringToKeyMap.emplace("F",		sf::Keyboard::Key::F);
		m_stringToKeyMap.emplace("G",		sf::Keyboard::Key::G);
		m_stringToKeyMap.emplace("H",		sf::Keyboard::Key::H);
		m_stringToKeyMap.emplace("I",		sf::Keyboard::Key::I);
		m_stringToKeyMap.emplace("J",		sf::Keyboard::Key::J);
		m_stringToKeyMap.emplace("K",		sf::Keyboard::Key::K);
		m_stringToKeyMap.emplace("L",		sf::Keyboard::Key::L);
		m_stringToKeyMap.emplace("M",		sf::Keyboard::Key::M);
		m_stringToKeyMap.emplace("N",		sf::Keyboard::Key::N);
		m_stringToKeyMap.emplace("O",		sf::Keyboard::Key::O);
		m_stringToKeyMap.emplace("P",		sf::Keyboard::Key::P);
		m_stringToKeyMap.emplace("Q",		sf::Keyboard::Key::Q);
		m_stringToKeyMap.emplace("R",		sf::Keyboard::Key::R);
		m_stringToKeyMap.emplace("S",		sf::Keyboard::Key::S);
		m_stringToKeyMap.emplace("T",		sf::Keyboard::Key::T);
		m_stringToKeyMap.emplace("U",		sf::Keyboard::Key::U);
		m_stringToKeyMap.emplace("V",		sf::Keyboard::Key::V);
		m_stringToKeyMap.emplace("W",		sf::Keyboard::Key::W);
		m_stringToKeyMap.emplace("X",		sf::Keyboard::Key::X);
		m_stringToKeyMap.emplace("Y",		sf::Keyboard::Key::Y);
		m_stringToKeyMap.emplace("Z",		sf::Keyboard::Key::Z);
		m_stringToKeyMap.emplace("Num0", sf::Keyboard::Key::Num0);
		m_stringToKeyMap.emplace("Num1", sf::Keyboard::Key::Num1);
		m_stringToKeyMap.emplace("Num2", sf::Keyboard::Key::Num2);
		m_stringToKeyMap.emplace("Num3", sf::Keyboard::Key::Num3);
		m_stringToKeyMap.emplace("Num4", sf::Keyboard::Key::Num4);
		m_stringToKeyMap.emplace("Num5", sf::Keyboard::Key::Num5);
		m_stringToKeyMap.emplace("Num6", sf::Keyboard::Key::Num6);
		m_stringToKeyMap.emplace("Num7", sf::Keyboard::Key::Num7);
		m_stringToKeyMap.emplace("Num8", sf::Keyboard::Key::Num8);
		m_stringToKeyMap.emplace("Num9", sf::Keyboard::Key::Num9);
		m_stringToKeyMap.emplace("Escape", sf::Keyboard::Key::Escape);
		m_stringToKeyMap.emplace("LControl", sf::Keyboard::Key::LControl);
		m_stringToKeyMap.emplace("LShift", sf::Keyboard::Key::LShift);
		m_stringToKeyMap.emplace("LAlt", sf::Keyboard::Key::LAlt);
		m_stringToKeyMap.emplace("LSystem", sf::Keyboard::Key::LSystem);
		m_stringToKeyMap.emplace("RControl", sf::Keyboard::Key::RControl);
		m_stringToKeyMap.emplace("RShift", sf::Keyboard::Key::RShift);
		m_stringToKeyMap.emplace("RAlt", sf::Keyboard::Key::RAlt);
		m_stringToKeyMap.emplace("RSystem", sf::Keyboard::Key::RSystem);
		m_stringToKeyMap.emplace("Menu", sf::Keyboard::Key::Menu);
		m_stringToKeyMap.emplace("LBracket", sf::Keyboard::Key::LBracket);
		m_stringToKeyMap.emplace("RBracket", sf::Keyboard::Key::RBracket);
		m_stringToKeyMap.emplace("SemiColon", sf::Keyboard::Key::SemiColon);
		m_stringToKeyMap.emplace("Comma", sf::Keyboard::Key::Comma);
		m_stringToKeyMap.emplace("Period", sf::Keyboard::Key::Period);
		m_stringToKeyMap.emplace("Quote", sf::Keyboard::Key::Quote);
		m_stringToKeyMap.emplace("Slash", sf::Keyboard::Key::Slash);
		m_stringToKeyMap.emplace("BackSlash", sf::Keyboard::Key::BackSlash);
		m_stringToKeyMap.emplace("Tilde", sf::Keyboard::Key::Tilde);
		m_stringToKeyMap.emplace("Equal", sf::Keyboard::Key::Equal);
		m_stringToKeyMap.emplace("Dash", sf::Keyboard::Key::Dash);
		m_stringToKeyMap.emplace("Space", sf::Keyboard::Key::Space);
		m_stringToKeyMap.emplace("Return", sf::Keyboard::Key::Return);
		m_stringToKeyMap.emplace("BackSpace", sf::Keyboard::Key::BackSpace);
		m_stringToKeyMap.emplace("Tab", sf::Keyboard::Key::Tab);
		m_stringToKeyMap.emplace("PageUp", sf::Keyboard::Key::PageUp);
		m_stringToKeyMap.emplace("PageDown", sf::Keyboard::Key::PageDown);
		m_stringToKeyMap.emplace("End", sf::Keyboard::Key::End);
		m_stringToKeyMap.emplace("Home", sf::Keyboard::Key::Home);
		m_stringToKeyMap.emplace("Insert", sf::Keyboard::Key::Insert);
		m_stringToKeyMap.emplace("Delete", sf::Keyboard::Key::Delete);
		m_stringToKeyMap.emplace("Add", sf::Keyboard::Key::Add);
		m_stringToKeyMap.emplace("Subtract", sf::Keyboard::Key::Subtract);
		m_stringToKeyMap.emplace("Multiply", sf::Keyboard::Key::Multiply);
		m_stringToKeyMap.emplace("Divide", sf::Keyboard::Key::Divide);
		m_stringToKeyMap.emplace("Left", sf::Keyboard::Key::Left);
		m_stringToKeyMap.emplace("Right", sf::Keyboard::Key::Right);
		m_stringToKeyMap.emplace("Up", sf::Keyboard::Key::Up);
		m_stringToKeyMap.emplace("Down", sf::Keyboard::Key::Down);
		m_stringToKeyMap.emplace("Numpad0", sf::Keyboard::Key::Numpad0);
		m_stringToKeyMap.emplace("Numpad1", sf::Keyboard::Key::Numpad1);
		m_stringToKeyMap.emplace("Numpad2", sf::Keyboard::Key::Numpad2);
		m_stringToKeyMap.emplace("Numpad3", sf::Keyboard::Key::Numpad3);
		m_stringToKeyMap.emplace("Numpad4", sf::Keyboard::Key::Numpad4);
		m_stringToKeyMap.emplace("Numpad5", sf::Keyboard::Key::Numpad5);
		m_stringToKeyMap.emplace("Numpad6", sf::Keyboard::Key::Numpad6);
		m_stringToKeyMap.emplace("Numpad7", sf::Keyboard::Key::Numpad7);
		m_stringToKeyMap.emplace("Numpad8", sf::Keyboard::Key::Numpad8);
		m_stringToKeyMap.emplace("Numpad9", sf::Keyboard::Key::Numpad9);
		m_stringToKeyMap.emplace("F1", sf::Keyboard::Key::F1);
		m_stringToKeyMap.emplace("F2", sf::Keyboard::Key::F2);
		m_stringToKeyMap.emplace("F3", sf::Keyboard::Key::F3);
		m_stringToKeyMap.emplace("F4", sf::Keyboard::Key::F4);
		m_stringToKeyMap.emplace("F5", sf::Keyboard::Key::F5);
		m_stringToKeyMap.emplace("F6", sf::Keyboard::Key::F6);
		m_stringToKeyMap.emplace("F7", sf::Keyboard::Key::F7);
		m_stringToKeyMap.emplace("F8", sf::Keyboard::Key::F8);
		m_stringToKeyMap.emplace("F9", sf::Keyboard::Key::F9);
		m_stringToKeyMap.emplace("F10", sf::Keyboard::Key::F10);
		m_stringToKeyMap.emplace("F11", sf::Keyboard::Key::F11);
		m_stringToKeyMap.emplace("F12", sf::Keyboard::Key::F12);
		m_stringToKeyMap.emplace("F13", sf::Keyboard::Key::F13);
		m_stringToKeyMap.emplace("F14", sf::Keyboard::Key::F14);
		m_stringToKeyMap.emplace("F15", sf::Keyboard::Key::F15);
		m_stringToKeyMap.emplace("Pause", sf::Keyboard::Key::Pause);
	}

	InputHandler::~InputHandler()
	{

	}

	bool InputHandler::ButtonPressed(sf::Keyboard::Key myKey)
	{
		if (sf::Keyboard::isKeyPressed(myKey))
		{
			m_keyMap[myKey] = true;
			return true;
		}

		return false;
	}

	bool InputHandler::ButtonReleased(sf::Keyboard::Key myKey)
	{
		if (!sf::Keyboard::isKeyPressed(myKey) && m_keyMap[myKey])
		{
			m_keyMap[myKey] = false;
			return true;
		}
		return false;
	}

	bool InputHandler::SingleButtonPress(sf::Keyboard::Key myKey)
	{
		if (sf::Keyboard::isKeyPressed(myKey) && !m_keyMap[myKey])
		{
			m_keyMap[myKey] = true;
			return true;
		}

		return false;
	}

	void InputHandler::SetKeyReleased(sf::Keyboard::Key myKey)
	{
		m_keyMap[myKey] = false;
	}

	sf::Keyboard::Key InputHandler::GetKeyFromString(std::string keyCode)
	{
		return m_stringToKeyMap[keyCode];
	}

}