#ifndef SRC_CORE_UTILITY_INPUTHANDLER
#define SRC_CORE_UTILITY_INPUTHANDLER

#include <SFML/Window.hpp>
#include <map>

namespace Core
{
	class InputHandler
	{
	public:
		InputHandler();
		~InputHandler();

		bool ButtonPressed(sf::Keyboard::Key myKey);
		bool ButtonReleased(sf::Keyboard::Key myKey);
		bool SingleButtonPress(sf::Keyboard::Key myKey);

		void SetKeyReleased(sf::Keyboard::Key myKey);

		sf::Keyboard::Key GetKeyFromString(std::string keyCode);

	private:
		std::map<sf::Keyboard::Key, bool> m_keyMap;
		std::map<std::string, sf::Keyboard::Key> m_stringToKeyMap;
	};
}

#endif