#ifndef SRC_CORE_UTILITY_BASE64_HPP
#define SRC_CORE_UTILITY_BASE64_HPP

#include <string>

namespace Core
{
	class Base64
	{
	public:
		Base64();
		~Base64();

		std::string base64_encode(unsigned char const*, unsigned int len);
		std::string base64_decode(std::string const& s);

	private:
		inline bool is_base64(unsigned char c) 
		{
			return (isalnum(c) || (c == '+') || (c == '/'));
		}

		const std::string base64_chars =
			"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
			"abcdefghijklmnopqrstuvwxyz"
			"0123456789+/";

	};
}

#endif