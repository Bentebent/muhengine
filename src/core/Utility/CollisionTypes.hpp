#ifndef SRC_CORE_UTILITY_COLLISIONTYPES_HPP
#define SRC_CORE_UTILITY_COLLISIONTYPES_HPP

namespace Core
{
	enum CollisionType
	{
		CIRCLE,
		RECTANGLE,
		COUNT
	};
}

#endif