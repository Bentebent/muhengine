#ifndef SRC_CORE_UTILITY_WINDOWHANDLER_HPP
#define SRC_CORE_UTILITY_WINDOWHANDLER_HPP

#include "InputHandler.hpp"
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <string>

namespace Core
{
	class WindowHandler
	{
	public:
		WindowHandler();
		~WindowHandler();

		sf::Window* GetWindow();

		void Initialize(int width, int height, std::string title, InputHandler* inputHandler);
		int WindowEvents();

		int GetWidth();
		int GetHeight();

		void SetSize(int width, int height);

		void Clear(sf::Color clearColor);
		void SwapBuffers();

	private:
		sf::RenderWindow* m_window;
		InputHandler* m_inputHandler;
		int m_width;
		int m_height;

		bool m_windowHasFocus;
	};
}

#endif