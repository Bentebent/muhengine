#include "CollisionHandler.hpp"
#include <glm/glm.hpp>
#include <World.hpp>
namespace Core
{
	CollisionHandler::CollisionHandler()
	{

	}

	CollisionHandler::~CollisionHandler()
	{

	}

	void CollisionHandler::SetCollisionState(sf::FloatRect collisionBox,  CollisionLayer* const& collisionLayer, bool collisionOn)
	{
		float scaledTileWidth = world.m_map->GetScaledTileWidth();
		float scaledTileHeight = world.m_map->GetScaledTileHeight();

		int mapTileWidth = world.m_map->GetMapWidth();
		int mapTileHeight = world.m_map->GetMapHeight();

		int widthInTiles = glm::max(1.0f, glm::floor(collisionBox.width / scaledTileWidth));
		int heightInTiles = glm::max(1.0f, glm::floor(collisionBox.height / scaledTileHeight));

		int topLeftTileX = -1;
		int topLeftTileY = -1;
		world.m_map->GetTilePosition(collisionBox.left, collisionBox.top, topLeftTileX, topLeftTileY);

		std::vector<std::tuple<int, int>> bottomTiles;
		std::vector<std::tuple<int, int>> leftTiles;
		std::vector<std::tuple<int, int>> rightTiles;
		std::vector<std::tuple<int, int>> topTiles;

		for (int i = 0; i <= widthInTiles; i++)
		{
			for (int j = 0; j <= heightInTiles; j++)
			{
				int x = topLeftTileX + i;
				int y = topLeftTileY + j;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					bottomTiles.push_back(std::tuple<int, int>(x, y));
			}
		}

		for (int i = 0; i <= widthInTiles; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				int x = topLeftTileX + i;
				int y = topLeftTileY - j;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					topTiles.push_back(std::tuple<int, int>(x, y));
			}

		}


		for (int i = 0; i <= heightInTiles; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				int x = topLeftTileX - j;
				int y = topLeftTileY + i;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					leftTiles.push_back(std::tuple<int, int>(x, y));
			}

		}


		for (int i = 0; i <= heightInTiles; i++)
		{
			for (int j = 0; j <= widthInTiles; j++)
			{

				int x = topLeftTileX + j;
				int y = topLeftTileY + i;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					rightTiles.push_back(std::tuple<int, int>(x, y));
			}
		}

		TileCollision(collisionBox, bottomTiles, collisionLayer, scaledTileWidth, scaledTileHeight, collisionOn);
		TileCollision(collisionBox, topTiles, collisionLayer, scaledTileWidth, scaledTileHeight, collisionOn);
		TileCollision(collisionBox, rightTiles, collisionLayer, scaledTileWidth, scaledTileHeight, collisionOn);
		TileCollision(collisionBox, leftTiles, collisionLayer, scaledTileWidth, scaledTileHeight, collisionOn);
	}

	CollisionResult CollisionHandler::MapCollision(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, float dirX, float dirY)
	{
		float scaledTileWidth = world.m_map->GetScaledTileWidth();
		float scaledTileHeight = world.m_map->GetScaledTileHeight();

		int mapTileWidth = world.m_map->GetMapWidth();
		int mapTileHeight = world.m_map->GetMapHeight();

		int widthInTiles = glm::max(1.0f, glm::floor(collisionBox.width / scaledTileWidth));
		int heightInTiles = glm::max(1.0f, glm::floor(collisionBox.height / scaledTileHeight));

		int topLeftTileX = -1;
		int topLeftTileY = -1;
		world.m_map->GetTilePosition(collisionBox.left, collisionBox.top, topLeftTileX, topLeftTileY);

		std::vector<std::tuple<int, int>> bottomTiles;
		std::vector<std::tuple<int, int>> leftTiles;
		std::vector<std::tuple<int, int>> rightTiles;
		std::vector<std::tuple<int, int>> topTiles;

		for (int i = 0; i <= widthInTiles; i++)
		{
			for (int j = 0; j <= heightInTiles; j++)
			{
				int x = topLeftTileX + i;
				int y = topLeftTileY + j;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					bottomTiles.push_back(std::tuple<int, int>(x, y));
			}
		}

		for (int i = 0; i <= widthInTiles; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				int x = topLeftTileX + i;
				int y = topLeftTileY - j;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					topTiles.push_back(std::tuple<int, int>(x, y));
			}
			
		}
			

		for (int i = 0; i <= heightInTiles; i++)
		{
			for (int j = -1; j <= 1; j++)
			{
				int x = topLeftTileX - j;
				int y = topLeftTileY + i;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					leftTiles.push_back(std::tuple<int, int>(x, y));
			}
			
		}
			

		for (int i = 0; i <= heightInTiles; i++)
		{
			for (int j = 0; j <= widthInTiles; j++)
			{

				int x = topLeftTileX + j;
				int y = topLeftTileY + i;

				if (x >= 0 && x < mapTileWidth && y >= 0 && y < mapTileHeight)
					rightTiles.push_back(std::tuple<int, int>(x, y));
			}
		}


		//Debug
		//for (int i = 0; i < bottomTiles.size(); i++)
		//	gfx::debug::RenderRectangle(std::get<0>(bottomTiles[i]) * scaledTileWidth, std::get<1>(bottomTiles[i]) * scaledTileHeight, scaledTileWidth, scaledTileHeight, 0, 255, 0, 100);
		
		//for (int i = 0; i < topTiles.size(); i++)
		//	gfx::debug::RenderRectangle(std::get<0>(topTiles[i]) * scaledTileWidth, std::get<1>(topTiles[i]) * scaledTileHeight, scaledTileWidth, scaledTileHeight, 0, 255, 0, 100);
		
		//for (int i = 0; i < leftTiles.size(); i++)
		//	gfx::debug::RenderRectangle(std::get<0>(leftTiles[i]) * scaledTileWidth, std::get<1>(leftTiles[i]) * scaledTileHeight, scaledTileWidth, scaledTileHeight, 0, 0, 255, 100);
		//
		//for (int i = 0; i < rightTiles.size(); i++)
		//	gfx::debug::RenderRectangle(std::get<0>(rightTiles[i]) * scaledTileWidth, std::get<1>(rightTiles[i]) * scaledTileHeight, scaledTileWidth, scaledTileHeight, 0, 0, 0, 100);

		bool collision = false;
		int collisionX = 0;
		int collisionY = 0;

		float subTileWidth = scaledTileWidth * 0.5;
		float subTileHeight = scaledTileWidth * 0.5;

		CollisionResult outval = CollisionResult::NO_COLLISION;

		if (dirY > 0)
		{
			DirectionalTileCollision(collisionBox, collisionLayer, bottomTiles, scaledTileWidth, scaledTileHeight, collision, collisionX, collisionY);

			if (collision)
				outval = GetCollisionResultY(collisionBox, collisionLayer, subTileWidth, collisionX, collisionY);
		}

		if (dirY < 0)
		{
			DirectionalTileCollision(collisionBox, collisionLayer, topTiles, scaledTileWidth, scaledTileHeight, collision, collisionX, collisionY);

			if (collision)
				outval = GetCollisionResultY(collisionBox, collisionLayer, subTileWidth, collisionX, collisionY);
		}

		if (dirX < 0)
		{
			DirectionalTileCollision(collisionBox, collisionLayer, leftTiles, scaledTileWidth, scaledTileHeight, collision, collisionX, collisionY);

			if (collision)
				outval = GetCollisionResultX(collisionBox, collisionLayer, subTileHeight, collisionX, collisionY);
		}

		if (dirX > 0)
		{
			DirectionalTileCollision(collisionBox, collisionLayer, rightTiles, scaledTileWidth, scaledTileHeight, collision, collisionX, collisionY);

			if (collision)
				outval = GetCollisionResultX(collisionBox, collisionLayer, subTileHeight, collisionX, collisionY);
		}

		return outval;
	}

	void CollisionHandler::TileCollision(sf::FloatRect collisionBox, std::vector<std::tuple<int, int>> tiles, CollisionLayer* const& collisionLayer, float scaledTileWidth, float scaledTileHeight, bool collisionOn)
	{
		float subTileWidth = scaledTileWidth * 0.5;
		float subTileHeight = scaledTileWidth * 0.5;

		for (int i = 0; i < tiles.size(); i++)
		{
			int x = std::get<0>(tiles[i]);
			int y = std::get<1>(tiles[i]);

			int leftX = x * 2;
			int rightX = x * 2 + 1;

			int topY = y * 2;
			int bottomY = y * 2 + 1;
			
			sf::FloatRect tileRect = sf::FloatRect(x * scaledTileWidth - subTileWidth, y * scaledTileHeight - subTileHeight, scaledTileWidth, scaledTileHeight);

			if (tileRect.intersects(collisionBox))
			{
				collisionLayer->tiles[x][y] = 1;

				sf::FloatRect tempRect = sf::FloatRect(leftX * subTileWidth - subTileWidth, topY * subTileHeight - subTileHeight, subTileWidth, subTileHeight);

				if (tempRect.intersects(collisionBox))
					collisionLayer->subTiles[leftX][topY] = collisionOn ? 1 : 0;

				tempRect = sf::FloatRect(rightX * subTileWidth - subTileWidth, topY * subTileHeight - subTileHeight, subTileWidth, subTileHeight);

				if (tempRect.intersects(collisionBox))
					collisionLayer->subTiles[rightX][topY] = collisionOn ? 1 : 0;

				tempRect = sf::FloatRect(leftX * subTileWidth - subTileWidth, bottomY * subTileHeight - subTileHeight, subTileWidth, subTileHeight);

				if (tempRect.intersects(collisionBox))
					collisionLayer->subTiles[leftX][bottomY] = collisionOn ? 1 : 0;

				tempRect = sf::FloatRect(rightX * subTileWidth - subTileWidth, bottomY * subTileHeight - subTileHeight, subTileWidth, subTileHeight);

				if (tempRect.intersects(collisionBox))
					collisionLayer->subTiles[rightX][bottomY] = collisionOn ? 1 : 0;
			}
		}
	}

	void CollisionHandler::DirectionalTileCollision(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, std::vector<std::tuple<int, int>> tiles, float scaledTileWidth, float scaledTileHeight, bool& collision, int& tileX, int& tileY)
	{
		std::map<int, sf::FloatRect> subRects;

		float subTileWidth = scaledTileWidth * 0.5;
		float subTileHeight = scaledTileWidth * 0.5;

		for (int i = 0; i < tiles.size(); i++)
		{
			int x = std::get<0>(tiles[i]);
			int y = std::get<1>(tiles[i]);

			int leftX = x * 2;
			int rightX = x * 2 + 1;

			int topY = y * 2;
			int bottomY = y * 2 + 1;

			if (collisionLayer->tiles[x][y] > 0)
			{
				sf::FloatRect tileRect = sf::FloatRect(x * scaledTileWidth - subTileWidth, y * scaledTileHeight - subTileHeight, scaledTileWidth, scaledTileHeight);
				
				if (tileRect.intersects(collisionBox))
				{
					if (collisionLayer->subTiles[leftX][topY] > 0)
						subRects.emplace(0, sf::FloatRect(leftX * subTileWidth - subTileWidth, topY * subTileHeight - subTileHeight, subTileWidth, subTileHeight));

					if (collisionLayer->subTiles[rightX][topY] > 0)
						subRects.emplace(1, sf::FloatRect(rightX * subTileWidth - subTileWidth, topY * subTileHeight - subTileHeight, subTileWidth, subTileHeight));

					if (collisionLayer->subTiles[leftX][bottomY] > 0)
						subRects.emplace(2, sf::FloatRect(leftX * subTileWidth - subTileWidth, bottomY * subTileHeight - subTileHeight, subTileWidth, subTileHeight));

					if (collisionLayer->subTiles[rightX][bottomY] > 0)
						subRects.emplace(3, sf::FloatRect(rightX * subTileWidth - subTileWidth, bottomY * subTileHeight - subTileHeight, subTileWidth, subTileHeight));

					//gfx::debug::RenderRectangle(x * scaledTileWidth - subTileWidth, y * scaledTileHeight - subTileHeight, scaledTileWidth, scaledTileHeight, 0, 125, 125, 100, false);

					for (std::map<int, sf::FloatRect>::iterator it = subRects.begin(); it != subRects.end(); it++)
					{
						if (it->second.intersects(collisionBox))
						{
							gfx::debug::RenderRectangle(it->second.left, it->second.top, subTileWidth, subTileHeight, 255, 0, 255, 100, false);

							switch (it->first)
							{
								case 0:
								{
									tileX = leftX;
									tileY = topY;
								}
								break;

								case 1:
								{
									tileX = rightX;
									tileY = topY;
								}
								break;

								case 2:
								{
									tileX = leftX;
									tileY = bottomY;
								}
								break;

								case 3:
								{
									tileX = rightX;
									tileY = bottomY;
								}
								break;
							}

							collision = true;
							return;
						}
					}
				}

				subRects.clear();
			}
		}
	}
}