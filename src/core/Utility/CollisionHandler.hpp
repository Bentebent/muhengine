#ifndef SRC_CORE_UTILITY_COLLISIONHANDLER_HPP
#define SRC_CORE_UTILITY_COLLISIONHANDLER_HPP

#include <SFML/Graphics.hpp>
#include <Map/Tile.hpp>
#include <Map/CollisionLayer.hpp>
#include <glm/glm.hpp>

namespace Core
{
	enum CollisionResult
	{
		NO_COLLISION,
		BOTH_EMPTY,
		LEFT_EMPTY,
		RIGHT_EMPTY,
		DOWN_EMPTY,
		UP_EMPTY,
		BLOCKED
	};

	class CollisionHandler
	{
	public:
		CollisionHandler();
		~CollisionHandler();

		CollisionResult MapCollision(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, float dirX, float dirY);

		void SetCollisionState(sf::FloatRect collisionBox,  CollisionLayer* const& collisionLayer, bool collisionOn);

	private:
		void DirectionalTileCollision(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, std::vector<std::tuple<int, int>> tiles, float scaledTileWidth, float scaledTileHeight, bool& collision, int& tileX, int& tileY);
		void TileCollision(sf::FloatRect collisionBox, std::vector<std::tuple<int, int>> tiles, CollisionLayer* const& collisionLayer, float scaledTileWidth, float scaledTileHeight, bool collisionOn);

		inline CollisionResult GetCollisionResultY(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, float subTileWidth, int collisionX, int collisionY)
		{
			bool neighbourRight = false;
			bool neighbourLeft = false;

			float boxCenterX = collisionBox.left + collisionBox.width / 2.0f;
			float tileCenterX = collisionX * subTileWidth;

			collisionX = glm::clamp(collisionX, 1, collisionLayer->width * 2);
			collisionY = glm::clamp(collisionY, 1, collisionLayer->height * 2);

			if (collisionLayer->subTiles[collisionX + 1][collisionY] > 0)
				neighbourRight = true;

			if (collisionLayer->subTiles[collisionX - 1][collisionY] > 0)
				neighbourLeft = true;

			if (!neighbourLeft && !neighbourRight)
			{
				if (boxCenterX - tileCenterX > 0)
					return CollisionResult::RIGHT_EMPTY;
				else
					return CollisionResult::LEFT_EMPTY;
			}
			else if (!neighbourLeft)
				return CollisionResult::LEFT_EMPTY;
			else if (!neighbourRight)
				return CollisionResult::RIGHT_EMPTY;
			else
				return CollisionResult::BLOCKED;
		}

		inline CollisionResult GetCollisionResultX(sf::FloatRect collisionBox, CollisionLayer* collisionLayer, float subTileHeight, int collisionX, int collisionY)
		{
			bool neighbourTop = false;
			bool neighbourBottom = false;

			collisionX = glm::clamp(collisionX, 1, collisionLayer->width * 2);
			collisionY = glm::clamp(collisionY, 1, collisionLayer->height * 2);

			float boxCenterY = collisionBox.top + collisionBox.height / 2.0f;
			float tileCenterY = collisionY * subTileHeight;

			if (collisionLayer->subTiles[collisionX][collisionY + 1] > 0)
				neighbourBottom = true;

			if (collisionLayer->subTiles[collisionX][collisionY - 1] > 0)
				neighbourTop = true;

			if (!neighbourTop && !neighbourBottom)
			{
				if (boxCenterY - tileCenterY > 0)
					return CollisionResult::DOWN_EMPTY;
				else
					return CollisionResult::UP_EMPTY;
			}
			else if (!neighbourTop)
				return CollisionResult::UP_EMPTY;
			else if (!neighbourBottom)
				return CollisionResult::DOWN_EMPTY;
			else
				return CollisionResult::BLOCKED;
		}
	};
}

#endif