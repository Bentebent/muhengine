#include "TestSystem.hpp"
#include <World.hpp>

#include <iostream>

namespace Core
{
    TestSystem::TestSystem()
    :BaseSystem(EntityHandler::GenerateAspect<TestComponent>(), 0ULL)
    {
    }

    void TestSystem::Update(float delta)
    {
		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			TestComponent* tc = WGETC<TestComponent>(*it);

			tc->a += 1;
		}
        
    }
}