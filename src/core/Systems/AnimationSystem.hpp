#ifndef SRC_CORE_SYSTEMS_ANIMATIONSYSTEM_HPP
#define SRC_CORE_SYSTEMS_ANIMATIONSYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>

namespace Core
{
	class AnimationSystem : public BaseSystem
	{
	public:
		virtual void Update(float delta) override;

		AnimationSystem();

		virtual const char* GetHumanName() { return "AnimationSystem"; }
	};
}

#endif