#ifndef SRC_CORE_SYSTEMS_TESTSYSTEM_HPP
#define SRC_CORE_SYSTEMS_TESTSYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>
#include <vector>

namespace Core
{
    class TestSystem : public BaseSystem
    {
    public:
        virtual void Update(float delta) override;

        TestSystem();

        virtual const char* GetHumanName() { return "TestSystem"; }
    };
}



#endif //MUHENGINE_TESTSYSTEM_HPP
