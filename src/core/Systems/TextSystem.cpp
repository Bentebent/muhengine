#include "TextSystem.hpp"
#include <World.hpp>

#include <iostream>
#include <SFML/Graphics.hpp>
#include <gfx/GFXInterface.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXInstanceData.hpp>
#include <Utility/StringUtility.hpp>
#include <math.h>

namespace Core
{
	TextSystem::TextSystem()
		:BaseSystem(EntityHandler::GenerateAspect<RenderingComponent, TransformationComponent, TextComponent>(), 0ULL)
	{
	}

	void TextSystem::Update(float delta)
	{
		float screenWidth = world.m_windowHandler->GetWidth();
		float screenHeight = world.m_windowHandler->GetHeight();

		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			TransformationComponent* tc = WGETC<TransformationComponent>(*it);
			RenderingComponent* rc = WGETC<RenderingComponent>(*it);
			TextComponent* txc = WGETC<TextComponent>(*it);

			if (!rc->renderMe)
				continue;

			int fontID = world.m_bitmaskSystem.Decode(rc->bitmask, MaskType::TEXTURE);

			sf::Text* text = world.m_frameHeap.NewObject<sf::Text>();
			gfx::TextInstanceData* tid = world.m_frameHeap.NewObject<gfx::TextInstanceData>();

			text->setColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
			text->setOrigin(rc->origin[0], rc->origin[1]);

			text->setPosition(tc->position[0], tc->position[1]);
			text->setScale(tc->scale[0], tc->scale[1]);
			text->setRotation(tc->rotation);

			text->setFont(*(sf::Font*)gfx::content::GetFont(fontID));
			text->setString(txc->text);
			text->setCharacterSize(txc->characterSize);

			tid->text = text;
			//tid->view.setCenter()
			
			tid->view.reset(sf::FloatRect(tc->position[0], tc->position[1], txc->textBox.width, txc->textBox.height));

			if(!txc->cacheMe)
				tid->view.setViewport(sf::FloatRect(tc->position[0] / screenWidth, tc->position[1] / screenHeight, txc->textBox.width / screenWidth, txc->textBox.height / screenHeight));

			tid->updated = txc->updated;
			tid->cached = txc->cacheMe;
			tid->renderTextureID = txc->cacheID;

			gfx::Execute(rc->bitmask, (void*)tid);

			txc->updated = false;
		}

	}

}