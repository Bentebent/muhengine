#ifndef SRC_CORE_SYSTEMS_SCRIPTSYSTEM_HPP
#define SRC_CORE_SYSTEMS_SCRIPTSYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>
#include <vector>
#include <queue>

namespace Core
{
	class ScriptSystem : public BaseSystem
	{
	public:
		virtual void Update(float delta) override;

		ScriptSystem();

		virtual const char* GetHumanName() { return "ScriptSystem"; }
	private:

		struct ScriptContainer
		{
			std::string funcName;
			int funcId;
			int stateId;
			int priority;
			int ownerID;
		};

		struct ComparePriority
		{
		public:
			bool operator()(ScriptContainer* lhs, ScriptContainer* rhs)
			{
				return lhs->priority > rhs->priority;
			}
		};

		std::priority_queue<ScriptContainer*, std::vector<ScriptContainer*>, ComparePriority> m_launchOrder;
	};
}



#endif //SRC_CORE_SYSTEMS_SCRIPTSYSTEM_HPP
