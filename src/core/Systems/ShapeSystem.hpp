#ifndef SRC_CORE_SYSTEMS_SHAPESYSTEM_HPP
#define SRC_CORE_SYSTEMS_SHAPESYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>

namespace Core
{
	class ShapeSystem : public BaseSystem
	{
	public:
		virtual void Update(float delta) override;

		ShapeSystem();

		virtual const char* GetHumanName() { return "ShapeSystem"; }
	private:
	};
}

#endif