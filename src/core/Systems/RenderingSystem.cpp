#include "RenderingSystem.hpp"
#include <World.hpp>

#include <iostream>
#include <SFML/Graphics.hpp>
#include <gfx/GFXInterface.hpp>
#include <gfx/GFXBitmaskDefinitions.hpp>
namespace Core
{
	RenderingSystem::RenderingSystem()
		:BaseSystem(EntityHandler::GenerateAspect<RenderingComponent, TransformationComponent>(), EntityHandler::GenerateAspect<TextComponent, ShapeComponent>())
	{
	}

	void RenderingSystem::Update(float delta)
	{
		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			TransformationComponent* tc = WGETC<TransformationComponent>(*it);
			RenderingComponent* rc = WGETC<RenderingComponent>(*it);

			unsigned int objectType = world.m_bitmaskSystem.Decode(rc->bitmask, MaskType::TYPE);

			float base = (rc->sourceRectangle[3] - rc->origin[1]) * tc->scale[1];

			float height =  (tc->position[1] + base) * -1.0f;
			rc->bitmask = world.m_bitmaskSystem.Encode(rc->bitmask, height, MaskType::DEPTH);

			switch (objectType)
			{
				case gfx::OBJECT_TYPES::SPRITE:
				{
					sf::Sprite* sprite = world.m_frameHeap.NewObject<sf::Sprite>();

					sprite->setColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
					sprite->setOrigin(rc->origin[0], rc->origin[1]);

					

					
					sprite->setTextureRect(sf::IntRect(rc->sourceRectangle[0], rc->sourceRectangle[1], rc->sourceRectangle[2], rc->sourceRectangle[3]));

					sprite->setPosition(tc->position[0], tc->position[1]);

					float scaleX = tc->scale[0];
					float scaleY = tc->scale[1];

					if (rc->flippedX)
						scaleX *= -1;

					if (rc->flippedY)
						scaleY *= -1;

					sprite->setScale(scaleX, scaleY);
					sprite->setRotation(tc->rotation);

					gfx::Execute(rc->bitmask, (void*)sprite);
				}
				break;
			}

			
		}

	}
}