#include "AnimationSystem.hpp"
#include <World.hpp>
#include <Animation/AnimationManager.hpp>

namespace Core
{
	AnimationSystem::AnimationSystem()
		:BaseSystem(EntityHandler::GenerateAspect<RenderingComponent, AnimationComponent>(), 0ULL)
	{
	}

	void AnimationSystem::Update(float delta)
	{
		float ms = delta * 1000;
		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			AnimationComponent* ac = WGETC<AnimationComponent>(*it);

			if (ac->playing)
			{
				ac->currentTime += ms * ac->speed;

				if (ac->currentTime >= ac->playTime)
				{
					AnimationManagerInstance().StepAnimation(*it);
				}
			}

		}
	}
}