#ifndef SRC_CORE_SYSTEMS_TEXTSYSTEM_HPP
#define SRC_CORE_SYSTEMS_TEXTSYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>
#include <vector>

namespace Core
{
	class TextSystem : public BaseSystem
	{
	public:
		virtual void Update(float delta) override;

		TextSystem();

		virtual const char* GetHumanName() { return "TextSystem"; }
	};
}



#endif //SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP
