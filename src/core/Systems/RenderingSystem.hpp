#ifndef SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP
#define SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP

#include <ComponentFramework/BaseSystem.hpp>
#include <vector>

namespace Core
{
	class RenderingSystem : public BaseSystem
	{
	public:
		virtual void Update(float delta) override;

		RenderingSystem();

		virtual const char* GetHumanName() { return "RenderingSystem"; }
	};
}



#endif //SRC_CORE_SYSTEMS_RENDERINGSYSTEM_HPP
