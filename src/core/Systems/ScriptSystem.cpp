#include "ScriptSystem.hpp"
#include <World.hpp>

#include <iostream>

namespace Core
{
	ScriptSystem::ScriptSystem()
		:BaseSystem(EntityHandler::GenerateAspect<ScriptComponent>(), 0ULL)
	{
	}

	void ScriptSystem::Update(float delta)
	{
		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			ScriptComponent* sc = WGETC<ScriptComponent>(*it);

			for (int i = 0; i < sc->scriptCount; i++)
			{
				ScriptContainer* myScript = world.m_frameHeap.NewObject<ScriptContainer>();

				myScript->priority	= sc->priorities[i];
				myScript->funcId	= sc->callingFunctions[i];
				myScript->stateId	= sc->stateIds[i];
				myScript->funcName	= sc->functionNames[i];
				myScript->ownerID	= *it;
				m_launchOrder.push(myScript);
			}
		}

		while (!m_launchOrder.empty())
		{
			ScriptContainer* myScript = m_launchOrder.top();
			m_launchOrder.pop();

			LUA_CALL func = world.m_luaCore->GetFunction(myScript->funcId);

			//funcId defines what your input and output should be
			switch (myScript->funcId)
			{
				//Takes no indata, returns nothing
				case 0:
				{
					func(nullptr, myScript->stateId, myScript->funcName);
				}

				case 1:
				{
					SimpleLuaCallData* slcd = world.m_frameHeap.NewPOD<SimpleLuaCallData>();

					slcd->dt		= delta;
					slcd->ownerID	= myScript->ownerID;

					func(slcd, myScript->stateId, myScript->funcName);
				}
			}
		}
		lua_gc(world.m_luaCore->GetCoreState(), LUA_GCCOLLECT, 0);
		m_launchOrder = std::priority_queue<ScriptContainer*, std::vector<ScriptContainer*>, ComparePriority>();
	}
}