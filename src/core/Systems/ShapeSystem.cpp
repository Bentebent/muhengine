#include "ShapeSystem.hpp"

#include <World.hpp>

#include <iostream>
#include <gfx/GFXInstanceData.hpp>

namespace Core
{
	ShapeSystem::ShapeSystem()
		:BaseSystem(EntityHandler::GenerateAspect<TransformationComponent, RenderingComponent, ShapeComponent>(), 0ULL)
	{
	}

	void ShapeSystem::Update(float delta)
	{
		for (std::vector<Entity>::iterator it = m_entities.begin(); it != m_entities.end(); it++)
		{
			ShapeComponent* sc = WGETC<ShapeComponent>(*it);
			TransformationComponent* tc = WGETC<TransformationComponent>(*it);
			RenderingComponent* rc = WGETC<RenderingComponent>(*it);

			if (!rc->renderMe)
				continue;

			switch (sc->shapeType)
			{
				case gfx::ShapeType::CIRCLE:
				{
					sf::CircleShape* circle = world.m_frameHeap.NewObject<sf::CircleShape>();

					circle->setPosition(tc->position[0], tc->position[1]);
					circle->setScale(tc->scale[0], tc->scale[1]);
					circle->setRotation(tc->rotation);

					circle->setOrigin(rc->origin[0], rc->origin[1]);
					circle->setFillColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
					circle->setTextureRect(sf::IntRect(rc->sourceRectangle[0], rc->sourceRectangle[1], rc->sourceRectangle[2], rc->sourceRectangle[3]));

					circle->setRadius(sc->radius);
					circle->setOutlineThickness(sc->outlineThickness);
					circle->setOutlineColor(sf::Color(sc->outlineColor[0], sc->outlineColor[1], sc->outlineColor[2], sc->outlineColor[3]));

					gfx::Execute(rc->bitmask, (void*)circle);
				}
				break;

				case gfx::ShapeType::POLYGON:
				{
					sf::CircleShape* circle = world.m_frameHeap.NewObject<sf::CircleShape>();

					circle->setPosition(tc->position[0], tc->position[1]);
					circle->setScale(tc->scale[0], tc->scale[1]);
					circle->setRotation(tc->rotation);

					circle->setOrigin(rc->origin[0], rc->origin[1]);
					circle->setFillColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
					circle->setTextureRect(sf::IntRect(rc->sourceRectangle[0], rc->sourceRectangle[1], rc->sourceRectangle[2], rc->sourceRectangle[3]));

					circle->setRadius(sc->radius);
					circle->setOutlineThickness(sc->outlineThickness);
					circle->setOutlineColor(sf::Color(sc->outlineColor[0], sc->outlineColor[1], sc->outlineColor[2], sc->outlineColor[3]));
					circle->setPointCount(sc->pointCount);

					gfx::Execute(rc->bitmask, (void*)circle);
				}
				break;

				case gfx::ShapeType::RECTANGLE:
				{
					sf::RectangleShape* rect = world.m_frameHeap.NewObject<sf::RectangleShape>();

					rect->setPosition(tc->position[0], tc->position[1]);
					rect->setScale(tc->scale[0], tc->scale[1]);
					rect->setRotation(tc->rotation);

					rect->setOrigin(rc->origin[0], rc->origin[1]);
					rect->setFillColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
					rect->setTextureRect(sf::IntRect(rc->sourceRectangle[0], rc->sourceRectangle[1], rc->sourceRectangle[2], rc->sourceRectangle[3]));

					rect->setSize(sf::Vector2f(sc->rectangleSize[0], sc->rectangleSize[1]));
					rect->setOutlineThickness(sc->outlineThickness);
					rect->setOutlineColor(sf::Color(sc->outlineColor[0], sc->outlineColor[1], sc->outlineColor[2], sc->outlineColor[3]));
					
					gfx::Execute(rc->bitmask, (void*)rect);
				}
				break;

				case gfx::ShapeType::CONVEX:
				{
					sf::ConvexShape* convex = world.m_frameHeap.NewObject<sf::ConvexShape>();

					convex->setPosition(tc->position[0], tc->position[1]);
					convex->setScale(tc->scale[0], tc->scale[1]);
					convex->setRotation(tc->rotation);

					convex->setOrigin(rc->origin[0], rc->origin[1]);
					convex->setFillColor(sf::Color(rc->color[0], rc->color[1], rc->color[2], rc->color[3]));
					convex->setTextureRect(sf::IntRect(rc->sourceRectangle[0], rc->sourceRectangle[1], rc->sourceRectangle[2], rc->sourceRectangle[3]));

					convex->setOutlineThickness(sc->outlineThickness);
					convex->setOutlineColor(sf::Color(sc->outlineColor[0], sc->outlineColor[1], sc->outlineColor[2], sc->outlineColor[3]));

					convex->setPointCount(sc->pointCount);

					for (int i = 0; i < sc->pointCount; i++)
						convex->setPoint(i, sf::Vector2f(sc->convexPoints[i][0], sc->convexPoints[i][1]));

					gfx::Execute(rc->bitmask, (void*)convex);
				}
				break;

				case gfx::ShapeType::LINE:
				{
				}
				break;

				case gfx::ShapeType::CUSTOM:
				{
				}
				break;
			}
		}
	}
}