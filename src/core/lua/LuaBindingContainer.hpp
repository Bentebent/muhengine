#ifndef SRC_CORE_LUA_BINDINGS_LUABINDINGCONTAINER_HPP
#define SRC_CORE_LUA_BINDINGS_LUABINDINGCONTAINER_HPP

//Bridges
#include <lua/Bridges/EntityBinding.hpp>
#include <lua/Bridges/TestComponentBridge.hpp>
#include <lua/Bridges/TransformComponentBridge.hpp>
#include <lua/Bridges/CollisionComponentBridge.hpp>
#include <lua/Bridges/ContentManagerBridge.hpp>
#include <lua/Bridges/BitmaskBridge.hpp>
#include <lua/Bridges/RenderingComponentBridge.hpp>
#include <lua/Bridges/ScriptComponentBridge.hpp>
#include <lua/Bridges/InputBridge.hpp>
#include <lua/Bridges/TextComponentBridge.hpp>
#include <lua/Bridges/ShapeComponentBridge.hpp>
#include <lua/Bridges/DebugBridge.hpp>
#include <lua/Bridges/GFXBridge.hpp>
#include <lua/Bridges/AnimationComponentBridge.hpp>
#include <lua/Bridges/AnimationBridge.hpp>
#include <lua/Bridges/MapBridge.hpp>
#include <lua/Bridges/CollisionBridge.hpp>

//User data
#include <lua/DataTypes/LuaUDuint64.hpp>
#include <lua/DataTypes/LuaUDSFMLView.hpp>

struct lua_State;
namespace Core
{
	struct LuaBindingContainer
	{
		LuaBindingContainer(lua_State* L) :

			//Bridges
			contentManagerBridge(L),
			bitmaskBridge(L),
			entityBinding(L),
			testComponentBridge(L),
			transformComponentBrigde(L),
			renderingComponentBridge(L),
			scriptComponentBridge(L),
			textComponentBridge(L),
			shapeComponentBridge(L),
			animationComponentBridge(L),
			collisionComponentBridge(L),
			inputBridge(L),
			debugBridge(L),
			gfxBridge(L),
			animationBridge(L),
			mapBridge(L),
			collisionBridge(L),


			//User data
			uint64UD(L),
			sfmlViewUD(L)
		{

		}

		//Bridges
		EntityBinding entityBinding;
		TestComponentBridge testComponentBridge;
		TransformComponentBridge transformComponentBrigde;
		ContentManagerBridge contentManagerBridge;
		BitmaskBridge bitmaskBridge;
		RenderingComponentBridge renderingComponentBridge;
		ScriptComponentBridge scriptComponentBridge;
		InputBridge inputBridge;
		TextComponentBridge textComponentBridge;
		ShapeComponentBridge shapeComponentBridge;
		AnimationComponentBridge animationComponentBridge;
		DebugBridge debugBridge;
		GFXBridge gfxBridge;
		AnimationBridge animationBridge;
		MapBridge mapBridge;
		CollisionComponentBridge collisionComponentBridge;
		CollisionBridge collisionBridge;

		//user data
		LuaUDuint64 uint64UD;
		LuaUDSFMLView sfmlViewUD;
	};
}


#endif