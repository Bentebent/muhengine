#ifndef SRC_CORE_LUA_LUAMETATABLETYPES_HPP
#define SRC_CORE_LUA_LUAMETATABLETYPES_HPP

#define UINT64_METATYPE "metatype_uint64"
#define SFML_VIEW_METATYPE "metatype_sfview"


#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <SFML/Graphics.hpp>

#include <cstdint>

namespace Core
{
	uint64_t* LuaNewUint64(lua_State* L, uint64_t val);
	sf::View* LuaNewSFMLView(lua_State* L, sf::View view);
}

#endif