#include "LuaCallScripts.hpp"
#include <iostream>
namespace Core
{
	LuaCallScripts::LuaCallScripts()
	{

	}

	LuaCallScripts::~LuaCallScripts()
	{

	}

	void LuaCallScripts::Initialize(lua_State* coreState, std::map<int, ChildState>* childStates)
	{
		m_coreState = coreState;
		m_luaChildStates = childStates;

		m_callingFunctions.clear();


		LUA_CALL temp = [&](void* inData, int stateIndex, std::string functionName) 
		{
			lua_State* ls = this->GetChildState(stateIndex);
			lua_pushstring(ls, functionName.c_str());
			lua_gettable(ls, LUA_GLOBALSINDEX);
			lua_resume(ls, 0);

			return nullptr;
		};

		m_callingFunctions.push_back(temp);

		temp = [&](void* inData, int stateIndex, std::string functionName)
		{
			lua_State* ls = this->GetChildState(stateIndex);
			lua_pushstring(ls, functionName.c_str());
			lua_gettable(ls, LUA_GLOBALSINDEX);

			SimpleLuaCallData* slcd = (SimpleLuaCallData*)inData;
			
			lua_pushnumber(ls, slcd->dt);
			lua_pushnumber(ls, slcd->ownerID);

			lua_resume(ls, 2);
			
			return nullptr;
		};

		m_callingFunctions.push_back(temp);
	}

	LUA_CALL LuaCallScripts::GetFunction(int index)
	{
		return m_callingFunctions.at(index);
	}

	lua_State* LuaCallScripts::GetChildState(int index)
	{
		std::map<int, ChildState>::iterator it = m_luaChildStates->find(index);

		if (it != m_luaChildStates->end())
			return it->second.luaState;
		return nullptr;
	}

}