#ifndef SRC_CORE_LUA_LUACALLSCRIPTS_HPP
#define SRC_CORE_LUA_LUACALLSCRIPTS_HPP

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>

#include <vector>
#include <map>
#include <functional>

#include "LuaUtility.hpp"

#define LUA_CALL std::function<void*(void* inData, int stateIndex, std::string functionName)>

namespace Core
{
	struct SimpleLuaCallData
	{
		int ownerID;
		float dt;
	};


	class LuaCallScripts
	{
	public:
		LuaCallScripts();
		~LuaCallScripts();

		void Initialize(lua_State* coreState, std::map<int, ChildState>* childStates);

		LUA_CALL GetFunction(int index);
		lua_State* GetChildState(int index);
		lua_State* GetCoreState() { return m_coreState; }
	private:
		lua_State* m_coreState;
		std::vector<LUA_CALL> m_callingFunctions;
		std::map<int, ChildState>* m_luaChildStates;
	};
}

#endif