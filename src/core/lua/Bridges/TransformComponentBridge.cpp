#include "TransformComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int AddTransformComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::TransformationComponent());

		if (nrOfArgs > 1)
		{
			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "rotation");
			float rotation = luau_tofloat(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "position");
			float position[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				position[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				position[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			lua_getfield(L, 2, "scale");
			float scale[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				scale[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				scale[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);
			tc->rotation = rotation;

			for (int i = 0; i < 2; i++)
			{
				tc->position[i] = position[i];
				tc->scale[i] = scale[i];
			}
		}

		return 0;
	}

	static int SetPosition(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);

		for (int i = 0; i < 2; i++)
		{
			lua_rawgeti(L, 2, i);
			tc->position[i] = luau_tofloat(L, -1);
			lua_pop(L, 1);
		}

		return 0;
	}

	static int SetScale(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);

		for (int i = 0; i < 2; i++)
		{
			lua_rawgeti(L, 2, i);
			tc->scale[i] = luau_tofloat(L, -1);
			lua_pop(L, 1);
		}
		return 0;
	}

	static int SetRotation(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		float rotation = luau_tofloat(L, 2);

		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);
		tc->rotation = rotation;

		return 0;
	}

	static int GetPosition(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, tc->position[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int GetScale(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, tc->scale[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int GetRotation(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);

		lua_pushnumber(L, tc->rotation);
		return 1;
	}
}

namespace Core
{
	TransformComponentBridge::TransformComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddTransformationComponent", AddTransformComponent);
		lua_register(coreState, "SetPosition", SetPosition);
		lua_register(coreState, "GetPosition", GetPosition);
		lua_register(coreState, "GetScale", GetScale);
		lua_register(coreState, "SetScale", SetScale);
		lua_register(coreState, "GetRotation", GetRotation);
		lua_register(coreState, "SetRotation", SetRotation);
	}
}