#ifndef SRC_CORE_LUA_BRIDGES_GFXBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_GFXBRIDGE_HPP

struct lua_State;
namespace Core
{
	class GFXBridge
	{
	public:
		GFXBridge(lua_State* coreState);
	};
}

#endif