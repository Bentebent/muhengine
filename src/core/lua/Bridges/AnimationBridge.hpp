#ifndef SRC_CORE_LUA_BRIDGES_ANIMATIONBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_ANIMATIONBRIDGE_HPP

struct lua_State;
namespace Core
{
	class AnimationBridge
	{
	public:
		AnimationBridge(lua_State* coreState);
	};
}

#endif