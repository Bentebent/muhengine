#include "TextComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <string>

extern "C"
{
	static int AddTextComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::TextComponent());

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		if (nrOfArgs > 1)
		{
			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "text");
			std::string text = lua_tostring(L, -1);
			lua_pop(L, 1);

			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "alignment");
			int alignment = lua_tointeger(L, -1);
			lua_pop(L, 1);

			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "characterSize");
			int characterSize = lua_tointeger(L, -1);
			lua_pop(L, 1);

			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "fontID");
			int fontID = lua_tointeger(L, -1);
			lua_pop(L, 1);

			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "cacheMe");
			bool cacheStatus = lua_toboolean(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "textBox");
			int box[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				box[0] = lua_tointeger(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				box[1] = lua_tointeger(L, -1);
				lua_pop(L, 1);
			}

			tc->cacheMe			= cacheStatus;
			tc->fontID			= fontID;
			tc->characterSize	= characterSize;
			tc->textBox.width	= box[0];
			tc->textBox.height	= box[0];
			tc->textAlignment	= alignment;

			Core::TextComponent::SetCacheStatus(tc, cacheStatus);
			Core::TextComponent::SetString(tc, text);
		}

		return 0;
	}

	static int RemoveTextComponent(lua_State* L)
	{
		return 0;
	}

	static int SetText(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		std::string text = lua_tostring(L, 2);

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);
		Core::TextComponent::SetString(tc, text);

		return 0;
	}

	static int SetTextBoxSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		int width			= lua_tointeger(L, 2);
		int height			= lua_tointeger(L, 3);

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);
		Core::TextComponent::SetTextBox(tc, width, height);
		return 0;
	}

	static int SetCharacterSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		int charSize = lua_tointeger(L, 2);

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);
		Core::TextComponent::SetCharacterSize(tc, charSize);

		return 0;
	}

	static int SetCacheStatus(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		bool cacheStatus = lua_toboolean(L, 2);

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);
		Core::TextComponent::SetCacheStatus(tc, cacheStatus);

		return 0;
	}

	static int SetFont(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		int fontID = lua_tointeger(L, 2);

		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		rc->bitmask = Core::world.m_bitmaskSystem.Encode(rc->bitmask, fontID, MaskType::TEXTURE);

		Core::TextComponent::SetFont(tc, fontID);

		return 0;
	}

	static int GetText(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		lua_pushstring(L, tc->originalText);

		return 1;
	}

	static int GetTextBoxSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		lua_newtable(L);

		lua_pushnumber(L, tc->textBox.width);
		lua_rawseti(L, -2, 0);

		lua_pushnumber(L, tc->textBox.height);
		lua_rawseti(L, -2, 1);

		return 1;
	}

	static int GetCharacterSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		lua_pushnumber(L, tc->characterSize);

		return 0;
	}

	static int GetCacheStatus(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		lua_pushboolean(L, tc->cacheMe);

		return 0;
	}

	static int GetFont(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TextComponent* tc = WGETC<Core::TextComponent>(entity);

		lua_pushinteger(L, tc->fontID);

		return 0;
	}
}

namespace Core
{
	TextComponentBridge::TextComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddTextComponent", AddTextComponent);
		lua_register(coreState, "SetText", SetText);
		lua_register(coreState, "SetTextBoxSize", SetTextBoxSize);
		lua_register(coreState, "SetCharacterSize", SetCharacterSize);
		lua_register(coreState, "SetCacheStatus", SetCacheStatus);
		lua_register(coreState, "SetFont", SetFont);
		lua_register(coreState, "GetText", GetText);
		lua_register(coreState, "GetTextBoxSize", GetTextBoxSize);
		lua_register(coreState, "GetCharacterSize", GetCharacterSize);
		lua_register(coreState, "GetCacheStatus", GetCacheStatus);
		lua_register(coreState, "GetFont", GetFont);
	}
}