#ifndef SRC_CORE_LUA_BINDINGS_ENTITYBINDING_HPP
#define SRC_CORE_LUA_BINDINGS_ENTITYBINDING_HPP

struct lua_State;
namespace Core
{
	class EntityBinding
	{
	public:
		EntityBinding(lua_State* coreState);
	};
}

#endif