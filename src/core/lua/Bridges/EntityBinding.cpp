#include "EntityBinding.hpp"
#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int CreateEntity(lua_State* L)
	{
		Core::Entity e = Core::world.m_entityHandler.CreateEntity();
		lua_pushnumber(L, e);

		return 1;
	}

	static int CreateLevelEntity(lua_State* L)
	{
		Core::Entity e = Core::world.m_entityHandler.CreateLevelEntity();
		lua_pushnumber(L, e);

		return 1;
	}

	static int DestroyEntity(lua_State* L)
	{
		Core::Entity e = lua_tonumber(L, 1);
		Core::world.m_entityHandler.DestroyEntity(e);
		return 0;
	}
}

namespace Core
{
	EntityBinding::EntityBinding(lua_State* coreState)
	{
		lua_register(coreState, "CreateEntity", CreateEntity);
		lua_register(coreState, "CreateLevelEntity", CreateLevelEntity);
		lua_register(coreState, "DestroyEntity", DestroyEntity);
	}
}