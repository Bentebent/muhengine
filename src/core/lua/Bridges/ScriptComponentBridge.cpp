#include "ScriptComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <string>

extern "C"
{
	static int AddScriptComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::ScriptComponent());
		Core::ScriptComponent* sc = WGETC<Core::ScriptComponent>(entity);

		if (nrOfArgs > 1)
		{
			for (int i = 2; i <= nrOfArgs; i++)
			{
				luaL_checktype(L, i, LUA_TTABLE);

				lua_getfield(L, i, "priority");
				int priority = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_getfield(L, i, "callingFunction");
				int callingFunction = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_getfield(L, i, "functionName");
				std::string functionName = lua_tostring(L, -1);
				lua_pop(L, 1);

				lua_getfield(L, i, "file");
				std::string file = lua_tostring(L, -1);
				lua_pop(L, 1);

				int stateId = Core::world.m_luaCore->LoadFile(file, false);

				sc->AddScript(stateId, functionName, callingFunction, priority);
			}
		}

		return 0;
	}

	static int AddScript(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ScriptComponent* sc = WGETC<Core::ScriptComponent>(entity);

		int priority			= lua_tonumber(L, 2);
		int callingFunction		= lua_tonumber(L, 3);
		std::string funcName	= lua_tostring(L, 4);
		std::string file		= lua_tostring(L, 5);

		int stateId = Core::world.m_luaCore->LoadFile(file, false);

		sc->AddScript(stateId, funcName, callingFunction, priority);

		return 0;
	}

	static int RemoveScript(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ScriptComponent* sc = WGETC<Core::ScriptComponent>(entity);

		return 0;
	}
}

namespace Core
{
	ScriptComponentBridge::ScriptComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddScriptComponent", AddScriptComponent);
		lua_register(coreState, "AddScript", AddScript);
	}
}