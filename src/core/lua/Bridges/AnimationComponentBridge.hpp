#ifndef SRC_CORE_LUA_BRIDGES_ANIMATIONCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_ANIMATIONCOMPONENTBRIDGE_HPP

struct lua_State;

namespace Core
{
	class AnimationComponentBridge
	{
	public:
		AnimationComponentBridge(lua_State* coreState);
	};
}

#endif