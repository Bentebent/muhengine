#ifndef SRC_CORE_LUA_BRIDGES_TRANSFORMCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_TRANSFORMCOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class TransformComponentBridge
	{
	public:
		TransformComponentBridge(lua_State* coreState);
	};
}

#endif