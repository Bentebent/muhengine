#include "BitmaskBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <gfx/GFXBitmaskDefinitions.hpp>

#include <cstdint>
#include <Lua/LuaMetatableTypes.hpp>
extern "C"
{

	static int SetBitmaskValue(lua_State* L)
	{

		Bitmask bitmask			= *(uint64_t*)lua_touserdata(L, 1);
		int bitmaskType			= 0;

		if (!lua_isnumber(L, 2))
		{
			std::string typeString = lua_tostring(L, 2);

			if (typeString == "COMMAND")
				bitmaskType = MaskType::COMMAND;
			else if (typeString == "LAYER")
				bitmaskType = MaskType::LAYER;
			else if (typeString == "TYPE")
				bitmaskType = MaskType::TYPE;
			else if (typeString == "TEXTURE")
				bitmaskType = MaskType::TEXTURE;
			else if (typeString == "VIEW")
				bitmaskType = MaskType::VIEW;
			else if (typeString == "DEPTH")
				bitmaskType = MaskType::DEPTH;
		}
		else
		{
			bitmaskType = lua_tonumber(L, 2);
		}

		int val = lua_tonumber(L, 3);

		bitmask = Core::world.m_bitmaskSystem.Encode(bitmask, val, bitmaskType);

		Core::LuaNewUint64(L, bitmask);

		return 1;
	}
}

namespace Core
{
	BitmaskBridge::BitmaskBridge(lua_State* coreState)
	{
		lua_register(coreState, "SetBitmaskValue", SetBitmaskValue);
	}
}