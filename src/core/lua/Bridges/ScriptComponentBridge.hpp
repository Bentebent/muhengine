#ifndef SRC_CORE_LUA_BRIDGES_SCRIPTCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_SCRIPTCOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class ScriptComponentBridge
	{
	public:
		ScriptComponentBridge(lua_State* coreState);
	};
}

#endif