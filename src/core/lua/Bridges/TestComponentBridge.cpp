#include "TestComponentBridge.hpp"
#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int AddTestComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);

		Core::world.m_entityHandler.AddComponents(entity, Core::TestComponent());

		if (nrOfArgs > 1)
		{
			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "a");
			int a = lua_tonumber(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "b");
			int b = lua_tonumber(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "foo");
			int foo[3];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				foo[0] = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				foo[1] = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 3);
				foo[2] = lua_tonumber(L, -1);
				lua_pop(L, 1);
			}

			Core::TestComponent* tc = WGETC<Core::TestComponent>(entity);
			tc->a = a;
			tc->b = b;
			
			for (int i = 0; i < 3; i++)
				tc->foo[i] = foo[i];
		}

		return 0;
	}

	static int SetA(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		int a = lua_tonumber(L, 2);

		Core::TestComponent* tc = WGETC<Core::TestComponent>(entity);
		tc->a = a;

		return 0;
	}

	static int GetA(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TestComponent* tc = WGETC<Core::TestComponent>(entity);

		lua_pushnumber(L, tc->a);
		return 1;
	}

	static int GetFoo(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TestComponent* tc = WGETC<Core::TestComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 3; i++)
		{
			lua_pushinteger(L, tc->foo[i]);
			lua_rawseti(L, -2, i);
		}
			
		return 1;
	}

	static int SetFoo(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::TestComponent* tc = WGETC<Core::TestComponent>(entity);

		for (int i = 0; i < 3; i++)
		{
			lua_rawgeti(L, 2, i + 1);
			tc->foo[i] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
		
		return 0;
	}
}

namespace Core
{
	TestComponentBridge::TestComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddTestComponent", AddTestComponent);
		lua_register(coreState, "GetA", GetA);
		lua_register(coreState, "SetA", SetA);
		lua_register(coreState, "GetFoo", GetFoo);
		lua_register(coreState, "SetFoo", SetFoo);
	}
}