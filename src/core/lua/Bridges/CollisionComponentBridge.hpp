#ifndef SRC_CORE_LUA_COLLISIONCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_COLLISIONCOMPONENTBRIDGE_HPP


struct lua_State;
namespace Core
{
	class CollisionComponentBridge
	{
	public:
		CollisionComponentBridge(lua_State* coreState);
	};
}

#endif 