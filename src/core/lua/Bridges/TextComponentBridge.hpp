#ifndef SRC_CORE_LUA_BRIDGES_TEXTCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_TEXTCOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class TextComponentBridge
	{
	public:
		TextComponentBridge(lua_State* coreState);
	};
}

#endif