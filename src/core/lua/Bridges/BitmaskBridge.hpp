#ifndef SRC_CORE_LUA_BRIDGES_BITMASKBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_BITMASKBRIDGE_HPP

struct lua_State;

namespace Core
{
	class BitmaskBridge
	{
	public:
		BitmaskBridge(lua_State* coreState);
	};
}

#endif