#ifndef SRC_CORE_LUA_BRIDGES_INPUTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_INPUTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class InputBridge
	{
	public:
		InputBridge(lua_State* coreState);
	};
}

#endif