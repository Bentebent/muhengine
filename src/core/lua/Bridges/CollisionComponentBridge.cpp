#include "CollisionComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <Utility/CollisionTypes.hpp>

extern "C"
{
	static int AddCollisionComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::CollisionComponent());

		if (nrOfArgs > 1)
		{
			luaL_checktype(L, 2, LUA_TTABLE);

			lua_getfield(L, 2, "collisionType");
			std::string collisionType = lua_tostring(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "offset");
			float offset[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				offset[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				offset[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			lua_getfield(L, 2, "size");
			float size[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				size[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				size[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

			if (collisionType == "rectangle")
				cc->collisionType = Core::CollisionType::RECTANGLE;
			else if (collisionType == "circle")
				cc->collisionType = Core::CollisionType::CIRCLE;

			cc->offsetX = offset[0];
			cc->offsetY = offset[1];

			cc->width = size[0];
			cc->height = size[1];

		}

		return 0;
	}

	static int GetOffset(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

		lua_newtable(L);

		lua_pushnumber(L, cc->offsetX);
		lua_rawseti(L, -2, 0);

		lua_pushnumber(L, cc->offsetY);
		lua_rawseti(L, -2, 1);

		return 1;
	}

	static int SetOffset(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

		lua_rawgeti(L, 2, 0);
		cc->offsetX = luau_tofloat(L, -1);
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 1);
		cc->offsetY = luau_tofloat(L, -1);
		lua_pop(L, 1);

		return 0;
	}

	static int GetSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

		lua_newtable(L);

		lua_pushnumber(L, cc->width);
		lua_rawseti(L, -2, 0);

		lua_pushnumber(L, cc->height);
		lua_rawseti(L, -2, 1);

		return 1;
	}

	static int SetSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

		lua_rawgeti(L, 2, 0);
		cc->width = luau_tofloat(L, -1);
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 1);
		cc->height = luau_tofloat(L, -1);
		lua_pop(L, 1);

		return 0;
	}
}

namespace Core
{
	CollisionComponentBridge::CollisionComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddCollisionComponent", AddCollisionComponent);
		lua_register(coreState, "GetCollisionOffset", GetOffset);
		lua_register(coreState, "SetCollisionOffset", SetOffset);
		lua_register(coreState, "GetCollisionSize", GetSize);
		lua_register(coreState, "SetCollisionSize", SetSize);

	}
}