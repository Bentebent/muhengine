#include "InputBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int ButtonPressed(lua_State* L)
	{
		std::string keyCode = lua_tostring(L, 1);
		bool result = Core::world.m_inputHandler->ButtonPressed(Core::world.m_inputHandler->GetKeyFromString(keyCode));

		lua_pushboolean(L, result);

		return 1;
	}

	static int ButtonReleased(lua_State* L)
	{
		std::string keyCode = lua_tostring(L, 1);
		bool result = Core::world.m_inputHandler->ButtonReleased(Core::world.m_inputHandler->GetKeyFromString(keyCode));

		lua_pushboolean(L, result);

		return 1;
	}

	static int SingleButtonPress(lua_State* L)
	{
		std::string keyCode = lua_tostring(L, 1);
		bool result = Core::world.m_inputHandler->SingleButtonPress(Core::world.m_inputHandler->GetKeyFromString(keyCode));

		lua_pushboolean(L, result);

		return 1;
	}
}

namespace Core
{
	InputBridge::InputBridge(lua_State* coreState)
	{
		lua_register(coreState, "ButtonPressed", ButtonPressed);
		lua_register(coreState, "ButtonReleased", ButtonReleased);
		lua_register(coreState, "SingleButtonPress", SingleButtonPress);
	}
}