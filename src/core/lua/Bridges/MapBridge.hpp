#ifndef SRC_CORE_LUA_BRIDGES_MAPBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_MAPBRIDGE_HPP

struct lua_State;
namespace Core
{
	class MapBridge
	{
	public:
		MapBridge(lua_State* coreState);
	};
}

#endif