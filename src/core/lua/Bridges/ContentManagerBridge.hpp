#ifndef SRC_CORE_LUA_BRIDGES_CONTENTMANAGEMENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_CONTENTMANAGEMENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class ContentManagerBridge
	{
	public:
		ContentManagerBridge(lua_State* coreState);
	};
}


#endif