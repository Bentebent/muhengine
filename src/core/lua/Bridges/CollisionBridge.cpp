#include "CollisionBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int MapCollision(lua_State* L)
	{
		int argCount = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);

		float posX = 0;
		float posY = 0;

		

		Core::TransformationComponent* tc = WGETC<Core::TransformationComponent>(entity);
		Core::CollisionComponent* cc = WGETC<Core::CollisionComponent>(entity);

		

		float dirX = lua_tonumber(L, 2);
		float dirY = lua_tonumber(L, 3);

		posX = tc->position[0];
		posY = tc->position[1];

		if (argCount == 4)
		{
			lua_rawgeti(L, 4, 0);
			posX = luau_tofloat(L, -1);
			lua_pop(L, 1);

			lua_rawgeti(L, 4, 1);
			posY = luau_tofloat(L, -1);
			lua_pop(L, 1);
		}

		sf::FloatRect collisionBox = sf::FloatRect(posX + cc->offsetX - cc->width / 2.0f, posY + cc->offsetY - cc->height / 2.0f, cc->width, cc->height);

		Core::CollisionResult result = Core::world.m_collisionHandler->MapCollision(collisionBox, Core::world.m_map->GetCollisionLayer(), dirX, dirY);

		lua_pushinteger(L, result);
		return 1;
	}

	static int SetCollision(lua_State* L)
	{
		float x = lua_tonumber(L, 1);
		float y = lua_tonumber(L, 2);
		float w = lua_tonumber(L, 3);
		float h = lua_tonumber(L, 4);

		bool collisionOn = lua_toboolean(L, 5);

		sf::FloatRect collisionBox = sf::FloatRect(x, y, w, h);
		Core::world.m_collisionHandler->SetCollisionState(collisionBox, Core::world.m_map->GetCollisionLayer(), collisionOn);

		return 0;
	}
}

namespace Core
{
	CollisionBridge::CollisionBridge(lua_State* coreState)
	{
		lua_register(coreState, "MapCollision", MapCollision);
		lua_register(coreState, "SetCollision", SetCollision);
	}
}