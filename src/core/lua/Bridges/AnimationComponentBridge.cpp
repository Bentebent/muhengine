#include "AnimationComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <gfx/GFXBitmaskDefinitions.hpp>

extern "C"
{
	static int AddAnimationComponent(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::AnimationComponent());
		return 0;
	}

	static int SetAnimationSpeed(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		float speed = lua_tonumber(L, 2);

		Core::AnimationComponent* ac = WGETC<Core::AnimationComponent>(entity);

		ac->speed = speed;

		return 0;
	}

	static int GetAnimationSpeed(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::AnimationComponent* ac = WGETC<Core::AnimationComponent>(entity);

		lua_pushnumber(L, ac->speed);

		return 1;
	}
}

namespace Core
{
	AnimationComponentBridge::AnimationComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddAnimationComponent", AddAnimationComponent);
		lua_register(coreState, "SetAnimationSpeed", SetAnimationSpeed);
		lua_register(coreState, "GetAnimationSpeed", GetAnimationSpeed);
	}
}