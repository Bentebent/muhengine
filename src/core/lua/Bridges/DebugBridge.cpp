#include "DebugBridge.hpp"

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <gfx/GFXInterface.hpp>

extern "C"
{
	static int RenderDebugText(lua_State* L)
	{
		float x = lua_tonumber(L, 1);
		float y = lua_tonumber(L, 2);

		std::string text = lua_tostring(L, 3);

		int size = lua_tonumber(L, 4);
		float r = lua_tonumber(L, 5);
		float g = lua_tonumber(L, 6);
		float b = lua_tonumber(L, 7);
		float a = lua_tonumber(L, 8);


		gfx::debug::RenderText(x, y, text.c_str(), size, r, g, b, a);

		return 0;
	}

	static int RenderDebugCircle(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		float x = lua_tonumber(L, 1);
		float y = lua_tonumber(L, 2);
		float radius = lua_tonumber(L, 3);
		float r = lua_tonumber(L, 4);
		float g = lua_tonumber(L, 5);
		float b = lua_tonumber(L, 6);
		float a = lua_tonumber(L, 7);

		bool gui = true;

		if (nrOfArgs >= 8)
			gui = lua_toboolean(L, 8);

		gfx::debug::RenderCircle(x, y, radius, r, g, b, a, gui);

		return 0;
	}

	static int RenderDebugRectangle(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		float x = lua_tonumber(L, 1);
		float y = lua_tonumber(L, 2);
		float w = lua_tonumber(L, 3);
		float h = lua_tonumber(L, 4);
		float r = lua_tonumber(L, 5);
		float g = lua_tonumber(L, 6);
		float b = lua_tonumber(L, 7);
		float a = lua_tonumber(L, 8);

		bool gui = true;

		if (nrOfArgs >= 9)
			gui = lua_toboolean(L, 9);

		gfx::debug::RenderRectangle(x, y, w, h, r, g, b, a, gui);
		return 0;
	}

	static int RenderDebugPolygon(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		float x = lua_tonumber(L, 1);
		float y = lua_tonumber(L, 2);
		float radius = lua_tonumber(L, 3);
		float pointCount = lua_tonumber(L, 4);
		float r = lua_tonumber(L, 5);
		float g = lua_tonumber(L, 6);
		float b = lua_tonumber(L, 7);
		float a = lua_tonumber(L, 8);



		bool gui = true;
		
		if (nrOfArgs >= 9)
			gui = lua_toboolean(L, 9);

		gfx::debug::RenderPolygon(x, y, radius, pointCount, r, g, b, a, gui);

		return 0;
	}
}

namespace Core
{
	DebugBridge::DebugBridge(lua_State* coreState)
	{
		lua_register(coreState, "RenderDebugCircle", RenderDebugCircle);
		lua_register(coreState, "RenderDebugText", RenderDebugText);
		lua_register(coreState, "RenderDebugPolygon", RenderDebugPolygon);
		lua_register(coreState, "RenderDebugRectangle", RenderDebugRectangle);
	}
}