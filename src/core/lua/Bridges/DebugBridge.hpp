#ifndef SRC_CORE_LUA_BRIDGES_DEBUGBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_DEBUGBRIDGE_HPP

struct lua_State;
namespace Core
{
	class DebugBridge
	{
	public:
		DebugBridge(lua_State* coreState);
	};
}

#endif