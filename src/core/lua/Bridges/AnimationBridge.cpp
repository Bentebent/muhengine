#include "AnimationBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <Animation/AnimationManager.hpp>

extern "C"
{
	static int PlayAnimation(lua_State* L)
	{
		int argCount = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		std::string anim = lua_tostring(L, 2);
		bool looped = false;
		bool reverse = false;

		if (argCount > 2)
		{
			looped = lua_toboolean(L, 3);
			reverse = lua_toboolean(L, 4);
		}

		Core::AnimationManagerInstance().PlayAnimation(entity, anim, looped, reverse);

		return 0;
	}

	static int QueueAnimation(lua_State* L)
	{
		int argCount = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		std::string anim = lua_tostring(L, 2);
		bool looped = false;
		bool reverse = false;

		if (argCount > 2)
		{
			looped = lua_toboolean(L, 3);
			reverse = lua_toboolean(L, 4);
		}

		Core::AnimationManagerInstance().QueueAnimation(entity, anim, looped, reverse);
		return 0;
	}

	static int StepAnimation(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::AnimationManagerInstance().StepAnimation(entity);

		return 0;
	}

	static int PauseAnimation(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::AnimationManagerInstance().StepAnimation(entity);

		return 0;
	}

	static int ResumeAnimation(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::AnimationManagerInstance().StepAnimation(entity);

		return 0;
	}

	static int IsAnimationPlaying(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		std::string anim = lua_tostring(L, 2);

		bool isPlaying = Core::AnimationManagerInstance().IsAnimationPlaying(entity, anim);

		lua_pushboolean(L, isPlaying);
		return 1;
	}
}

namespace Core
{
	AnimationBridge::AnimationBridge(lua_State* coreState)
	{
		lua_register(coreState, "PlayAnimation", PlayAnimation);
		lua_register(coreState, "QueueAnimation", QueueAnimation);
		lua_register(coreState, "StepAnimation", StepAnimation);
		lua_register(coreState, "PauseAnimation", PauseAnimation);
		lua_register(coreState, "ResumeAnimation", ResumeAnimation);
		lua_register(coreState, "IsAnimationPlaying", IsAnimationPlaying);
	}
}