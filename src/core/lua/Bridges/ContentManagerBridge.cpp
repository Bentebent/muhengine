#include "ContentManagerBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <gfx/GFXInterface.hpp>
#include <ContentManagement/ContentManager.hpp>
#include <ContentManagement/ContentLoadPolicyDefinition.hpp>
#include <ContentManagement/Loaders/TextureLoader.hpp>
#include <ContentManagement/Loaders/FontLoader.hpp>
#include <ContentManagement/Loaders/AnimationLoader.hpp>

extern "C"
{
	static int LoadTexture(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int textureId;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::BLOCKING);
		textureId = ((Core::TextureData*)Core::contentManager.GetContentData(path))->textureID;
		lua_pushnumber(L, textureId);
		return 1;
	}

	static int LoadTextureAsync(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int textureId;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::NON_BLOCKING);
		textureId = ((Core::TextureData*)Core::contentManager.GetContentData(path))->textureID;
		lua_pushnumber(L, textureId);
		return 1;
	}

	static int LoadFont(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int fontId;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::BLOCKING);
		fontId = ((Core::FontData*)Core::contentManager.GetContentData(path))->fontID;
		lua_pushnumber(L, fontId);
		return 1;
	}

	static int LoadFontAsync(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int fontId;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::NON_BLOCKING);
		fontId = ((Core::FontData*)Core::contentManager.GetContentData(path))->fontID;
		lua_pushnumber(L, fontId);
		return 1;
	}

	static int LoadAnimation(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int textureID;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::BLOCKING);
		textureID = ((Core::AnimationData*)Core::contentManager.GetContentData(path))->textureID;

		lua_pushnumber(L, textureID);
		return 1;
	}

	static int LoadAnimationAsync(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		unsigned int textureID;

		Core::contentManager.Load(path, Core::ContentLoadPolicy::NON_BLOCKING);
		textureID = ((Core::AnimationData*)Core::contentManager.GetContentData(path))->textureID;

		lua_pushnumber(L, textureID);
		return 1;
	}

	static int ReloadContent(lua_State* L)
	{
		Core::contentManager.Reload();
		return 0;
	}
}

namespace Core
{
	ContentManagerBridge::ContentManagerBridge(lua_State* coreState)
	{
		lua_register(coreState, "LoadTexture", LoadTexture);
		lua_register(coreState, "LoadTextureAsync", LoadTextureAsync);
		lua_register(coreState, "LoadFont", LoadFont);
		lua_register(coreState, "LoadFontAsync", LoadFontAsync);
		lua_register(coreState, "LoadAnimation", LoadAnimation);
		lua_register(coreState, "LoadAnimationAsync", LoadAnimationAsync);
		lua_register(coreState, "ReloadContent", ReloadContent);
	}
}