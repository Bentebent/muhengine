#ifndef SRC_CORE_LUA_BRIDGES_COLLISIONBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_COLLISIONBRIDGE_HPP

struct lua_State;

namespace Core
{
	class CollisionBridge
	{
	public:
		CollisionBridge(lua_State* coreState);
	};
}

#endif