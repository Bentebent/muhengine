#include "GFXBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <gfx/GFXInterface.hpp>
#include <lua/LuaMetatableTypes.hpp>

extern "C"
{
	static int SetView(lua_State* L)
	{
		int id = lua_tointeger(L, 1);

		float x = lua_tonumber(L, 2);
		float y = lua_tonumber(L, 3);
		float w = lua_tonumber(L, 4);
		float h = lua_tonumber(L, 5);

		float vx = lua_tonumber(L, 6);
		float vy = lua_tonumber(L, 7);
		float vw = lua_tonumber(L, 8);
		float vh = lua_tonumber(L, 9);

		sf::View v;
		v.setCenter(x, y);
		v.setSize(w, h);
		v.setViewport(sf::FloatRect(vx, vy, vw, vh));

		gfx::view::SetView((void*)&v, id);

		return 0;
	}

	static int SetViewPosition(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		int id = lua_tointeger(L, 1);

		float x = lua_tonumber(L, 2);
		float y = lua_tonumber(L, 3);

		if (nrOfArgs >= 4)
		{
			bool clampToMap = lua_toboolean(L, 4);

			if (clampToMap)
			{
				float mapWidth = Core::world.m_map->GetMapWidthPixels();
				float mapHeight = Core::world.m_map->GetMapHeightPixels();

				float scaledHalfWidth = Core::world.m_map->GetScaledTileWidth() / 2.0f;
				float scaledHalfHeight = Core::world.m_map->GetScaledTileHeight() / 2.0f;

				sf::View* view = nullptr;
				gfx::view::GetView((void*&)view, id);

				sf::Vector2f size = view->getSize();

				float xMax = x + size.x / 2.0f;
				float xMin = x - size.x / 2.0f;

				if (xMax > mapWidth && xMin < 0)
				{
					//Do nothing, map is too small/view is too big
				}
				else if (xMin < 0)
				{
					x = x - xMin - scaledHalfWidth;
				}
				else if (xMax > mapWidth - scaledHalfWidth)
				{
					x = x - (xMax - mapWidth) - scaledHalfWidth;
				}

				float yMax = y + size.y / 2.0f;
				float yMin = y - size.y / 2.0f;

				if (yMax > mapHeight && yMin < 0)
				{
					//Do nothing, map is too small/view is too big
				}
				else if (yMin < 0)
				{
					y = y - yMin - scaledHalfHeight;
				}
				else if (yMax > mapHeight - scaledHalfHeight)
				{
					y = y - (yMax - mapHeight) - scaledHalfHeight;
				}
			}
		}


		gfx::view::SetViewPosition(x, y, id);

		return 0;
	}

	static int SetPrimaryView(lua_State* L)
	{
		int id = lua_tointeger(L, 1);
		gfx::view::SetPrimaryView(id);
		return 0;
	}

	static int GetView(lua_State* L)
	{
		int id = lua_tonumber(L, 1);

		sf::View* v;

		gfx::view::GetView((void*&)v, id);
		Core::LuaNewSFMLView(L, *v);

		return 1;
	}

	static int GetPrimaryView(lua_State* L)
	{
		sf::View* v;
		int id = -1;
		gfx::view::GetPrimaryView((void*&)v, id);

		Core::LuaNewSFMLView(L, *v);
		return 1;
	}
}

namespace Core
{
	GFXBridge::GFXBridge(lua_State* coreState)
	{
		lua_register(coreState, "SetView", SetView);
		lua_register(coreState, "SetViewPosition", SetViewPosition);
		lua_register(coreState, "SetPrimaryView", SetPrimaryView);
		lua_register(coreState, "GetPrimaryView", GetPrimaryView);
		lua_register(coreState, "GetView", GetView);
	}
}