#ifndef SRC_CORE_LUA_BRIDGES_RENDERINGCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_RENDERINGCOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class RenderingComponentBridge
	{
	public:
		RenderingComponentBridge(lua_State* coreState);
	};
}

#endif