#ifndef SRC_CORE_LUA_BRIDGES_TESTCOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_TESTCOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class TestComponentBridge
	{
	public:
		TestComponentBridge(lua_State* coreState);
	};
}

#endif