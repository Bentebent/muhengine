#include "ShapeComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int AddShapeComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::ShapeComponent());

		if (nrOfArgs > 1)
		{
			lua_getfield(L, 2, "shapeType");
			int shapeType = lua_tonumber(L, -1);

			Core::ShapeComponent* shapeComponent = WGETC<Core::ShapeComponent>(entity);

			switch (shapeType)
			{
				case gfx::ShapeType::CIRCLE:
				{
					lua_getfield(L, 3, "outlineColor");
					int outlineColor[4];
					if (lua_istable(L, -1))
					{
						lua_rawgeti(L, -1, 1);
						outlineColor[0] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 2);
						outlineColor[1] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 3);
						outlineColor[2] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 4);
						outlineColor[3] = lua_tonumber(L, -1);
						lua_pop(L, 1);
					}

					lua_getfield(L, 3, "radius");
					float radius = lua_tonumber(L, -1);
					lua_pop(L, 1);

					lua_getfield(L, 3, "outlineThickness");
					float outlineThickness = lua_tonumber(L, -1);
					lua_pop(L, 1);

					shapeComponent->shapeType			= shapeType;
					shapeComponent->radius				= radius;
					shapeComponent->outlineThickness	= outlineThickness;

					for (int i = 0; i < 4; i++)
						shapeComponent->outlineColor[i] = outlineColor[i];
				}
				break;

				case gfx::ShapeType::RECTANGLE:
				{
					lua_getfield(L, 3, "outlineColor");
					int outlineColor[4];
					if (lua_istable(L, -1))
					{
						lua_rawgeti(L, -1, 1);
						outlineColor[0] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 2);
						outlineColor[1] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 3);
						outlineColor[2] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 4);
						outlineColor[3] = lua_tonumber(L, -1);
						lua_pop(L, 1);
					}

					lua_getfield(L, 3, "rectangleSize");
					float rectangleSize[2];
					if (lua_istable(L, -1))
					{
						lua_rawgeti(L, -1, 1);
						rectangleSize[0] = luau_tofloat(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 2);
						rectangleSize[1] = luau_tofloat(L, -1);
						lua_pop(L, 1);
					}

					lua_getfield(L, 3, "outlineThickness");
					float outlineThickness = lua_tonumber(L, -1);
					lua_pop(L, 1);

					shapeComponent->shapeType = shapeType;
					shapeComponent->outlineThickness = outlineThickness;

					for (int i = 0; i < 4; i++)
						shapeComponent->outlineColor[i] = outlineColor[i];
					for (int i = 0; i < 2; i++)
						shapeComponent->rectangleSize[i] = rectangleSize[i];

				}
				break;

				case gfx::ShapeType::POLYGON:
				{
					lua_getfield(L, 3, "outlineColor");
					int outlineColor[4];
					if (lua_istable(L, -1))
					{
						lua_rawgeti(L, -1, 1);
						outlineColor[0] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 2);
						outlineColor[1] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 3);
						outlineColor[2] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 4);
						outlineColor[3] = lua_tonumber(L, -1);
						lua_pop(L, 1);
					}

					lua_getfield(L, 3, "radius");
					float radius = lua_tonumber(L, -1);
					lua_pop(L, 1);

					lua_getfield(L, 3, "outlineThickness");
					float outlineThickness = lua_tonumber(L, -1);
					lua_pop(L, 1);

					lua_getfield(L, 3, "pointCount");
					int pointCount = lua_tointeger(L, -1);
					lua_pop(L, 1);

					shapeComponent->shapeType			= shapeType;
					shapeComponent->radius				= radius;
					shapeComponent->outlineThickness	= outlineThickness;
					shapeComponent->pointCount			= pointCount;

					for (int i = 0; i < 4; i++)
						shapeComponent->outlineColor[i] = outlineColor[i];
				}
				break;

				case gfx::ShapeType::CONVEX:
				{
					lua_getfield(L, 3, "outlineThickness");
					float outlineThickness = lua_tonumber(L, -1);
					lua_pop(L, 1);

					lua_getfield(L, 3, "pointCount");
					int pointCount = lua_tointeger(L, -1);
					lua_pop(L, 1);

					lua_getfield(L, 3, "outlineColor");
					int outlineColor[4];
					if (lua_istable(L, -1))
					{
						lua_rawgeti(L, -1, 1);
						outlineColor[0] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 2);
						outlineColor[1] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 3);
						outlineColor[2] = lua_tonumber(L, -1);
						lua_pop(L, 1);

						lua_rawgeti(L, -1, 4);
						outlineColor[3] = lua_tonumber(L, -1);
						lua_pop(L, 1);
					}

					lua_getfield(L, 3, "convexPoints");
					float** convexPoints = new float*[pointCount];

					if (lua_istable(L, -1))
					{
						for (int i = 1; i <= pointCount; i++)
						{
							lua_rawgeti(L, -1, i);
							convexPoints[i - 1] = new float[2];
							for (int j = 1; j <= 2; j++)
							{
								lua_rawgeti(L, -1, j);
								convexPoints[i-1][j-1] = lua_tonumber(L, -1);
								lua_pop(L, 1);
							}

							lua_pop(L, 1);
						}
						
					}

					shapeComponent->shapeType = shapeType;
					shapeComponent->outlineThickness = outlineThickness;
					shapeComponent->pointCount = pointCount;

					for (int i = 0; i < 4; i++)
						shapeComponent->outlineColor[i] = outlineColor[i];

					for (int i = 0; i < pointCount; i++)
					{
						for (int j = 0; j < pointCount; j++)
						{
							shapeComponent->convexPoints[i][j] = convexPoints[i][j];
						}

						delete[] convexPoints[i];
					}

					delete[] convexPoints;
				}
				break;

				case gfx::ShapeType::CUSTOM:
				{

				}
				break;
			}
		}
		return 0;
	}

	static int SetRadius(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		float radius = luau_tofloat(L, 2);
		sc->radius = radius;

		return 0;
	}

	static int SetShapeType(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		int shapeType = lua_tointeger(L, 2);
		sc->shapeType = shapeType;

		return 0;
	}

	static int SetPointCount(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		int pointCount = lua_tointeger(L, 2);
		sc->pointCount = pointCount;

		return 0;
	}

	static int SetOutlineThickness(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		int outlineThickness = lua_tointeger(L, 2);
		sc->outlineThickness = outlineThickness;

		return 0;
	}

	static int SetConvexPoints(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		int pointCount = lua_tointeger(L, 2);
		

		float** convexPoints = new float*[pointCount];

		if (lua_istable(L, 3))
		{
			for (int i = 1; i <= pointCount; i++)
			{
				lua_rawgeti(L, 3, i);
				convexPoints[i - 1] = new float[2];
				for (int j = 1; j <= 2; j++)
				{
					lua_rawgeti(L, -1, j);
					convexPoints[i - 1][j - 1] = lua_tonumber(L, -1);
					lua_pop(L, 1);
				}
				lua_pop(L, 1);
			}
		}

		sc->pointCount = pointCount;

		for (int i = 0; i < pointCount; i++)
		{
			for (int j = 0; j < pointCount; j++)
			{
				sc->convexPoints[i][j] = convexPoints[i][j];
			}

			delete[] convexPoints[i];
		}

		delete[] convexPoints;

		return 0;
	}

	static int SetRectangleSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		for (int i = 0; i < 2; i++)
		{
			lua_rawgeti(L, 2, i);
			sc->rectangleSize[i] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}

		return 0;
	}

	static int SetOutlineColor(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		for (int i = 0; i < 4; i++)
		{
			lua_rawgeti(L, 2, i);
			sc->outlineColor[i] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}

		return 0;
	}

	static int GetRadius(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_pushnumber(L, sc->radius);

		return 1;
	}

	static int GetShapeType(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_pushnumber(L, sc->shapeType);

		return 1;
	}

	static int GetPointCount(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_pushnumber(L, sc->pointCount);

		return 1;
	}

	static int GetOutlineThickness(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_pushnumber(L, sc->outlineThickness);

		return 1;
	}

	static int GetConvexPoints(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_pushnumber(L, sc->pointCount);

		lua_createtable(L, sc->pointCount, 0);

		for (int i = 0; i < sc->pointCount; i++)
		{
			lua_pushnumber(L, i);
			for (int j = 0; j < 2; j++)
			{ 
				lua_pushnumber(L, sc->convexPoints[i][j]);
				lua_rawseti(L, -2, j);
			}

			lua_settable(L, -2);
		}

		return 2;
	}

	static int GetRectangleSize(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, sc->rectangleSize[i]);
			lua_rawseti(L, -2, i);
		}

		return 0;
	}

	static int GetOutlineColor(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::ShapeComponent* sc = WGETC<Core::ShapeComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 4; i++)
		{
			lua_pushnumber(L, sc->outlineColor[i]);
			lua_rawseti(L, -2, i);
		}

		return 0;
	}
}

namespace Core
{
	ShapeComponentBridge::ShapeComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddShapeComponent",		AddShapeComponent);
		lua_register(coreState, "SetRadius",				SetRadius);
		lua_register(coreState, "SetShapeType",				SetShapeType);
		lua_register(coreState, "SetPointCount",			SetPointCount);
		lua_register(coreState, "SetOutlineThickness",		SetOutlineThickness);
		lua_register(coreState, "SetConvexPoints",			SetConvexPoints);
		lua_register(coreState, "SetRectangleSize",			SetRectangleSize);
		lua_register(coreState, "SetOutlineColor",			SetOutlineColor);
		lua_register(coreState, "GetRadius",				GetRadius);
		lua_register(coreState, "GetShapeType",				GetShapeType);
		lua_register(coreState, "GetPointCount",			GetPointCount);
		lua_register(coreState, "GetOutlineThickness",		GetOutlineThickness);
		lua_register(coreState, "GetConvexPoints",			GetConvexPoints);
		lua_register(coreState, "GetRectangleSize",			GetRectangleSize);
		lua_register(coreState, "GetOutlineColor",			GetOutlineColor);
	}
}