#include "RenderingComponentBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <bitmask/bitmask_types.hpp>
#include <glm/glm.hpp>

extern "C"
{
	static int AddRenderingComponent(lua_State* L)
	{
		int nrOfArgs = lua_gettop(L);

		Core::Entity entity = lua_tonumber(L, 1);
		Core::world.m_entityHandler.AddComponents(entity, Core::RenderingComponent());

		if (nrOfArgs > 1)
		{
			luaL_checktype(L, 2, LUA_TTABLE);
			lua_getfield(L, 2, "bitmask");
			Bitmask bitmask = *(uint64_t*)lua_touserdata(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "renderMe");
			bool renderMe = lua_toboolean(L, -1);
			lua_pop(L, 1);

			lua_getfield(L, 2, "color");
			float color[4];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				color[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				color[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 3);
				color[2] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 4);
				color[3] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			lua_getfield(L, 2, "sourceRectangle");
			int sourceRectangle[4];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				sourceRectangle[0] = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				sourceRectangle[1] = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 3);
				sourceRectangle[2] = lua_tonumber(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 4);
				sourceRectangle[3] = lua_tonumber(L, -1);
				lua_pop(L, 1);
			}

			lua_getfield(L, 2, "origin");
			float origin[2];
			if (lua_istable(L, -1))
			{
				lua_rawgeti(L, -1, 1);
				origin[0] = luau_tofloat(L, -1);
				lua_pop(L, 1);

				lua_rawgeti(L, -1, 2);
				origin[1] = luau_tofloat(L, -1);
				lua_pop(L, 1);
			}

			Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
			
			rc->bitmask = bitmask;
			rc->renderMe = renderMe;
			

			for (int i = 0; i < 4; i++)
			{
				rc->color[i] = color[i];
				rc->sourceRectangle[i] = sourceRectangle[i];
			}

			for (int i = 0; i < 2; i++)
			{
				rc->origin[i] = origin[i];
			}
		}
		return 0;
	}

	static int GetOrigin(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, rc->origin[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int SetOrigin(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		for (int i = 0; i < 2; i++)
		{
			lua_rawgeti(L, 2, i);
			rc->origin[i] = luau_tofloat(L, -1);
			lua_pop(L, 1);
		}
		return 0;
	}

	static int GetSourceRectangle(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		lua_newtable(L);

		for (int i = 0; i < 4; i++)
		{
			lua_pushnumber(L, rc->sourceRectangle[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int SetSourceRectangle(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		for (int i = 0; i < 4; i++)
		{
			lua_rawgeti(L, 2, i);
			rc->sourceRectangle[i] = lua_tonumber(L, -1);
			lua_pop(L, 1);
		}
		return 0;
	}

	static int GetColour(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		lua_newtable(L);
		
		for (int i = 0; i < 4; i++)
		{
			lua_pushnumber(L, rc->color[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int SetColour(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		
		lua_rawgeti(L, 2, 0);
		int r = lua_tonumber(L, -1);
		r = glm::clamp(r, 0, 255);
		rc->color[0] = r;
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 1);
		int g = lua_tonumber(L, -1);
		g = glm::clamp(g, 0, 255);
		rc->color[1] = g;
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 2);
		int b = lua_tonumber(L, -1);
		b = glm::clamp(b, 0, 255);
		rc->color[2] = b;
		lua_pop(L, 1);

		lua_rawgeti(L, 2, 3);
		int a = lua_tonumber(L, -1);
		a = glm::clamp(a, 0, 255);
		rc->color[3] = a;
		lua_pop(L, 1);
		
		return 0;
	}

	static int GetBitmask(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		lua_pushnumber(L, rc->bitmask);
		return 1;
	}

	static int SetBitmask(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Bitmask bitmask = lua_tonumber(L, 2);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		rc->bitmask = bitmask;

		return 0;
	}

	static int SetRenderMe(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		bool renderMe = lua_toboolean(L, 2);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		rc->renderMe = renderMe;

		return 0;
	}

	static int GetRenderMe(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);

		lua_pushboolean(L, rc->renderMe);
		return 1;
	}

	static int SetFlippedX(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		bool flipped = lua_toboolean(L, 2);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		rc->flippedX = flipped;
		return 0;
	}

	static int GetFlippedX(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		lua_pushboolean(L, rc->flippedX);
		return 1;
	}

	static int GetFlippedY(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		lua_pushboolean(L, rc->flippedY);
		return 1;
	}

	static int SetFlippedY(lua_State* L)
	{
		Core::Entity entity = lua_tonumber(L, 1);
		bool flipped = lua_toboolean(L, 2);

		Core::RenderingComponent* rc = WGETC<Core::RenderingComponent>(entity);
		rc->flippedY = flipped;
		return 0;
	}

}

namespace Core
{
	RenderingComponentBridge::RenderingComponentBridge(lua_State* coreState)
	{
		lua_register(coreState, "AddRenderingComponent", AddRenderingComponent);

		lua_register(coreState, "SetRenderMe", SetRenderMe);
		lua_register(coreState, "GetRenderMe", GetRenderMe);

		lua_register(coreState, "SetBitmask", SetBitmask);
		lua_register(coreState, "GetBitmask", GetBitmask);

		lua_register(coreState, "SetColour", SetColour);
		lua_register(coreState, "GetColour", GetColour);

		lua_register(coreState, "SetSourceRectangle", SetSourceRectangle);
		lua_register(coreState, "GetSourceRectangle", GetSourceRectangle);

		lua_register(coreState, "GetOrigin", GetOrigin);
		lua_register(coreState, "SetOrigin", SetOrigin);

		lua_register(coreState, "SetFlippedX", SetFlippedX);
		lua_register(coreState, "GetFlippedX", GetFlippedX);

		lua_register(coreState, "SetFlippedY", SetFlippedY);
		lua_register(coreState, "GetFlippedY", GetFlippedY);

		
	}
}