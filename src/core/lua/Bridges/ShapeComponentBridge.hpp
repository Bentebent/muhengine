#ifndef SRC_CORE_LUA_BRIDGES_SHAPECOMPONENTBRIDGE_HPP
#define SRC_CORE_LUA_BRIDGES_SHAPECOMPONENTBRIDGE_HPP

struct lua_State;
namespace Core
{
	class ShapeComponentBridge
	{
	public:
		ShapeComponentBridge(lua_State* coreState);
	};
}

#endif