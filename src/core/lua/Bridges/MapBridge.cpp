#include "MapBridge.hpp"

#include <World.hpp>

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

extern "C"
{
	static int LoadMap(lua_State* L)
	{
		std::string path = lua_tostring(L, 1);
		Core::world.m_tiledParser->LoadMap(path, Core::world.m_map);
		//Core::world.m_map = Core::world.m_tiledParser->LoadMap(path);
		//Core::world.m_map->Finalize();
		return 0;
	}

	static int UnloadMap(lua_State* L)
	{
		Core::world.m_levelHeap.Rewind();
		return 0;
	}

	static int GetTilePosition(lua_State* L)
	{
		float pos[2];
		for (int i = 0; i < 2; i++)
		{
			lua_rawgeti(L, 1, i);
			pos[i] = luau_tofloat(L, -1);
			lua_pop(L, 1);
		}

		int tPos[2];

		Core::world.m_map->GetTilePosition(pos[0], pos[1], tPos[0], tPos[1]);

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, tPos[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int GetScaledTileSize(lua_State* L)
	{
		float size[2];

		size[0] = Core::world.m_map->GetScaledTileWidth();
		size[1] = Core::world.m_map->GetScaledTileHeight();

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, size[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int GetTileScale(lua_State* L) 
	{
		float scale[2];

		scale[0] = Core::world.m_map->GetTileScaleX();
		scale[1] = Core::world.m_map->GetTileScaleY();

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, scale[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}
	
	static int GetTileSize(lua_State* L) 
	{ 
		float size[2];

		size[0] = Core::world.m_map->GetTileWidth();
		size[1] = Core::world.m_map->GetTileHeight();

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, size[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}

	static int GetMapSize(lua_State* L) 
	{
		float size[2];

		size[0] = Core::world.m_map->GetMapWidth();
		size[1] = Core::world.m_map->GetMapHeight();

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, size[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}
	
	static int GetMapSizePixels(lua_State* L) 
	{
		float size[2];

		size[0] = Core::world.m_map->GetMapWidthPixels();
		size[1] = Core::world.m_map->GetMapHeightPixels();

		lua_newtable(L);

		for (int i = 0; i < 2; i++)
		{
			lua_pushnumber(L, size[i]);
			lua_rawseti(L, -2, i);
		}

		return 1;
	}


}

namespace Core
{
	MapBridge::MapBridge(lua_State* coreState)
	{
		lua_register(coreState, "LoadMap", LoadMap);
		lua_register(coreState, "UnloadMap", UnloadMap);
		lua_register(coreState, "GetTilePosition", GetTilePosition);
		lua_register(coreState, "GetScaledTileSize", GetScaledTileSize);
		lua_register(coreState, "GetTileScale", GetTileScale);
		lua_register(coreState, "GetTileSize", GetTileSize);
		lua_register(coreState, "GetMapSize", GetMapSize);
		lua_register(coreState, "GetMapSizePixels", GetMapSizePixels);
	}
}