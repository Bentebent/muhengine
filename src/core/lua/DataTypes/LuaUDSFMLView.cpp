#include "LuaUDSFMLView.hpp"

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <lua/LuaMetatableTypes.hpp>

extern "C"
{
	static sf::View* ToSFMLView(lua_State* L, int index)
	{
		sf::View* v = (sf::View*)lua_touserdata(L, index);
		
		if (v == nullptr)
			luaL_typerror(L, index, SFML_VIEW_METATYPE);

		return v;
	}

	static sf::View* CheckView(lua_State* L, int index)
	{
		sf::View* v;
		luaL_checktype(L, index, LUA_TUSERDATA);

		v = (sf::View*)luaL_checkudata(L, index, SFML_VIEW_METATYPE);
		
		if (v == nullptr)
			luaL_typerror(L, index, SFML_VIEW_METATYPE);

		return v;
	}

	static sf::View* PushView(lua_State* L)
	{
		sf::View* v = (sf::View*)lua_newuserdata(L, sizeof(sf::View));
		
		luaL_getmetatable(L, SFML_VIEW_METATYPE);
		lua_setmetatable(L, -2);

		return v;
	}

	static int SFVIEW_getCenter(lua_State* L)
	{
		sf::View* v = CheckView(L, 1);

		sf::Vector2f center = v->getCenter();

		lua_newtable(L);

		lua_pushnumber(L, center.x);
		lua_rawseti(L, -2, 0);

		lua_pushnumber(L, center.y);
		lua_rawseti(L, -2, 1);

		return 1;
	}

	static int SFVIEW_new(lua_State* L)
	{
		sf::View* v = PushView(L);
		
		v->setCenter(0, 0);
		v->setSize(320, 240);
		v->setViewport(sf::FloatRect(0, 0, 1.0f, 1.0f));

		return 1;
	}

	static int SFVIEW_getSize(lua_State* L)
	{
		sf::View* v = CheckView(L, 1);

		sf::Vector2f size = v->getSize();

		lua_newtable(L);

		lua_pushnumber(L, size.x);
		lua_rawseti(L, -2, 0);

		lua_pushnumber(L, size.y);
		lua_rawseti(L, -2, 1);

		return 1;
	}

	static int SFVIEW_tostring(lua_State* L)
	{
		char buff[32];
		sprintf(buff, "%p", ToSFMLView(L, 1));
		lua_pushfstring(L, "SFMLView (%s)", buff);
		return 1;
	}
}


namespace Core
{
	LuaUDSFMLView::LuaUDSFMLView(lua_State* coreState)
	{
		lua_newtable(coreState);
			int sfview_table = lua_gettop(coreState);
			luau_setfunction(coreState, "new", SFVIEW_new);
			luau_setfunction(coreState, "getSize", SFVIEW_getSize);
			luau_setfunction(coreState, "getCenter", SFVIEW_getCenter)

			luaL_newmetatable(coreState, SFML_VIEW_METATYPE);
				lua_pushvalue(coreState, sfview_table);
				lua_setfield(coreState, -2, "__index");
				luau_setfunction(coreState, "__tostring", SFVIEW_tostring);
			lua_pop(coreState, 1);
		lua_setglobal(coreState, "SFView");
	}
}