#ifndef SRC_CORE_LUA_DATATYPES_LUAUDSFMLVIEW_HPP
#define SRC_CORE_LUA_DATATYPES_LUAUDSFMLVIEW_HPP

struct lua_State;

namespace Core
{
	class LuaUDSFMLView
	{
	public:
		LuaUDSFMLView(lua_State* coreState);
	};
}

#endif
