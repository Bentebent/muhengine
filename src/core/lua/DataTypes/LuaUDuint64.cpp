#include "LuaUDuint64.hpp"

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>
#include <lua/luaUtility.hpp>

#include <lua/LuaMetatableTypes.hpp>

extern "C"
{
	/* get value of uint64 userdata or Lua number at index, or die */
	static int checkuint64(lua_State *L, int index)
	{
		if (lua_isuserdata(L, index) && luaL_checkudata(L, index, UINT64_METATYPE))
		{
			return *(uint64_t*)lua_touserdata(L, index);
		}
		else if (lua_isnumber(L, index))
		{
			return lua_tonumber(L, index);
		}
		else
		{
			lua_pushstring(L, "Invalid operand. Expected 'integer' or 'number'");
			lua_error(L);
			return 0; /* will never get here */
		}
	}

	static int create_uint64(lua_State* L, uint64_t val)
	{
		uint64_t* ud = (uint64_t*)lua_newuserdata(L, sizeof(uint64_t));
		*ud = val;
		luaL_getmetatable(L, UINT64_METATYPE);
		lua_setmetatable(L, -2);
		return 1;
	}

	static int uint64_new(lua_State* L) { return create_uint64(L, checkuint64(L, 1)); }
	static int uint64_add(lua_State* L) { return create_uint64(L, checkuint64(L, 1) + checkuint64(L, 2)); }
	static int uint64_sub(lua_State* L) { return create_uint64(L, checkuint64(L, 1) - checkuint64(L, 2)); }
	static int uint64_mul(lua_State* L) { return create_uint64(L, checkuint64(L, 1) * checkuint64(L, 2)); }
	static int uint64_div(lua_State* L) { return create_uint64(L, checkuint64(L, 1) / checkuint64(L, 2)); }
	static int uint64_mod(lua_State* L) { return create_uint64(L, checkuint64(L, 1) % checkuint64(L, 2)); }
	static int uint64_pow(lua_State* L) { return create_uint64(L, pow(checkuint64(L, 1), checkuint64(L, 2))); }
	static int uint64_unm(lua_State* L) { return create_uint64(L, -checkuint64(L, 1)); }
	static int uint64_eq(lua_State* L) { lua_pushboolean(L, checkuint64(L, 1) == checkuint64(L, 2)); return 1; }
	static int uint64_lt(lua_State* L) { lua_pushboolean(L, checkuint64(L, 1) <  checkuint64(L, 2)); return 1; }
	static int uint64_le(lua_State* L) { lua_pushboolean(L, checkuint64(L, 1) <= checkuint64(L, 2)); return 1; }

	static int uint64_tostring(lua_State* L) {
		lua_pushnumber(L, checkuint64(L, 1));
		lua_tostring(L, -1);
		return 1;
	}

}


namespace Core
{
	LuaUDuint64::LuaUDuint64(lua_State* coreState)
	{
		static const struct luaL_reg integermt[] = {
			{ "__add", uint64_add },
			{ "__sub", uint64_sub },
			{ "__mul", uint64_mul },
			{ "__div", uint64_div },
			{ "__mod", uint64_mod },
			{ "__pow", uint64_pow },
			{ "__unm", uint64_unm },
			{ "__eq",  uint64_eq },
			{ "__lt",  uint64_lt },
			{ "__le",  uint64_le },
			{ "__tostring", uint64_tostring },
			NULL, NULL
		};
		luaL_newmetatable(coreState, UINT64_METATYPE);
		luaL_openlib(coreState, NULL, integermt, 0);

		lua_register(coreState, "uint64", uint64_new);
	}
}