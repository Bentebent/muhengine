#ifndef SRC_CORE_LUA_DATATYPES_LUAUDUINT64_HPP
#define SRC_CORE_LUA_DATATYPES_LUAUDUINT64_HPP

struct lua_State;

namespace Core
{
	class LuaUDuint64
	{
	public:
		LuaUDuint64(lua_State* coreState);
	};
}

#endif
