#include "LuaMetatableTypes.hpp"

namespace Core
{
	uint64_t* LuaNewUint64(lua_State* L, uint64_t val)
	{
		uint64_t* ud = (uint64_t*)lua_newuserdata(L, sizeof(uint64_t));
		*ud = val;
		luaL_getmetatable(L, UINT64_METATYPE);
		lua_setmetatable(L, -2);
		return ud;
	}

	sf::View* LuaNewSFMLView(lua_State* L, sf::View view)
	{
		sf::View* v = (sf::View*)lua_newuserdata(L, sizeof(sf::View));
		*v = view;
		luaL_getmetatable(L, SFML_VIEW_METATYPE);
		lua_setmetatable(L, -2);
		return v;
	}
}