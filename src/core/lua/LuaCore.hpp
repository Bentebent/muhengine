#ifndef SRC_CORE_LUA_LUACORE_HPP
#define SRC_CORE_LUA_LUACORE_HPP

#include "LuaCallScripts.hpp"
#include "LuaUtility.hpp"
#include "LuaBindingContainer.hpp"

#include <lua/lua.hpp>
#include <lua/lualib.h>
#include <lua/lauxlib.h>

#include <string>
#include <vector>
#include <map>
#include <iostream>

namespace Core
{
	class LuaCore
	{
			
	public:
		LuaCore();
		~LuaCore();
	
		bool Initialize();
		int LoadFile(std::string file, bool coreState);
		void ReloadAll();
		void Close();
		void ClearChildren();
		void RemoveChild(int index);

		LUA_CALL GetFunction(int index);

		lua_State* GetCoreState() { return m_luaState; }

	private:									
		
		void LoadMainState();

		lua_State* CreateThread(std::string file, int& refKey);

		lua_State* m_luaState;
		std::string m_coreStatePath;

		int m_stateCounter;
		std::map<int, ChildState>* m_childLuaStates;

		LuaCallScripts* m_callScripts;
	};
}

#endif