#include "LuaCore.hpp"

namespace Core
{
	LuaCore::LuaCore()
	{

	}

	LuaCore::~LuaCore()
	{

	}

	void LuaCore::LoadMainState()
	{
		m_luaState = lua_open();
		luaL_openlibs(m_luaState);
	}


	bool LuaCore::Initialize()
	{
		m_stateCounter = 0;
		m_childLuaStates = new std::map<int, ChildState>();

		LoadMainState();
		
		m_callScripts = new LuaCallScripts();
		m_callScripts->Initialize(m_luaState, m_childLuaStates);

		LuaBindingContainer bindingInit(m_luaState);

		return false;
	}

	int LuaCore::LoadFile(std::string file, bool coreState)
	{
		int out = 0;
		if (coreState)
		{
			luaL_dofile(m_luaState, file.c_str());
			m_coreStatePath = file;
		}
		else
		{
			//http://lua-users.org/lists/lua-l/2003-10/msg00276.html
			//newState is an independent thread, but its globals
			//table is still the same as m_luaState's globals table,
			//so we'll replace it with a new table.
			//The new table is set up so it will still look
			//to m_luaState's globals for items it doesn't contain.

			int ref = -1;
			lua_State* newState = CreateThread(file, ref);

			m_childLuaStates->emplace(m_stateCounter, ChildState(newState, file, ref));
			out = m_stateCounter;
			m_stateCounter++;
		}

		return out;
	}

	lua_State* LuaCore::CreateThread(std::string file, int& refKey)
	{
		lua_State* newState = lua_newthread(m_luaState);
		refKey = luaL_ref(m_luaState, LUA_REGISTRYINDEX);

		lua_newtable(newState); //new globals table
		lua_newtable(newState); //metatable

		lua_pushliteral(newState, "__index");
		lua_pushvalue(newState, LUA_GLOBALSINDEX); //original globals 
		lua_settable(newState, -3);
		lua_setmetatable(newState, -2);
		lua_replace(newState, LUA_GLOBALSINDEX); //replace newState's globals

		luaL_dofile(newState, file.c_str());

		return newState;
	}

	void LuaCore::RemoveChild(int index)
	{
		std::map<int, ChildState>::iterator it = m_childLuaStates->find(index);

		if (it != m_childLuaStates->end())
		{
			lua_unref(m_luaState, it->second.refKey);
			m_childLuaStates->erase(it->first);
		}
	}

	LUA_CALL LuaCore::GetFunction(int index)
	{
		return m_callScripts->GetFunction(index);
	}


	void LuaCore::ReloadAll()
	{
		Close();
		LoadMainState();
		m_callScripts->Initialize(m_luaState, m_childLuaStates);
		LuaBindingContainer bindingInit(m_luaState);

		LoadFile(m_coreStatePath, true);

		for (std::map<int, ChildState>::iterator it = m_childLuaStates->begin(); it != m_childLuaStates->end(); it++)
		{
			int key			= it->first;
			ChildState cs	= it->second;
			int refKey = -1;

			lua_State* ls	= CreateThread(cs.filepath, refKey);
			cs.luaState = ls;
			cs.refKey = refKey;

			
			m_childLuaStates->insert_or_assign(key, cs);
		}
	}

	void LuaCore::ClearChildren()
	{
		m_childLuaStates->clear();
		m_stateCounter = 0;
	}

	void LuaCore::Close()
	{
		lua_close(m_luaState);
		m_luaState = nullptr;
	}

}