#ifndef SRC_CORE_COMPONENTS_COLLISIONCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_COLLISIONCOMPONENT_HPP

#include <Utility/CollisionTypes.hpp>

namespace Core
{
	struct CollisionComponent
	{
		float offsetX;
		float offsetY;

		float width;
		float height;

		CollisionType collisionType;

		CollisionComponent()
		{
			offsetX = 0;
			offsetY = 0;

			width = 0;
			height = 0;

			collisionType = CollisionType::RECTANGLE;
		}

		inline static const char* GetName()
		{
			return "CollisionComponent";
		}
	};
}

#endif