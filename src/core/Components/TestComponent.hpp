#ifndef SRC_CORE_COMPONENTS_TESTCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_TESTCOMPONENT_HPP

namespace Core
{
    struct TestComponent
    {
        int a;
        int b;

		int foo[3];

        TestComponent()
        {
            a = 0;
            b = 0;

			foo[0] = 0;
			foo[1] = 1;
			foo[2] = 2;
        }

        inline static const char* GetName()
        {
            return "TestComponent";
        }
    };
}

#endif //SRC_CORE_COMPONENTS_TESTCOMPONENT_HPP
