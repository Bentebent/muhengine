#ifndef SRC_CORE_COMPONENTS_RENDERINGCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_RENDERINGCOMPONENT_HPP

#include <bitmask/dynamic_bitmask.hpp>
#include <SFML/Graphics.hpp>

namespace Core
{
	struct RenderingComponent
	{
		Bitmask bitmask;
		int color[4];

		float origin[2];
		int sourceRectangle[4];

		bool renderMe;
		bool flippedX;
		bool flippedY;

		RenderingComponent()
		{
			bitmask = 0;
			renderMe = true;
			flippedX = false;
			flippedY = false;
			

			for (int i = 0; i < 4; i++)
			{
				color[i] = 255;
				sourceRectangle[i] = 0;
			}
				

			for (int i = 0; i < 2; i++)
				origin[i] = 0;
		}

		inline static const char* GetName()
		{
			return "RenderingComponent";
		}
	};
}

#endif