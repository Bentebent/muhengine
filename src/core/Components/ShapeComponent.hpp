#ifndef SRC_CORE_COMPONENTS_SHAPECOMPONENT
#define SRC_CORE_COMPONENTS_SHAPECOMPONENT

#include <gfx/GFXShapeType.hpp>

namespace Core
{
	struct ShapeComponent
	{
		unsigned int shapeType;

		int outlineThickness;		
		int outlineColor[4];

		int radius;
		int pointCount;

		float convexPoints[MAX_CONVEX_POINTS][2];
		float rectangleSize[2];
		
		ShapeComponent()
		{
			outlineThickness = 0;
			
			for (int i = 0; i < 4; i++)
				outlineColor[i] = 255;

			shapeType	= gfx::ShapeType::CIRCLE;

			radius		= 1;
			pointCount	= 0;

			for (int i = 0; i < MAX_CONVEX_POINTS; i++)
			{
				convexPoints[i][0] = 0;
				convexPoints[i][1] = 0;
			}

			rectangleSize[0] = 0;
			rectangleSize[1] = 0;
		}

		inline static const char* GetName()
		{
			return "ShapeComponent";
		}
	};
}

#endif