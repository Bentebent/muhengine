#ifndef SRC_CORE_COMPONENTS_SCRIPTCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_SCRIPTCOMPONENT_HPP

#define MAX_SCRIPTS 10

#include <string>
#include <World.hpp>

namespace Core
{
	struct ScriptComponent
	{
		int scriptCount;
		int priorities[MAX_SCRIPTS];
		int stateIds[MAX_SCRIPTS]; //Use this to get a lua_state
		char* functionNames[MAX_SCRIPTS]; //Use this to know what function to call
		int callingFunctions[MAX_SCRIPTS]; //Index to function that defines how push/call/pull to lua is done

		ScriptComponent()
		{
			scriptCount = 0;

			for (int i = 0; i < MAX_SCRIPTS; i++)
			{
				priorities[i]		= -1;
				stateIds[i]			= -1;
				functionNames[i]	= new char(1024);
				callingFunctions[i] = -1;
			}
		}

		inline static const char* GetName()
		{
			return "ScriptComponent";
		}

		void AddScript(int stateId, std::string funcName, int funcId, int priority)
		{
			priorities[scriptCount]			= priority;
			stateIds[scriptCount]			= stateId;
			callingFunctions[scriptCount]	= funcId;

			functionNames[scriptCount][funcName.size()] = 0;
			memcpy(functionNames[scriptCount], funcName.c_str(), funcName.size());

			scriptCount++;
		}

		void RemoveScript()
		{

		}
	};
}

#endif