#ifndef SRC_CORE_COMPONENTS_ANIMATIONCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_ANIMATIONCOMPONENT_HPP

namespace Core
{
	struct AnimationComponent
	{
		int animationID;
		int queuedAnimationID;

		float currentTime;
		float playTime;
		float speed;

		bool loop;
		bool loopFuture;
		bool playing;

		bool reversed;
		bool reversedFuture;

		int currentFrame;

		AnimationComponent()
		{
			animationID			= -1;
			queuedAnimationID	= -1;

			currentTime = 0;
			playTime	= 0;
			speed		= 1;

			loop		= false;
			loopFuture	= false;
			playing		= false;
			reversed	= false;	

			currentFrame = -1;
		}

		inline static const char* GetName()
		{
			return "AnimationComponent";
		}
	};
}

#endif