#ifndef SRC_CORE_COMPONENTS_TRANSFORMATIONCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_TRANSFORMATIONCOMPONENT_HPP

namespace Core
{
	struct TransformationComponent
	{
		float position[2];
		float scale[2];
		float rotation;

		TransformationComponent()
		{
			for (int i = 0; i < 2; i++)
			{
				position[i] = 0;
				scale[i] = 1;
			}

			rotation = 0;
		}

		inline static const char* GetName()
		{
			return "TransformationComponent";
		}
	};
}

#endif //SRC_CORE_COMPONENTS_TRANSFORMATIONCOMPONENT_HPP
