#ifndef SRC_CORE_COMPONENTS_TEXTCOMPONENT_HPP
#define SRC_CORE_COMPONENTS_TEXTCOMPONENT_HPP

#include <string>
#include <Utility/StringUtility.hpp>
#include <World.hpp>

namespace Core
{
	enum TEXT_ALIGNMENT
	{
		CENTER,
		RIGHT,
		LEFT
	};

	struct TextBox
	{
		unsigned int width;
		unsigned int height;

		TextBox()
		{
			width = 0;
			height = 0;
		}
	};

	struct TextComponent
	{
		char text[1024];
		char originalText[1024];
		unsigned int textAlignment;
		unsigned int characterSize;
		unsigned int fontID;
		unsigned int cacheID;

		TextBox textBox;

		bool updated;
		bool cacheMe;

		TextComponent()
		{
			fontID			= 0;
			cacheID			= 0;
			textAlignment	= TEXT_ALIGNMENT::LEFT;
			characterSize	= 12;
			updated			= false;
			cacheMe			= false;
		}

		inline static const char* GetName()
		{
			return "TextComponent";
		}

		static void SetCacheStatus(TextComponent* tc, bool cache)
		{
			if (cache)
			{
				tc->cacheMe = true;
				tc->updated = true;
				gfx::text::CreateCache(tc->cacheID, tc->textBox.width, tc->textBox.height);
				
			}
		}

		static void SetFont(TextComponent* tc, int fontid)
		{
			tc->fontID = fontid;
			
			sf::String string = WrapText(sf::String(tc->originalText), tc->textBox.width, *(sf::Font*)gfx::content::GetFont(tc->fontID), tc->characterSize);
			strcpy(tc->text, string.toAnsiString().c_str());
			
			tc->updated = true;
		}

		static void SetString(TextComponent* tc, std::string string)
		{
			std::string tempString = WrapText(sf::String(string), tc->textBox.width, *(sf::Font*)gfx::content::GetFont(tc->fontID), tc->characterSize);
			strcpy(tc->text, tempString.c_str());
			strcpy(tc->originalText, string.c_str());

			tc->updated = true;
		}

		static void SetTextBox(TextComponent* tc, unsigned int width, unsigned int height)
		{
			tc->textBox.width	= width;
			tc->textBox.height	= height;

			gfx::text::UpdateCache(tc->cacheID, width, height);

			sf::String string = WrapText(sf::String(tc->originalText), tc->textBox.width, *(sf::Font*)gfx::content::GetFont(tc->fontID), tc->characterSize);
			strcpy(tc->text, string.toAnsiString().c_str());

			tc->updated = true;
		}

		static void SetCharacterSize(TextComponent* tc, unsigned int size)
		{
			tc->characterSize = size;
			sf::String string = WrapText(sf::String(tc->originalText), tc->textBox.width, *(sf::Font*)gfx::content::GetFont(tc->fontID), tc->characterSize);
			strcpy(tc->text, string.toAnsiString().c_str());

			tc->updated = true;
		}
	};

	
}

#endif //SRC_CORE_COMPONENTS_TESTCOMPONENT_HPP
