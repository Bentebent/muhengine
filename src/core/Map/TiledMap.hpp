#ifndef SRC_CORE_MAP_TILEMAP_HPP
#define SRC_CORE_MAP_TILEMAP_HPP

#include "TileLayer.hpp"
#include "TileSet.hpp"
#include "CollisionLayer.hpp"

#include <vector>
#include <bitset>
#include <gfx/GFXInstanceData.hpp>
#include "CollisionTileSet.hpp"

namespace Core
{
	class TiledMap
	{
	public:
		TiledMap();
		~TiledMap();

		void Initialize(std::string renderOrder, std::string orientation, int width, int height, int tileWidth, int tileHeight, float tileScaleX, float tileScaleY);

		void AddTileLayer(TileLayer* layer);
		void SetCollisionLayer(CollisionLayer* collisionLayer);
		void AddTileSet(TileSet* tileSet);
		void SetCollisionTileSet(CollisionTileSet* collisionTileSet);

		void Finalize();

		void GetTilePosition(float x, float y, int& tileX, int& tileY);

		inline float GetScaledTileWidth() { return m_tileWidth * m_tileScaleX; }
		inline float GetScaledTileHeight() { return m_tileHeight * m_tileScaleY; }

		inline float GetTileScaleX() { return m_tileScaleX; }
		inline float GetTileScaleY() { return m_tileScaleY; }

		inline int GetTileWidth() { return m_tileWidth; }
		inline int GetTileHeight() { return m_tileHeight; }

		inline int GetMapWidth() { return m_width; }
		inline int GetMapHeight() { return m_height; }
		
		inline float GetMapWidthPixels() { return m_width * m_tileWidth * m_tileScaleX; }
		inline float GetMapHeightPixels() { return m_height * m_tileHeight * m_tileScaleY; }

		inline CollisionLayer* GetCollisionLayer() { return m_collisionLayer; }

	private:
		void FinalizeEntityLayers();
		void FinalizeGraphicalLayers();
		void FinalizeCollisionLayers();

		inline void GetRects(int tileIndex, sf::IntRect*& sourceRect, sf::RectangleShape*& renderTile)
		{
			for (int i = 0; i < m_tileSets.size(); i++)
			{
				if (m_tileSets[i]->GetRenderingTile(tileIndex, sourceRect, renderTile))
					return;
			}
		}

		inline void GetRects(std::pair<unsigned int, std::bitset<3>> tileData, sf::IntRect*& sourceRect, sf::RectangleShape*& renderTile)
		{
			for (int i = 0; i < m_tileSets.size(); i++)
			{
				if (m_tileSets[i]->GetRenderingTile(tileData, sourceRect, renderTile))
					return;
			}
		}

		inline void GetLuaScript(int tileIndex, std::string& scriptName)
		{
			for (int i = 0; i < m_tileSets.size(); i++)
			{
				if (m_tileSets[i]->GetLuaScript(tileIndex, scriptName))
					return;
			}
		}

		std::string	m_renderOrder;
		std::string m_orientation;
		int m_width;
		int m_height;
		int m_tileWidth;
		int m_tileHeight;
		float m_tileScaleX;
		float m_tileScaleY;

		std::vector<TileLayer*> m_backgroundLayers;
		std::vector<TileLayer*> m_groundLayers;
		std::vector<TileLayer*> m_objectLayers;
		std::vector<TileLayer*> m_foregroundLayers;
		std::vector<TileLayer*> m_postFXLayers;
		std::vector<TileLayer*> m_entityLayers;

		std::vector<TileLayer*> m_tileLayers;
		std::vector<TileSet*> m_tileSets;

		std::vector<gfx::TileInstanceData***> m_tileInstanceData;

		CollisionLayer* m_collisionLayer;
		CollisionTileSet* m_collisionTileSet;
	};
}

#endif 