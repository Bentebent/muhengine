#ifndef SRC_CORE_MAP_COLLISIONLAYER_HPP
#define SRC_CORE_MAP_COLLISIONLAYER_HPP

namespace Core
{
	  struct CollisionLayer
	  {
		  int** tiles;
		  int** subTiles;
		  int width;
		  int height;
	  };
}


#endif