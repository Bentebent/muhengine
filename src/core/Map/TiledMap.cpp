#include "TiledMap.hpp"
#include <gfx/GFXInstanceData.hpp>
#include <gfx/GFXInterface.hpp>
#include <World.hpp>

namespace Core
{
	TiledMap::TiledMap()
	{

	}

	TiledMap::~TiledMap()
	{
		for (int i = 0; i < m_tileInstanceData.size(); i++)
		{
			for (int x = 0; x < m_width; x++)
			{
				for (int y = 0; y < m_height; y++)
				{
					delete m_tileInstanceData[i][x][y];
				}

				delete[] m_tileInstanceData[i][x];
			}

			delete[] m_tileInstanceData[i];
		}

		gfx::map::RemoveMap();
	}

	void TiledMap::Initialize(std::string renderOrder, std::string orientation, int width, int height, int tileWidth, int tileHeight, float tileScaleX, float tileScaleY)
	{
		m_renderOrder	= renderOrder;
		m_orientation	= orientation;
		m_width			= width;
		m_height		= height;
		m_tileWidth		= tileWidth;
		m_tileHeight	= tileHeight;
		m_tileScaleX	= tileScaleX;
		m_tileScaleY	= tileScaleY;

		gfx::map::SetMapInfo(tileWidth, tileHeight, width, height, tileScaleX, tileScaleY);
	}

	void TiledMap::GetTilePosition(float x, float y, int& tileX, int& tileY)
	{
		tileX = glm::floor(x / (m_tileWidth * m_tileScaleX));
		tileY = glm::floor(y / (m_tileHeight * m_tileScaleY));
	}

	void TiledMap::AddTileLayer(TileLayer* layer)
	{
		switch (layer->layerType)
		{
		case LayerType::LT_BACKGROUND:
			m_backgroundLayers.push_back(layer);
			break;
		case LayerType::LT_FOREGROUND:
			m_foregroundLayers.push_back(layer);
			break;
		case LayerType::LT_GROUND:
			m_groundLayers.push_back(layer);
			break;
		case LayerType::LT_OBJECT:
			m_objectLayers.push_back(layer);
			break;
		case LayerType::LT_POSTFX:
			m_postFXLayers.push_back(layer);
				break;
		case LayerType::LT_ENTITY:
			m_entityLayers.push_back(layer);
			break;
		}

		if (layer->layerType != LayerType::LT_ENTITY)
			m_tileLayers.push_back(layer);
	}

	void TiledMap::SetCollisionLayer(CollisionLayer* collisionLayer)
	{
		m_collisionLayer = collisionLayer;
	}

	void TiledMap::SetCollisionTileSet(CollisionTileSet* collisionTileSet)
	{
		m_collisionTileSet = collisionTileSet;
	}


	void TiledMap::AddTileSet(TileSet* tileSet)
	{
		m_tileSets.push_back(tileSet);
	}

	void TiledMap::FinalizeEntityLayers()
	{
		lua_State* luaState = world.m_luaCore->GetCoreState();
		std::string entityFolder = "scripts/entities/";

		//Load entity tile layers
		for (int i = 0; i < m_entityLayers.size(); i++)
		{
			int width = m_tileLayers[i]->width;
			int height = m_tileLayers[i]->height;

			for (int x = 0; x < width; x++)
			{
				for (int y = 0; y < height; y++)
				{
					int tile = m_entityLayers[i]->tileData[y * width + x].first;

					if (tile > 0)
					{
						std::string scriptName;
						GetLuaScript(tile, scriptName);
						scriptName = entityFolder + scriptName;

						luaL_loadfile(luaState, scriptName.c_str());
						lua_pcall(luaState, 0, 0, 0);

						lua_getglobal(luaState, "LoadEntity");
						lua_pushnumber(luaState, x);
						lua_pushnumber(luaState, y);
						lua_pushnumber(luaState, m_entityLayers[i]->zIndex);

						if (lua_pcall(luaState, 3, 0, 0) != 0)
							std::cout << (luaState, "error running function 'LoadEntity': %s", lua_tostring(luaState, -1));
					}
				}
			}
		}

	}

	void TiledMap::FinalizeGraphicalLayers()
	{
		std::vector<gfx::TileInstanceData*> tempList;

		for (int i = 0; i < m_tileLayers.size(); i++)
		{
			int width = m_tileLayers[i]->width;
			int height = m_tileLayers[i]->height;

			gfx::TileInstanceData*** layer = new gfx::TileInstanceData**[width];

			for (int x = 0; x < width; x++)
			{
				layer[x] = new gfx::TileInstanceData*[height];

				for (int y = 0; y < height; y++)
				{
					std::pair<unsigned int, std::bitset<3>>	 tileData = m_tileLayers[i]->tileData[y * width + x];

					gfx::TileInstanceData* tid = nullptr;

					if (tileData.first > 0)
					{
						tid = new gfx::TileInstanceData();
						tid->renderTile = nullptr;
						tid->sourceRectangle = nullptr;

						GetRects(tileData, tid->sourceRectangle, tid->renderTile);
					}
					layer[x][y] = tid;
				}
			}
			m_tileInstanceData.push_back(layer);
			gfx::map::AddLayer((void**)layer, m_tileLayers[i]->name.c_str(), m_tileLayers[i]->layerType, tempList.size(), m_tileLayers[i]->zIndex,
				m_tileLayers[i]->opacity, m_tileLayers[i]->scroll[0], m_tileLayers[i]->scroll[1], m_tileLayers[i]->repeat[0], m_tileLayers[i]->repeat[1]);
			tempList.clear();
		}
	}
	void TiledMap::FinalizeCollisionLayers()
	{
		for (int x = 0; x < m_collisionLayer->width; x++)
		{
			for (int y = 0; y < m_collisionLayer->height; y++)
			{
				int collisionIndex = m_collisionLayer->tiles[x][y];

				int leftX = x * 2;
				int rightX = x * 2 + 1;

				int topY = y * 2;
				int bottomY = y * 2 + 1;

				if (collisionIndex > 0)
				{
					CollisionTile* tile = nullptr;
					bool foundTile = m_collisionTileSet->GetTile(collisionIndex, tile);
					if (foundTile)
					{
						m_collisionLayer->subTiles[leftX][topY] = tile->leftTop ? 1 : 0;
						m_collisionLayer->subTiles[rightX][topY] = tile->rightTop ? 1 : 0;
						m_collisionLayer->subTiles[leftX][bottomY] = tile->leftBottom ? 1 : 0;
						m_collisionLayer->subTiles[rightX][bottomY] = tile->rightBottom ? 1 : 0;
					}
					

				}
				else
				{
					m_collisionLayer->subTiles[leftX][topY] = 0;
					m_collisionLayer->subTiles[rightX][topY] = 0;
					m_collisionLayer->subTiles[leftX][bottomY] = 0;
					m_collisionLayer->subTiles[rightX][bottomY] = 0;
				}
			}
		}
	}

	void TiledMap::Finalize()
	{
		FinalizeEntityLayers();
		FinalizeGraphicalLayers();
		FinalizeCollisionLayers();
	}
}