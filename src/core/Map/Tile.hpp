#ifndef SRC_CORE_MAP_TILE_HPP
#define SRC_CORE_MAP_TILE_HPP

#define MAX_SCRIPT_NAME_LENGTH 32

namespace Core
{
	struct Tile
	{
		int id;
		int SLI;
		int SRI;
		char scriptName[MAX_SCRIPT_NAME_LENGTH];
	};

	struct CollisionTile
	{
		bool rightBottom;
		bool rightTop;
		bool leftBottom;
		bool leftTop;
	};
}

#endif