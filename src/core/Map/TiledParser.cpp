#include "TiledParser.hpp"
#include "TiledMap.hpp"
#include <Utility/TinyXML2.hpp>
#include <Utility/StringUtility.hpp>
#include <iostream>
#include <math.h>

#include <World.hpp>

#include "CollisionLayer.hpp"
#include "CollisionTileSet.hpp"

namespace Core
{
	TiledParser::TiledParser()
	{
		m_tilesetFolder = "content/tilesets/";
		m_textureFolder = "content/textures/";
	}

	TiledParser::~TiledParser()
	{

	}

	void TiledParser::LoadMap(std::string filepath, TiledMap*& map)
	{
		world.m_levelHeap.Rewind();

		map = world.m_levelHeap.NewObject<TiledMap>();

		tinyxml2::XMLDocument mapFile;
		mapFile.LoadFile(filepath.c_str());
		
		tinyxml2::XMLElement* mapElement = mapFile.FirstChildElement("map");
		ParseMap(mapElement, map);

		tinyxml2::XMLElement* tileSetXML = mapElement->FirstChildElement("tileset");

		while (tileSetXML != nullptr && std::string(tileSetXML->Value()) != "layer")
		{
			ParseTileset(tileSetXML, map);

			tileSetXML = tileSetXML->NextSiblingElement();
		}

		tinyxml2::XMLElement* tilelayerXML = tileSetXML;

		while (tilelayerXML != nullptr && std::string(tilelayerXML->Value()) == "layer")
		{
			TileLayer* tileLayer = ParseTilelayer(tilelayerXML, map);

			if (tileLayer != nullptr)
				map->AddTileLayer(tileLayer);

			tilelayerXML = tilelayerXML->NextSiblingElement();
		}

		map->Finalize();
	}

	void TiledParser::ParseMap(tinyxml2::XMLElement* mapElement, TiledMap*& map)
	{
		const char* version = mapElement->Attribute("version");
		const char* orientation = mapElement->Attribute("orientation");
		const char* renderOrder = mapElement->Attribute("renderorder");

		int width = 0;
		int height = 0;
		int mapTileWidth = 0;
		int mapTileHeight = 0;
		int nextObjId = 1;
		float tileScaleX = 1;
		float tileScaleY = 1;

		mapElement->QueryIntAttribute("width", &width);
		mapElement->QueryIntAttribute("height", &height);
		mapElement->QueryIntAttribute("tilewidth", &mapTileWidth);
		mapElement->QueryIntAttribute("tileheight", &mapTileHeight);
		mapElement->QueryIntAttribute("nextobjectid", &nextObjId);

		tinyxml2::XMLElement* propertiesElement = mapElement->FirstChildElement("properties");

		if (propertiesElement != nullptr)
		{
			GetProperty(propertiesElement, "tileScaleX", tileScaleX);
			GetProperty(propertiesElement, "tileScaleY", tileScaleY);
		}

		map->Initialize(renderOrder, orientation, width, height, mapTileWidth, mapTileHeight, tileScaleX, tileScaleY);
	}

	void TiledParser::ParseTileset(tinyxml2::XMLElement* tsxElement, TiledMap*& map)
	{
		//Attributes found in .tmx file
		int firstgid = 0;
		std::string tsxSource = "";

		tsxElement->QueryAttribute("firstgid", &firstgid);
		tsxSource = tsxElement->Attribute("source");
		
		std::vector<std::string> substr = SplitString(tsxSource, '/');
		tsxSource = m_tilesetFolder + substr.back();

		//Load the .tsx file
		tinyxml2::XMLDocument tsxFile;
		tsxFile.LoadFile(tsxSource.c_str());

		//tileset attributes
		tinyxml2::XMLElement* tilesetElement = tsxFile.FirstChildElement("tileset");

		int tileWidth	= 0;
		int tileHeight	= 0;
		int tileCount	= 0;
		int spacing = 0;
		int offsetX = 0;
		int offsetY = 0;
		int margin = 0;

		std::string name = "";

		name = tilesetElement->Attribute("name");
		tilesetElement->QueryAttribute("tilewidth", &tileWidth);
		tilesetElement->QueryAttribute("tileheight", &tileHeight);
		tilesetElement->QueryAttribute("tilecount", &tileCount);
		tilesetElement->QueryIntAttribute("spacing", &spacing);
		tilesetElement->QueryIntAttribute("margin", &margin);

		tinyxml2::XMLElement* tileOffset = tilesetElement->FirstChildElement("tileoffset");

		if (tileOffset != nullptr)
		{
			tileOffset->QueryIntAttribute("x", &offsetX);
			tileOffset->QueryIntAttribute("y", &offsetY);
		}

		//Image attributes
		tinyxml2::XMLElement* imageElement = tilesetElement->FirstChildElement("image");

		std::string imgSource = "";
		int width = 0;
		int height = 0;

		imgSource = imageElement->Attribute("source");
		imageElement->QueryAttribute("width", &width);
		imageElement->QueryAttribute("height", &height);

		substr = SplitString(imgSource, '/');
		imgSource = m_textureFolder + substr.back();

		if (name == "collision_tileset")
		{
			tinyxml2::XMLElement* tileElement = imageElement->NextSiblingElement();

			CollisionTileSet* tileSet = world.m_levelHeap.NewObject<CollisionTileSet>();
			tileSet->Initialize(firstgid, name, imgSource.c_str(), tileWidth, tileHeight, offsetX, offsetY, width, height, margin, spacing);

			while (tileElement != nullptr)
			{
				int id = 0;
				bool topLeft = false;
				bool topRight = false;
				bool bottomLeft = false;
				bool bottomRight = false;

				tileElement->QueryAttribute("id", &id);

				tinyxml2::XMLElement* propertiesElement = tileElement->FirstChildElement("properties");

				GetProperty(propertiesElement, "leftTop", topLeft);
				GetProperty(propertiesElement, "rightTop", topRight);
				GetProperty(propertiesElement, "leftBottom", bottomLeft);
				GetProperty(propertiesElement, "rightBottom", bottomRight);

				CollisionTile* newTile = world.m_levelHeap.NewObject<CollisionTile>();

				newTile->leftBottom = bottomLeft;
				newTile->leftTop = topLeft;
				newTile->rightBottom = bottomRight;
				newTile->rightTop = topRight;
				
				tileSet->AddTile(id + firstgid, newTile);

				tileElement = tileElement->NextSiblingElement();
			}

			map->SetCollisionTileSet(tileSet);
		}
		else
		{
			//Init tileset
			TileSet* tileSet = world.m_levelHeap.NewObject<TileSet>();
			tileSet->Initialize(firstgid, name, imgSource.c_str(), tileWidth, tileHeight, offsetX, offsetY, width, height, margin, spacing);
			tileSet->InitializeRenderTile(tileWidth, tileHeight);


			tinyxml2::XMLElement* tileElement = imageElement->NextSiblingElement();

			while (tileElement != nullptr)
			{
				int id = 0;
				int SLI = 0;
				int SRI = 0;
				std::string script;

				tileElement->QueryAttribute("id", &id);

				tinyxml2::XMLElement* propertiesElement = tileElement->FirstChildElement("properties");
				GetProperty(propertiesElement, "SLI", SLI);
				GetProperty(propertiesElement, "SRI", SRI);
				GetProperty(propertiesElement, "script", script);

				Tile* newTile = world.m_levelHeap.NewObject<Tile>();
				newTile->id = id + firstgid;
				newTile->SLI = SLI;
				newTile->SRI = SRI;
				strcpy(newTile->scriptName, script.c_str());

				tileSet->AddTile(newTile->id, newTile);

				tileElement = tileElement->NextSiblingElement();
			}

			map->AddTileSet(tileSet);
		}
	}

	TileLayer* TiledParser::ParseTilelayer(tinyxml2::XMLElement* tilelayerElement, TiledMap*& map)
	{
		//Attributes
		int layerWidth = 0;
		int layerHeight = 0;
		int layerOffsetX = 0;
		int layerOffsetY = 0;
		float opacity = 1.0f;

		//Properties
		float scrollX = 1;
		float scrollY = 1;
		bool repeatX = false;
		bool repeatY = false;
		int zIndex = 0;
		std::string layerType;

		const char* layerName = tilelayerElement->Attribute("name");

		tilelayerElement->QueryAttribute("width", &layerWidth);
		tilelayerElement->QueryAttribute("height", &layerHeight);
		tilelayerElement->QueryAttribute("offsetx", &layerOffsetX);
		tilelayerElement->QueryAttribute("offsety", &layerOffsetY);
		tilelayerElement->QueryAttribute("opacity", &opacity);

		tinyxml2::XMLElement* propertiesElement = tilelayerElement->FirstChildElement("properties");

		if (propertiesElement != nullptr)
		{
			GetProperty(propertiesElement, "scrollX", scrollX);
			GetProperty(propertiesElement, "scrollY", scrollY);
			GetProperty(propertiesElement, "repeatX", repeatX);
			GetProperty(propertiesElement, "repeatY", repeatY);
			GetProperty(propertiesElement, "zIndex", zIndex);
			GetProperty(propertiesElement, "layerType", layerType);
		}

		if (layerType == "invisible")
			return nullptr;
		else if (layerType == "collision")
		{
			ParseCollisionLayer(tilelayerElement, map);
			return nullptr;
		}

		TileLayer* tileLayer = world.m_levelHeap.NewObject<TileLayer>();

		//Get data attributes
		const char* encoding	= tilelayerElement->FirstChildElement("data")->Attribute("encoding");
		const char* compression = tilelayerElement->FirstChildElement("data")->Attribute("compression");
		const char* data		= tilelayerElement->FirstChildElement("data")->GetText();

		std::string trimmedData = TrimString(std::string(data));
		std::string decodedData;

		//Assign all shit
		tileLayer->name = layerName;
		tileLayer->width = layerWidth;
		tileLayer->height = layerHeight;
		tileLayer->opacity = opacity;
		tileLayer->zIndex = zIndex;
		tileLayer->scroll[0] = scrollX;
		tileLayer->scroll[1] = scrollY;
		tileLayer->repeat[0] = repeatX;
		tileLayer->repeat[1] = repeatY;

		if (layerType == "background")
			tileLayer->layerType = LayerType::LT_BACKGROUND;
		else if (layerType == "foreground")
			tileLayer->layerType = LayerType::LT_FOREGROUND;
		else if (layerType == "ground")
			tileLayer->layerType = LayerType::LT_GROUND;
		else if (layerType == "postfx")
			tileLayer->layerType = LayerType::LT_POSTFX;
		else if (layerType == "object")
			tileLayer->layerType = LayerType::LT_OBJECT;
		else if (layerType == "entity")
			tileLayer->layerType = LayerType::LT_ENTITY;

		//Allocate, trim, decode data string
		tileLayer->tileData = new std::pair<unsigned int, std::bitset<3>>[layerWidth * layerHeight];

		int expectedSize = layerWidth * layerHeight * 4; //number of tiles * 4 bytes = 32bits / tile
		std::vector<unsigned char> byteArray; //to hold decompressed data as bytes
		byteArray.reserve(expectedSize);

		if (encoding == std::string("base64"))
		{
			decodedData = m_base64Decoder.base64_decode(trimmedData);
		}

		if (compression == std::string("zlib"))
		{
			DecompressString(decodedData.c_str(), byteArray, decodedData.size(), expectedSize);
		}

		int index = 0;
		for (int i = 0; i < byteArray.size(); i += 4)
		{
			unsigned int parsedInt = 0;

			parsedInt = (parsedInt << 8) + byteArray[i + 3];
			parsedInt = (parsedInt << 8) + byteArray[i + 2];
			parsedInt = (parsedInt << 8) + byteArray[i + 1];
			parsedInt = (parsedInt << 8) + byteArray[i + 0];

			std::pair<unsigned int, std::bitset<3>> rotationPair = ResolveRotation(parsedInt);

			tileLayer->tileData[index++] = rotationPair;
		}

		return tileLayer;
	}

	void TiledParser::ParseCollisionLayer(tinyxml2::XMLElement* tilelayerElement, TiledMap*& map)
	{
		CollisionLayer* collisionLayer = world.m_levelHeap.NewObject<CollisionLayer>();

		int layerWidth = 0;
		int layerHeight = 0;

		tinyxml2::XMLElement* propertiesElement = tilelayerElement->FirstChildElement("properties");

		tilelayerElement->QueryAttribute("width", &layerWidth);
		tilelayerElement->QueryAttribute("height", &layerHeight);

		collisionLayer->width = layerWidth;
		collisionLayer->height = layerHeight;

		int subtileLayerWidth = layerWidth * 2;
		int subtileLayerHeight = layerHeight * 2;

		collisionLayer->tiles = world.m_levelHeap.NewPODArray<int*>(layerWidth);
		for (int x = 0; x < layerWidth; x++)
		{
			collisionLayer->tiles[x] = world.m_levelHeap.NewPODArray<int>(layerHeight);
		}

		collisionLayer->subTiles = world.m_levelHeap.NewPODArray<int*>(subtileLayerWidth);

		for (int x = 0; x < subtileLayerWidth; x++)
		{
			collisionLayer->subTiles[x] = world.m_levelHeap.NewPODArray<int>(subtileLayerHeight);
		}

		const char* encoding = tilelayerElement->FirstChildElement("data")->Attribute("encoding");
		const char* compression = tilelayerElement->FirstChildElement("data")->Attribute("compression");
		const char* data = tilelayerElement->FirstChildElement("data")->GetText();

		std::string trimmedData = TrimString(std::string(data));
		std::string decodedData;

		int expectedSize = layerWidth * layerHeight * 4; //number of tiles * 4 bytes = 32bits / tile
		std::vector<unsigned char> byteArray; //to hold decompressed data as bytes
		byteArray.reserve(expectedSize);

		if (encoding == std::string("base64"))
		{
			decodedData = m_base64Decoder.base64_decode(trimmedData);
		}

		if (compression == std::string("zlib"))
		{
			DecompressString(decodedData.c_str(), byteArray, decodedData.size(), expectedSize);
		}

		int index = 0;
		for (int i = 0; i < byteArray.size(); i += 4)
		{
			int parsedInt = 0;

			parsedInt = (parsedInt << 8) + byteArray[i + 3];
			parsedInt = (parsedInt << 8) + byteArray[i + 2];
			parsedInt = (parsedInt << 8) + byteArray[i + 1];
			parsedInt = (parsedInt << 8) + byteArray[i + 0];

			int x = index % layerWidth;
			int y = index / layerWidth;

			collisionLayer->tiles[x][y] = parsedInt;

			index++;
		}

		map->SetCollisionLayer(collisionLayer);
	}

	std::pair<unsigned int, std::bitset<3>> TiledParser::ResolveRotation(unsigned int gid)
	{
		std::vector<unsigned char> bytes = IntToBytes(gid);
		unsigned int tileGID = bytes[0] | bytes[1] << 8 | bytes[2] << 16 | bytes[3] << 24;

		bool flipped_diagonally = (tileGID & FLIPPED_DIAGONALLY_FLAG);
		bool flipped_horizontally = (tileGID & FLIPPED_HORIZONTALLY_FLAG);
		bool flipped_vertically = (tileGID & FLIPPED_VERTICALLY_FLAG);

		std::bitset<3> b;
		b.set(0, flipped_vertically);
		b.set(1, flipped_horizontally);
		b.set(2, flipped_diagonally);

		tileGID &= ~(FLIPPED_HORIZONTALLY_FLAG |
			FLIPPED_VERTICALLY_FLAG |
			FLIPPED_DIAGONALLY_FLAG);
		return std::pair<sf::Uint32, std::bitset<3> >(tileGID, b);
	}
}