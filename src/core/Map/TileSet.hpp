 #ifndef SRC_CORE_MAP_TILESET_HPP
#define SRC_CORE_MAP_TILESET_HPP

#include <string>
#include <map>
#include <SFML/Graphics.hpp>
#include <bitset>
#include "Tile.hpp"
namespace Core
{
	class TileSet
	{
	public:
		TileSet();
		~TileSet();

		void Initialize(int firstGid, std::string name, std::string source, int tileWidth, int tileHeight, int tileOffsetX, int tileOffsetY, int sheetSizeX, int sheetSizeY, int margin, int spacing);
		void InitializeRenderTile(int tileWidth, int tileHeight);
		
		void AddTile(int key, Tile* tile);
		void BuildSourceRectangles();

		inline sf::RectangleShape* TileBelongs(int tileIndex)
		{
			if (tileIndex < m_firstgid || tileIndex >= m_sourceRectangles.size() + m_firstgid)
				return nullptr;

			m_renderTile->setTextureRect(*m_sourceRectangles[tileIndex]);
			return m_renderTile;
		}

		inline bool GetLuaScript(int tileIndex, std::string& luaScript)
		{
			if (tileIndex < m_firstgid || tileIndex >= m_sourceRectangles.size() + m_firstgid)
				return false;
			else
			{
				luaScript = m_tileMap[tileIndex]->scriptName;
			}
			return true;
		}

		inline bool GetRenderingTile(int tileIndex, sf::IntRect*& sourceRect, sf::RectangleShape*& renderTile)
		{
			if (tileIndex < m_firstgid || tileIndex >= m_sourceRectangles.size() + m_firstgid)
				return false;
			else
			{
				sourceRect = m_sourceRectangles[tileIndex];
				renderTile = m_renderTile;
			}
			return true;
		}

		inline bool GetRenderingTile(std::pair<unsigned int, std::bitset<3>> tileData, sf::IntRect*& sourceRect, sf::RectangleShape*& renderTile)
		{
			if (tileData.first < m_firstgid || tileData.first >= m_sourceRectangles.size() + m_firstgid)
				return false;
			else
			{
				sourceRect = m_sourceRectangles[tileData.first];

				//000 = no change
				//001 = vertical = swap y axis
				//010 = horizontal = swap x axis
				//011 = horiz + vert = swap both axes = horiz+vert = rotate 180 degrees
				//100 = diag = rotate 90 degrees right and swap x axis
				//101 = diag+vert = rotate 270 degrees right
				//110 = horiz+diag = rotate 90 degrees right
				//111 = horiz+vert+diag = rotate 90 degrees right and swap y axis

				std::bitset<3> bits = tileData.second;

				if (!bits.test(0) && !bits.test(1) && !bits.test(2))
				{
					//000
					renderTile = m_renderTile;
				}
				else if (bits.test(0) && !bits.test(1) && !bits.test(2))
				{
					//001
					renderTile = m_flippedVerticallyTile;
				}
				else if (!bits.test(0) && bits.test(1) && !bits.test(2))
				{
					//010
					renderTile = m_flippedHorizontallyTile;
				}
				else if (bits.test(0) && bits.test(1) && !bits.test(2))
				{
					//011
					renderTile = m_flippedHorizontallyVerticallyTile;
				}
				else if (!bits.test(0) && !bits.test(1) && bits.test(2))
				{
					//100
					renderTile = m_flippedDiagonallyTile;
				}
				else if (bits.test(0) && !bits.test(1) && bits.test(2))
				{
					//101
					renderTile = m_flippedDiagonallyVerticallyTile;

				}
				else if (!bits.test(0) && bits.test(1) && bits.test(2))
				{
					//110
					renderTile = m_flippedDiagonallyHorizontallyTile;

				}
				else if (bits.test(0) && bits.test(1) && bits.test(2))
				{
					//111
					renderTile = m_flippedDiagonallyVerticallyHorizontallyTile;
				}
			}
			return true;
		}

	private:

		sf::RectangleShape* CreateTile(float scaleX, float scaleY, float rot, int tileWidth, int tileHeight);

		int m_firstgid;
		std::string m_source;
		std::string m_name;

		unsigned int m_textureid;

		int m_tileSize[2];
		int m_tileOffset[2];
		int m_sheetSize[2];
		int m_margin;
		int m_spacing;

		sf::RectangleShape* m_renderTile;
		sf::RectangleShape* m_flippedHorizontallyTile;
		sf::RectangleShape* m_flippedVerticallyTile;
		sf::RectangleShape* m_flippedDiagonallyTile;
		sf::RectangleShape* m_flippedDiagonallyHorizontallyTile;
		sf::RectangleShape* m_flippedHorizontallyVerticallyTile;
		sf::RectangleShape* m_flippedDiagonallyVerticallyTile;
		sf::RectangleShape* m_flippedDiagonallyVerticallyHorizontallyTile;

		sf::Texture* m_texture;

		std::map<int, sf::IntRect*> m_sourceRectangles;
		//std::vector<Tile*> m_tiles;

		std::map<int, Tile*> m_tileMap;
	};
}

#endif