#ifndef SRC_CORE_MAP_TILEDPARSER_HPP
#define SRC_CORE_MAP_TILEDPARSER_HPP

#include "TileSet.hpp"
#include "TileLayer.hpp"
#include "Tile.hpp"
#include "TiledMap.hpp"

#include <string>
#include <map>
#include <bitset>
#include <Utility/Base64.hpp>
#include <Utility/TinyXML2.hpp>
namespace Core
{
	class TiledParser
	{
	public:
		TiledParser();
		~TiledParser();

		void LoadMap(std::string filepath, TiledMap*& map);

	private:
		void ParseTileset(tinyxml2::XMLElement* tilesetElement, TiledMap*& map);
		TileLayer* ParseTilelayer(tinyxml2::XMLElement* tilelayerElement, TiledMap*& map);
		void ParseMap(tinyxml2::XMLElement* element, TiledMap*& map);
		void ParseCollisionLayer(tinyxml2::XMLElement* tilelayerElement, TiledMap*& map);

		std::pair<unsigned int, std::bitset<3>> ResolveRotation(unsigned int gid);

		template<class T>
		void GetProperty(tinyxml2::XMLElement* element, std::string propertyName, T& value)
		{
			tinyxml2::XMLElement* propertyElement = element->FirstChildElement("property");
			std::string pName;

			while (propertyElement != nullptr)
			{
				pName = propertyElement->Attribute("name");

				if (pName == propertyName)
				{
					propertyElement->QueryAttribute("value", &value);
					return;
				}

				propertyElement = propertyElement->NextSiblingElement();
			}
		}

		template<>
		void GetProperty(tinyxml2::XMLElement* element, std::string propertyName, std::string& value)
		{
			tinyxml2::XMLElement* propertyElement = element->FirstChildElement("property");
			std::string pName;

			while (propertyElement != nullptr)
			{
				pName = propertyElement->Attribute("name");

				if (pName == propertyName)
				{
					value = propertyElement->Attribute("value");
					return;
				}

				propertyElement = propertyElement->NextSiblingElement();
			}
		}

		inline std::vector<unsigned char> IntToBytes(unsigned int paramInt)
		{
			std::vector<unsigned char> arrayOfByte(4);
			for (int i = 0; i < 4; i++)
				arrayOfByte[i] = (paramInt >> (i * 8));
			return arrayOfByte;
		}

		Base64 m_base64Decoder;

		std::string m_tilesetFolder;
		std::string m_textureFolder;

		const unsigned FLIPPED_HORIZONTALLY_FLAG = 0x80000000;
		const unsigned FLIPPED_VERTICALLY_FLAG = 0x40000000;
		const unsigned FLIPPED_DIAGONALLY_FLAG = 0x20000000;
	};
}

#endif
