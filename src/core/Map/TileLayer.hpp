#ifndef SRC_CORE_MAP_TILELAYER_HPP
#define SRC_CORE_MAP_TILELAYER_HPP

#include <string>
#include <set>
#include "Tile.hpp"

#include <map/LayerType.hpp>

#include <bitset>
namespace Core
{
	struct TileLayer
	{
		std::string name;

		int width;
		int height;

		float opacity;

		LayerType layerType;
		int zIndex;

		float scroll[2];
		bool repeat[2];

		std::pair<unsigned int, std::bitset<3>>* tileData;

		~TileLayer()
		{
			delete[] tileData;
		}

		//bool postFX;
		//bool parallax;
		//
		//
		//float opacity;
		//
		//int zIndex;
		//int tileSheet;
		//bool collidable;
	};
}

#endif