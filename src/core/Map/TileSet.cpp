#include "TileSet.hpp"
#include <gfx/GFXInterface.hpp>
#include <World.hpp>

namespace Core
{
	TileSet::TileSet()
	{

	}

	TileSet::~TileSet()
	{

	}

	void TileSet::Initialize(int firstGid, std::string name, std::string source, int tileWidth, int tileHeight, int tileOffsetX, int tileOffsetY, int sheetSizeX, int sheetSizeY, int margin, int spacing)
	{
		m_firstgid	= firstGid;
		m_source	= source;
		m_name		= name;

		m_tileSize[0] = tileWidth;
		m_tileSize[1] = tileHeight;

		m_tileOffset[0] = tileOffsetX;
		m_tileOffset[1] = tileOffsetY;

		m_sheetSize[0] = sheetSizeX;
		m_sheetSize[1] = sheetSizeY;

		m_margin = margin;
		m_spacing = spacing;

		BuildSourceRectangles();

		m_texture = world.m_levelHeap.NewObject<sf::Texture>();
		m_texture->loadFromFile(source);

	}

	sf::RectangleShape* TileSet::CreateTile(float scaleX, float scaleY, float rot, int tileWidth, int tileHeight)
	{
		sf::RectangleShape*	output = world.m_levelHeap.NewObject<sf::RectangleShape>();

		output->setSize(sf::Vector2f(tileWidth, tileHeight));
		output->setFillColor(sf::Color::White);
		output->setScale(scaleX, scaleY);
		output->setRotation(rot);

		output->setTexture(m_texture);

		return output;
	}

	void TileSet::InitializeRenderTile(int tileWidth, int tileHeight)
	{
		m_renderTile = CreateTile(1, 1, 0, tileWidth, tileHeight);
		m_flippedHorizontallyTile = CreateTile(-1, 1, 0, tileWidth, tileHeight);
		m_flippedVerticallyTile = CreateTile(1, -1, 0, tileWidth, tileHeight);
		m_flippedDiagonallyTile = CreateTile(-1, 1, 270, tileWidth, tileHeight);
		m_flippedDiagonallyHorizontallyTile = CreateTile(1, 1, 90, tileWidth, tileHeight);
		m_flippedHorizontallyVerticallyTile = CreateTile(-1, -1, 0, tileWidth, tileHeight);
		m_flippedDiagonallyVerticallyTile = CreateTile(1, 1, 270, tileWidth, tileHeight);
		m_flippedDiagonallyVerticallyHorizontallyTile = CreateTile(1, -1, 270, tileWidth, tileHeight);
	}
	

	void TileSet::AddTile(int key, Tile* tile)
	{
		m_tileMap.emplace(key, tile);
	}

	void TileSet::BuildSourceRectangles()
	{
		int gid = m_firstgid;

		for (int y = 0; y < m_sheetSize[1]; y += m_tileSize[1])
		{
			for (int x = 0; x < m_sheetSize[0]; x += m_tileSize[0])
			{
				sf::IntRect* rect = world.m_levelHeap.NewObject<sf::IntRect>(x, y, m_tileSize[0], m_tileSize[1]); 
				m_sourceRectangles.emplace(gid, rect);
				gid++;
			}
		}
	}
}