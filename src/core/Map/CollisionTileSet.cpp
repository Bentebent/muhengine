#include "CollisionTileSet.hpp"

namespace Core
{
	CollisionTileSet::CollisionTileSet()
	{

	}

	CollisionTileSet::~CollisionTileSet()
	{

	}

	void CollisionTileSet::AddTile(int key, CollisionTile* tile)
	{
		m_tiles.emplace(key, tile);
	}

	void CollisionTileSet::Initialize(int firstGid, std::string name, std::string source, int tileWidth, int tileHeight, int tileOffsetX, int tileOffsetY, int sheetSizeX, int sheetSizeY, int margin, int spacing)
	{
		m_firstgid = firstGid;
		m_source = source;
		m_name = name;

		m_tileSize[0] = tileWidth;
		m_tileSize[1] = tileHeight;

		m_tileOffset[0] = tileOffsetX;
		m_tileOffset[1] = tileOffsetY;

		m_sheetSize[0] = sheetSizeX;
		m_sheetSize[1] = sheetSizeY;

		m_margin = margin;
		m_spacing = spacing;
	}
}