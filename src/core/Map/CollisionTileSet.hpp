#ifndef SRC_CORE_MAP_COLLISIONTILESET_HPP
#define SRC_CORE_MAP_COLLISIONTILESET_HPP

#include "Tile.hpp"
#include <string>
#include <map>
namespace Core
{
	class CollisionTileSet
	{
	public:
		CollisionTileSet();
		~CollisionTileSet();
		void AddTile(int key, CollisionTile* tile);

		void Initialize(int firstGid, std::string name, std::string source, int tileWidth, int tileHeight, int tileOffsetX, int tileOffsetY, int sheetSizeX, int sheetSizeY, int margin, int spacing);

		inline bool GetTile(int tileIndex, CollisionTile*& collisionTile)
		{
			if (tileIndex < m_firstgid || tileIndex >= m_tiles.size() + m_firstgid)
				return false;
			else
				collisionTile = m_tiles[tileIndex];

			return true;
		}

	private:

		int m_firstgid;
		std::string m_source;
		std::string m_name;

		int m_tileSize[2];
		int m_tileOffset[2];
		int m_sheetSize[2];
		int m_margin;
		int m_spacing;

		std::map<int, CollisionTile*> m_tiles;
	};

}

#endif