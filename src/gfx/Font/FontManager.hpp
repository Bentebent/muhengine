#ifndef SRC_GFX_FONT_FONTMANAGER_HPP
#define SRC_GFX_FONT_FONTMANAGER_HPP

#include "FontData.hpp"
#include <SFML/Graphics.hpp>
#include <map>

namespace gfx
{
	class FontManager
	{
	public:
		friend FontManager& FontManagerInstance();

		void LoadFont(unsigned int& id, const char* path);
		void ReloadFonts();
		void DeleteFont(int id);

		inline sf::Font* GetFont(int id)
		{
			return m_fonts[id].font;
		}

	private:
		FontManager();
		~FontManager();

		std::map<int, FontData> m_fonts;

		unsigned long long int m_fontIdCounter;
	};

	FontManager& FontManagerInstance();
}

#endif