#include "FontManager.hpp"

namespace gfx
{
	FontManager& FontManagerInstance()
	{
		static FontManager fm;
		return fm;
	}

	void FontManager::LoadFont(unsigned int& id, const char* path)
	{
		sf::Font* font = new sf::Font();
		font->loadFromFile(path);

		FontData fd;
		fd.font = font;
		fd.filePath = path;
		fd.id = m_fontIdCounter;
		id = fd.id;

		m_fontIdCounter++;

		m_fonts.emplace(id, fd);
	}

	void FontManager::ReloadFonts()
	{
		for (std::map<int, FontData>::iterator it = m_fonts.begin(); it != m_fonts.end(); it++)
			it->second.font->loadFromFile(it->second.filePath);
	}

	void FontManager::DeleteFont(int id)
	{

	}

	FontManager::FontManager()
	{
		m_fontIdCounter = 0;
	}

	FontManager::~FontManager()
	{
		for (std::map<int, FontData>::iterator it = m_fonts.begin(); it != m_fonts.end(); it++)
			delete it->second.font;
		m_fonts.clear();
	}

};