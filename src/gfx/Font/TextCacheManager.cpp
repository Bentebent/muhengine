#include "TextCacheManager.hpp"

namespace gfx
{
	TextCacheManager& TextCacheManagerInstance()
	{
		static TextCacheManager tcm;
		return tcm;
	}

	void TextCacheManager::CreateTextCache(unsigned int& id, unsigned int width, unsigned int height)
	{
		m_renderTextures.emplace(m_textCacheIdCounter, new sf::RenderTexture());
		m_renderTextures[m_textCacheIdCounter]->create(width, height);

		id = m_textCacheIdCounter;
		m_textCacheIdCounter++;
	}

	void TextCacheManager::UpdateTextCache(unsigned int& id, unsigned int width, unsigned int height)
	{
		delete m_renderTextures[id];
		m_renderTextures[id] = new sf::RenderTexture();
		m_renderTextures[id]->create(width, height);
	}

	void TextCacheManager::DeleteTextCache(unsigned int id)
	{
		delete m_renderTextures[id];
		m_renderTextures.erase(id);
	}

	TextCacheManager::TextCacheManager()
	{
		m_textCacheIdCounter = 0;
	}

	//Fix this sometime I guess
	TextCacheManager::~TextCacheManager()
	{
		
	}

	void TextCacheManager::ClearCache()
	{
		for (std::map<int, sf::RenderTexture*>::iterator it = m_renderTextures.begin(); it != m_renderTextures.end(); it++)
			delete it->second;

		m_renderTextures.clear();
	}

};