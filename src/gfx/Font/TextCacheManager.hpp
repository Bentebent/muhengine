#ifndef SRC_GFX_FONT_TEXTCACHEMANAGER_HPP
#define SRC_GFX_FONT_TEXTCACHEMANAGER_HPP

#include <SFML/Graphics.hpp>

namespace gfx
{
	class TextCacheManager
	{
	public:
		friend TextCacheManager& TextCacheManagerInstance();

		void CreateTextCache(unsigned int& id, unsigned int width, unsigned int height);
		void UpdateTextCache(unsigned int& id, unsigned int width, unsigned int height);
		void DeleteTextCache(unsigned int id);

		void ClearCache();

		inline sf::RenderTexture* GetRenderTexture(int id)
		{
			return m_renderTextures[id];
		}

	private:
		TextCacheManager();
		~TextCacheManager();

		std::map<int, sf::RenderTexture*> m_renderTextures;

		unsigned long long int m_textCacheIdCounter;
	};

	TextCacheManager& TextCacheManagerInstance();
}

#endif