#ifndef SRC_GFX_FONT_FONTDATA_HPP
#define SRC_GFX_FONT_FONTDATA_HPP

#include <SFML/Graphics.hpp>

namespace gfx
{
	struct FontData
	{
		int id;
		sf::Font* font;
		std::string filePath;
	};
}

#endif