#ifndef SRC_GFX_TEXTURE_TEXTUREDATA_HPP
#define SRC_GFX_TEXTURE_TEXTUREDATA_HPP

#include <SFML/Graphics.hpp>
#include <string>
namespace gfx
{
	struct TextureData
	{
		int id;
		sf::Texture* texture;
		std::string filePath;
	};
}

#endif