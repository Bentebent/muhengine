#ifndef SRC_GFX_TEXTURE_TEXTUREMANAGER_HPP
#define SRC_GFX_TEXTURE_TEXTUREMANAGER_HPP

#include "TextureData.hpp"
#include <SFML/Graphics.hpp>
#include <map>

namespace gfx
{
	class TextureManager
	{
	public:
		friend TextureManager& TextureManagerInstance();

		void LoadTexture(unsigned int& id, const char* path);
		void ReloadTextures();
		void DeleteTexture(int id);

		inline sf::Texture* GetTexture(int id)
		{
			return m_textures[id].texture;
		}

	private:
		TextureManager();
		~TextureManager();

		std::map<int, TextureData> m_textures;

		unsigned long long int m_textureIdCounter;
	};

	TextureManager& TextureManagerInstance();
}

#endif