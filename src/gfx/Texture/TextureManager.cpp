#include "TextureManager.hpp"

namespace gfx
{
	TextureManager& TextureManagerInstance()
	{
		static TextureManager tm;
		return tm;
	}

	TextureManager::TextureManager()
	{
		m_textureIdCounter = 0;
	}

	TextureManager::~TextureManager()
	{
		for (std::map<int, TextureData>::iterator it = m_textures.begin(); it != m_textures.end(); it++)
			delete it->second.texture;
		m_textures.clear();
	}

	void TextureManager::LoadTexture(unsigned int& id, const char* path)
	{
		sf::Texture* texture = new sf::Texture();
		texture->loadFromFile(path);

		TextureData td;
		td.texture	= texture;
		td.filePath	= path;
		td.id		= m_textureIdCounter;
		id			= td.id;

		m_textureIdCounter++;

		m_textures.emplace(id, td);
	}

	void TextureManager::ReloadTextures()
	{
		for (std::map<int, TextureData>::iterator it = m_textures.begin(); it != m_textures.end(); it++)
		{
			delete it->second.texture;
			it->second.texture = new sf::Texture();
			it->second.texture->loadFromFile(it->second.filePath);
		}
			
	}

	void TextureManager::DeleteTexture(int id)
	{
		std::map<int, gfx::TextureData>::iterator it = m_textures.find(id);

		if (it != m_textures.end())
		{
			delete m_textures[id].texture;
			m_textures.erase(id);
		}
	}

}