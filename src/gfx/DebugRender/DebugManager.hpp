#ifndef SRC_GFX_DEBUG_DEBUGMANAGER_HPP
#define SRC_GFX_DEBUG_DEBUGMANAGER_HPP

#include <SFML/Graphics.hpp>

namespace gfx
{
	class DebugManager
	{
	public:
		friend DebugManager& DebugManagerInstance();

		void Initialize(sf::RenderWindow* renderWindow);

		void AddString(float x, float y, const char* text, int size, float r, float g, float b, float a);
		void AddCircle(float x, float y, float radius, float r, float g, float b, float a, bool gui);
		void AddRectangle(float x, float y, float w, float h, float r, float g, float b, float a, bool gui);
		void AddPolygon(float x, float y, float radius, int pointCount, float r, float g, float b, float a, bool gui);

		void Render(sf::View defaultView);

	private:
		DebugManager();
		~DebugManager();

		sf::RenderWindow* m_renderWindow;

		sf::Font m_font;

		std::vector<sf::Text> m_strings;
		std::vector<sf::CircleShape> m_circles;
		std::vector<sf::RectangleShape> m_rectangles;

		std::vector<sf::CircleShape> m_guiCircles;
		std::vector<sf::RectangleShape> m_guiRectangles;

	};
}

#endif