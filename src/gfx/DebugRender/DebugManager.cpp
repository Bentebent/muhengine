#include "DebugManager.hpp"

namespace gfx
{
	DebugManager::DebugManager()
	{

	}

	DebugManager::~DebugManager()
	{

	}

	DebugManager& DebugManagerInstance()
	{
		static DebugManager dm;
		return dm;
	}

	void DebugManager::Initialize(sf::RenderWindow* renderWindow)
	{
		m_renderWindow = renderWindow;
		m_font.loadFromFile("content/fonts/arial.ttf");
	}

	void DebugManager::AddString(float x, float y, const char* text, int size, float r, float g, float b, float a)
	{
		sf::Text debugText;
		debugText.setFont(m_font);
		debugText.setPosition(x, y);
		debugText.setString(text);
		debugText.setCharacterSize(size);
		debugText.setColor(sf::Color(r, g, b, a));

		m_strings.push_back(debugText);
	}

	void DebugManager::AddCircle(float x, float y, float radius, float r, float g, float b, float a, bool gui)
	{
		sf::CircleShape circle;
		circle.setPosition(x, y);
		circle.setOrigin(radius, radius);
		circle.setRadius(radius);
		circle.setFillColor(sf::Color(r, g, b, a));
		
		if (!gui)
			m_circles.push_back(circle);
		else
			m_guiCircles.push_back(circle);
	}

	void DebugManager::AddRectangle(float x, float y, float w, float h, float r, float g, float b, float a, bool gui)
	{
		sf::RectangleShape rect;

		rect.setPosition(x, y);
		rect.setSize(sf::Vector2f(w, h));
		rect.setFillColor(sf::Color(r, g, b, a));

		if (!gui)
			m_rectangles.push_back(rect);
		else
			m_guiRectangles.push_back(rect);
	}

	void DebugManager::AddPolygon(float x, float y, float radius, int pointCount, float r, float g, float b, float a, bool gui)
	{
		sf::CircleShape circle;
		circle.setPosition(x, y);
		circle.setOrigin(radius, radius);
		circle.setRadius(radius);
		circle.setFillColor(sf::Color(r, g, b, a));
		circle.setPointCount(pointCount);

		if (!gui)
			m_circles.push_back(circle);
		else
			m_guiCircles.push_back(circle);
	}

	void DebugManager::Render(sf::View defaultView)
	{
		for (std::vector<sf::RectangleShape>::iterator it = m_rectangles.begin(); it != m_rectangles.end(); it++)
			m_renderWindow->draw(*it);

		for (std::vector<sf::CircleShape>::iterator it = m_circles.begin(); it != m_circles.end(); it++)
			m_renderWindow->draw(*it);

		m_renderWindow->setView(defaultView);

		for (std::vector<sf::RectangleShape>::iterator it = m_guiRectangles.begin(); it != m_guiRectangles.end(); it++)
			m_renderWindow->draw(*it);

		for (std::vector<sf::CircleShape>::iterator it = m_guiCircles.begin(); it != m_guiCircles.end(); it++)
			m_renderWindow->draw(*it);

		for (std::vector<sf::Text>::iterator it = m_strings.begin(); it != m_strings.end(); it++)
			m_renderWindow->draw(*it);


		m_strings.clear();
		m_circles.clear();
		m_rectangles.clear();

		m_guiCircles.clear();
		m_guiRectangles.clear();
	}
}