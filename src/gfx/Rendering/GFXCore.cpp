#include "GFXCore.hpp"
#include <Texture/TextureManager.hpp>
#include <Font/TextCacheManager.hpp>
#include <RenderJob/RenderJobManager.hpp>
#include <DebugRender/DebugManager.hpp>
#include <View/ViewContainer.hpp>
#include <Map/MapManager.hpp>

namespace gfx
{
	GFXCore& GFXCoreInstance()
	{
		static GFXCore gfxCore;
		return gfxCore;
	}

	GFXCore::GFXCore()
	{

	}

	GFXCore::~GFXCore()
	{

	}

	void GFXCore::Initialize(sf::RenderWindow* renderWindow)
	{
		m_renderWindow = renderWindow;
		DebugManagerInstance().Initialize(renderWindow);

		MapManagerInstance().Initialize(renderWindow);
	}

	void GFXCore::Destroy()
	{
		TextCacheManagerInstance().ClearCache();
	}

	void GFXCore::Render(float dt)
	{
		RenderJobManagerInstance().Sort();
		RenderJobManagerInstance().Decode();

		std::vector<InstanceData>* instances = &RenderJobManagerInstance().GetInstances();

		sf::View defaultView = m_renderWindow->getView();
		int viewID = -1;
		int currentLayer = -1;

		/**********************************************************************************************/
		//Render all background layers
		/**********************************************************************************************/
		MapManagerInstance().RenderLayers(LayerType::LT_BACKGROUND, false);

		for (std::vector<InstanceData>::iterator it = instances->begin(); it != instances->end(); it++)
		{
			/**********************************************************************************************/
			//Render layers between currentLayer and layers
			/**********************************************************************************************/
			if (it->layer != currentLayer)
			{
				MapManagerInstance().RenderLayers(LayerType::LT_GROUND, currentLayer + 1, it->layer);
				currentLayer = it->layer;
			}


			if (it->view != viewID)
			{
				sf::View v = ViewContainerInstance().GetView(it->view);
				m_renderWindow->setView(v);
				viewID = it->view;
			}

			switch (it->objectType)
			{
				case OBJECT_TYPES::SPRITE:
				{
					m_renderWindow->draw(*((sf::Sprite*)it->data));
				}
				break;

				case OBJECT_TYPES::TEXT:
				{
					if (((TextInstanceData*)it->data)->cached)
					{
						sf::RenderTexture* rt = TextCacheManagerInstance().GetRenderTexture(((TextInstanceData*)it->data)->renderTextureID);

						if (rt == nullptr)
							continue;

						if (((TextInstanceData*)it->data)->updated)
						{
							rt->clear(sf::Color::Transparent);
							rt->setView(((TextInstanceData*)it->data)->view);
							rt->draw(*((TextInstanceData*)it->data)->text);
							rt->display();
						}

						sf::Sprite sprite(rt->getTexture());
						sprite.setPosition(((TextInstanceData*)it->data)->text->getPosition());
						m_renderWindow->draw(sprite);

						m_renderWindow->setView(m_renderWindow->getDefaultView());
					}
					else
					{
						m_renderWindow->draw(*((TextInstanceData*)it->data)->text);
					}
					
				}
				break;

				case OBJECT_TYPES::SHAPE:
				{
					m_renderWindow->draw(*(sf::Shape*)it->data);
				}
				break;
			}
		}

		//Render any ground layers that got left out
		MapManagerInstance().RenderLayers(LayerType::LT_GROUND, true);

		//Render all foreground layers
		MapManagerInstance().RenderLayers(LayerType::LT_FOREGROUND, false);
		
		//Render all postfx layers
		MapManagerInstance().RenderLayers(LayerType::LT_POSTFX, false);

		MapManagerInstance().ResetRenderStatus();

		//Debug
		m_renderWindow->setView(ViewContainerInstance().GetPrimaryView().view);
		DebugManagerInstance().Render(defaultView);
		RenderJobManagerInstance().ClearRenderJobs();
	}

}