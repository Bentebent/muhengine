#ifndef SRC_GFX_RENDERING_GFXCORE_HPP
#define SRC_GFX_RENDERING_GFXCORE_HPP

#include <SFML/Graphics.hpp>

namespace gfx
{
	class GFXCore
	{
	public:
		friend GFXCore& GFXCoreInstance();
		void Initialize(sf::RenderWindow* renderWindow);
		void Render(float dt);
		void Destroy();

	private:
		GFXCore();
		~GFXCore();

		sf::RenderWindow* m_renderWindow;
	};

	GFXCore& GFXCoreInstance();
}

#endif