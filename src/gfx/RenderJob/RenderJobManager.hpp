#ifndef SRC_GFX_RENDERING_RENDERJOB_RENDERJOBMANAGER_HPP
#define SRC_GFX_RENDERING_RENDERJOB_RENDERJOBMANAGER_HPP

#include "RenderJob.hpp"
#include <gfx/GFXInstanceData.hpp>

#include <bitmask/dynamic_bitmask.hpp>
#include <bitmask/bitmask_system.hpp>
#include <vector>

#include <SFML/Graphics.hpp>

#define USE_CURRENT_BUFFER	0
#define USE_NEXT_BUFFER		1
#define SWAP_JOB_BUFFERS	2

namespace gfx
{
	class RenderJobManager
	{
	public:
		friend RenderJobManager& RenderJobManagerInstance();

		void Initialize();

		void Sort();
		void Decode();
		void AddRenderJob(Bitmask bitmask, void* data);
		void ClearRenderJobs();
		std::vector<RenderJob>& GetRenderJobs();

		inline std::vector<InstanceData>& GetInstances()
		{
			return m_instances;
		}

	private:
		RenderJobManager();
		~RenderJobManager();

		std::vector<RenderJob> m_renderJobs;
		std::vector<InstanceData> m_instances;
		BitmaskSystem m_bitmaskSystem;
	};

	RenderJobManager& RenderJobManagerInstance();
}

#endif