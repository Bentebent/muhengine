#ifndef SRC_GFX_RENDERJOB_RENDERJOB_HPP
#define SRC_GFX_RENDERJOB_RENDERJOB_HPP

#include <bitmask/dynamic_bitmask.hpp>

namespace gfx
{
	struct RenderJob
	{
		Bitmask bitmask;
		void* data;
	};
}

#endif