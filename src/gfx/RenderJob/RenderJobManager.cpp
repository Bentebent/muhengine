#include "RenderJobManager.hpp"
#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXInstanceData.hpp>
#include <Texture/TextureManager.hpp>
#include <Font/FontManager.hpp>

#include <algorithm>
#include <cassert>
#include <iostream>

namespace gfx
{
	int compare_ints(const void* a, const void* b)   // comparison function
	{
		gfx::RenderJob arg1 = *reinterpret_cast<const gfx::RenderJob*>(a);
		gfx::RenderJob arg2 = *reinterpret_cast<const gfx::RenderJob*>(b);
		if (arg1.bitmask > arg2.bitmask) return -1;
		if (arg1.bitmask < arg2.bitmask) return 1;
		return 0;
	}

	RenderJobManager& RenderJobManagerInstance()
	{
		static RenderJobManager rjm;
		return rjm;
	}

	RenderJobManager::RenderJobManager()
	{
		m_renderJobs.reserve(100000);
		m_bitmaskSystem.Init();
	}

	RenderJobManager::~RenderJobManager()
	{

	}

	void RenderJobManager::Initialize()
	{
	}

	void RenderJobManager::Sort()
	{
		std::qsort(m_renderJobs.data(), m_renderJobs.size(), sizeof(RenderJob), compare_ints);
	}

	void RenderJobManager::Decode()
	{

		unsigned int command		= std::numeric_limits<decltype(command)>::max();
		unsigned int texture		= std::numeric_limits<decltype(texture)>::max();
		unsigned int font			= std::numeric_limits<decltype(font)>::max();
		unsigned int type			= std::numeric_limits<decltype(type)>::max();
		unsigned int view			= std::numeric_limits<decltype(view)>::max();
		unsigned int layer			= std::numeric_limits<decltype(layer)>::max();

		InstanceData instanceData;

		for (std::vector<RenderJob>::iterator it = m_renderJobs.begin(); it != m_renderJobs.end(); it++)
		{
			command = m_bitmaskSystem.Decode(it->bitmask, MaskType::COMMAND);

			if (command == COMMAND_TYPES::COMMAND)
			{
				//Parse whatever commands might be interesting
			}
			else
			{
				type = m_bitmaskSystem.Decode(it->bitmask, MaskType::TYPE);
				view = m_bitmaskSystem.Decode(it->bitmask, MaskType::VIEW);
				layer = m_bitmaskSystem.Decode(it->bitmask, MaskType::LAYER);

				switch (type)
				{
					case OBJECT_TYPES::SPRITE:
					{
						texture = m_bitmaskSystem.Decode(it->bitmask, MaskType::TEXTURE);
						sf::Texture* t = TextureManagerInstance().GetTexture(texture);

						((sf::Sprite*)it->data)->setTexture(*t);
						instanceData.data		= it->data;
						instanceData.objectType = OBJECT_TYPES::SPRITE;
						instanceData.view		= view;
						instanceData.layer		= layer;
						m_instances.push_back(instanceData);
					}
					break;

					case OBJECT_TYPES::TEXT:
					{
						font = m_bitmaskSystem.Decode(it->bitmask, MaskType::TEXTURE);
						sf::Font* f = FontManagerInstance().GetFont(font);

						((TextInstanceData*)it->data)->text->setFont(*f);

						instanceData.data		= it->data;
						instanceData.objectType = OBJECT_TYPES::TEXT;
						instanceData.view		= view;
						instanceData.layer		= layer;
						m_instances.push_back(instanceData);
					}
					break;

					case OBJECT_TYPES::SHAPE:
					{
						instanceData.data		= it->data;
						instanceData.objectType = OBJECT_TYPES::SHAPE;
						instanceData.view		= view;
						instanceData.layer		= layer;
						m_instances.push_back(instanceData);
					}
					break;

					case OBJECT_TYPES::TEXTURED_SHAPE:
					{
						texture = m_bitmaskSystem.Decode(it->bitmask, MaskType::TEXTURE);
						sf::Texture* t = TextureManagerInstance().GetTexture(texture);

						((sf::Shape*)it->data)->setTexture(t);

						instanceData.data		= it->data;
						instanceData.objectType = OBJECT_TYPES::SHAPE;
						instanceData.view		= view;
						instanceData.layer		= layer;
						m_instances.push_back(instanceData);
					}
				}

			}
		}
	}

	void RenderJobManager::AddRenderJob(Bitmask bitmask, void* data)
	{
		RenderJob rj;
		rj.bitmask = bitmask;
		rj.data = data;
		m_renderJobs.push_back(rj);
	}

	void RenderJobManager::ClearRenderJobs()
	{
		m_renderJobs.clear();
		m_instances.clear();
	}

	std::vector<RenderJob>& RenderJobManager::GetRenderJobs()
	{
		return m_renderJobs;
	}
}