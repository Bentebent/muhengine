#ifndef SRC_GFX_MAP_MAPMANAGER_HPP
#define SRC_GFX_MAP_MAPMANAGER_HPP

#define TILE_DRAWING_BUFFER 10

#include <map>
#include <gfx/GFXInstanceData.hpp>
#include <map/LayerType.hpp>
namespace gfx
{
	class MapManager
	{
	public:
		friend MapManager& MapManagerInstance();

		void Initialize(sf::RenderWindow* renderWindow);

		void SetMapInfo(int tileWidth, int tileHeight, int tileCountX, int tileCountY, float tileScaleX, float tileScaleY);
		void SetMapScale(int tileScaleX, int tileScaleY);
		void AddLayer(TileInstanceData*** layer, const char* name, int layerType, int size, int zIndex, float alpha, float scrollX, float scrollY, bool repeatX, bool repeatY);

		void RenderLayers(LayerType layerType, bool onlyRemaining);
		void RenderLayers(LayerType layerType, int zMin, int zMax);

		std::map<int, LayerInstanceData*> GetLayers(LayerType layerType);

		void ResetRenderStatus();

		void ClearMap();

	private:
		MapManager();
		~MapManager();

		//void RenderLayer(int size, TileInstanceData*** tileInstanceData, sf::View view, float scrollX, float scrollY);
		void RenderLayer(int size, LayerInstanceData* lid);

		int m_tileWidth;
		int m_tileHeight;
		int m_tileCountX;
		int m_tileCountY;

		float m_tileScaleX;
		float m_tileScaleY;

		std::map<int, LayerInstanceData*> m_backgroundLayers;
		std::map<int, LayerInstanceData*> m_groundLayers;
		std::map<int, LayerInstanceData*> m_objectLayers;
		std::map<int, LayerInstanceData*> m_foregroundLayers;
		std::map<int, LayerInstanceData*> m_postFXLayers;

		sf::RenderWindow* m_renderWindow;
	};

	MapManager& MapManagerInstance();
}

#endif