#include "MapManager.hpp"
#include <Map/LayerType.hpp>
#include <View/ViewContainer.hpp>
#include <iostream>
#include <glm/glm.hpp>

namespace gfx
{
	MapManager& MapManagerInstance()
	{
		static MapManager mm;
		return mm;
	}

	MapManager::MapManager()
	{

	}

	MapManager::~MapManager()
	{

	}

	void MapManager::Initialize(sf::RenderWindow* renderWindow)
	{
		m_renderWindow = renderWindow;
	}

	void MapManager::SetMapInfo(int tileWidth, int tileHeight, int tileCountX, int tileCountY, float tileScaleX, float tileScaleY)
	{
		m_tileWidth		= tileWidth;
		m_tileHeight	= tileHeight;
		m_tileCountX	= tileCountX;
		m_tileCountY	= tileCountY;
		m_tileScaleX	= tileScaleX;
		m_tileScaleY	= tileScaleY;
	}

	void MapManager::SetMapScale(int tileScaleX, int tileScaleY)
	{
		m_tileScaleX = tileScaleX;
		m_tileScaleY = tileScaleY;
	}


	//void MapManager::RenderLayer(int size, TileInstanceData*** tileInstanceData, sf::View view, float scrollX, float scrollY)
	void MapManager::RenderLayer(int size, LayerInstanceData* lid)
	{

		sf::View v = lid->view;
		float vx = v.getCenter().x;
		float vy = v.getCenter().y;
		float hsx = v.getSize().x / 2.0f;
		float hsy = v.getSize().y / 2.0f;

		int scaledSizeX = m_tileWidth * m_tileScaleX;
		int scaledSizeY = m_tileHeight * m_tileScaleY;

		int tilesInFrameX = v.getSize().x / scaledSizeX + TILE_DRAWING_BUFFER * 2;

		int startX = (vx - ((hsx / scaledSizeX + TILE_DRAWING_BUFFER) * scaledSizeX)) / scaledSizeX;
		int endX = (vx + ((hsx / scaledSizeX + TILE_DRAWING_BUFFER) * scaledSizeX)) / scaledSizeX;

		int startY = (vy - ((hsy / scaledSizeY + TILE_DRAWING_BUFFER) * scaledSizeY)) / scaledSizeY;
		int endY = (vy + ((hsy / scaledSizeY + TILE_DRAWING_BUFFER) * scaledSizeY)) / scaledSizeY;

		lid->renderingDrawCalls.clear();
		int screensFromZeroX = 0;
		int screensFromZeroY = 0;


		if (lid->repeatX)
			screensFromZeroX = endX / m_tileCountX;
		
		if (lid->repeatY)
			screensFromZeroY = endY / m_tileCountY;

		int count = 0;

		//No support for repeats in x < 0 and y < 0
		for (int x = 0; x <= screensFromZeroX; x++)
		{
			for (int y = 0; y <= screensFromZeroY; y++)
			{
				int sX = startX - m_tileCountX * x;
				int eX = endX - m_tileCountX * x;

				sX = glm::clamp(sX, 0, m_tileCountX);
				eX = glm::clamp(eX, 0, m_tileCountX);

				if ((sX == 0 && eX == 0) || (sX == m_tileCountX && eX == m_tileCountX))
					continue;

				int sY = startY - m_tileCountY * y;
				int eY = endY - m_tileCountX * y;

				sY = glm::clamp(sY, 0, m_tileCountY);
				eY = glm::clamp(eY, 0, m_tileCountY);

				if ((sY == 0 && eY == 0) || (sY == m_tileCountY && eY == m_tileCountY))
					continue;

				LayerDrawCall ldc;
				ldc.position = sf::Vector2f(m_tileCountX * scaledSizeX * x, m_tileCountY * scaledSizeY * y);
				ldc.startX = sX;
				ldc.endX = eX;
				ldc.startY = sY;
				ldc.endY = eY;

				lid->renderingDrawCalls[count++] = ldc;
			}
		}

		for (std::map<int, LayerDrawCall>::iterator it = lid->renderingDrawCalls.begin(); it != lid->renderingDrawCalls.end(); it++)
		{
			for (int x = it->second.startX; x < it->second.endX; x++)
			{
				for (int y = it->second.startY; y < it->second.endY; y++)
				{
					TileInstanceData* tid = lid->tileInstanceData[x][y];
		
					if (tid == nullptr)
						continue;
					sf::Vector2f originScale = tid->renderTile->getScale();

					tid->renderTile->setTextureRect(*tid->sourceRectangle);
					tid->renderTile->setPosition(x * scaledSizeX + it->second.position.x, y * scaledSizeY + it->second.position.y);
					tid->renderTile->setScale(originScale.x * m_tileScaleX, originScale.y * m_tileScaleY);
					tid->renderTile->setOrigin(scaledSizeX / 2.0f, scaledSizeY / 2.0f);
					tid->renderTile->setFillColor(sf::Color(255, 255, 255, lid->alpha * 255));

					m_renderWindow->draw(*tid->renderTile);

					tid->renderTile->setScale(originScale);
				}
			}
		}
	}

	void MapManager::RenderLayers(LayerType layerType, bool onlyRemaining)
	{
		sf::Vector2f currentViewPos = ViewContainerInstance().GetPrimaryView().view.getCenter();
		sf::Vector2f oldPos = sf::Vector2f(ViewContainerInstance().GetPrimaryView().oldX, ViewContainerInstance().GetPrimaryView().oldY);
		sf::Vector2f delta = currentViewPos - oldPos;
		
		std::map<int, LayerInstanceData*> layers;

		switch (layerType)
		{
			case LayerType::LT_BACKGROUND:
			{
				layers = m_backgroundLayers;
			}
			break;

			case LayerType::LT_FOREGROUND:
			{
				layers = m_foregroundLayers;
			}
			break;

			case LayerType::LT_GROUND:
			{
				layers = m_groundLayers;
			}
			break;

			case LayerType::LT_OBJECT:
			{
				layers = m_objectLayers;
			}
			break;

			case LayerType::LT_POSTFX:
			{
				layers = m_postFXLayers;
			}
			break;
		}

		for (std::map<int, LayerInstanceData*>::iterator it = layers.begin(); it != layers.end(); it++)
		{
			if (onlyRemaining && it->second->rendered)
				continue;
		
			int size = it->second->size;
			it->second->rendered = true;
		
			it->second->view.move(delta.x * it->second->scrollX, delta.y * it->second->scrollY);
			m_renderWindow->setView(it->second->view);
		
			RenderLayer(size, it->second);

			//RenderLayer(size, it->second->tileInstanceData, it->second->view, it->second->scrollX, it->second->scrollY);
		}
	}

	void MapManager::RenderLayers(LayerType layerType, int zMin, int zMax)
	{
		sf::Vector2f currentViewPos = ViewContainerInstance().GetPrimaryView().view.getCenter();
		sf::Vector2f oldPos = sf::Vector2f(ViewContainerInstance().GetPrimaryView().oldX, ViewContainerInstance().GetPrimaryView().oldY);
		sf::Vector2f delta = currentViewPos - oldPos;

		std::map<int, LayerInstanceData*> layers;


		switch (layerType)
		{
			case LayerType::LT_BACKGROUND:
				{
					layers = m_backgroundLayers;
				}
				break;
			case LayerType::LT_FOREGROUND:
				{
					layers = m_foregroundLayers;
				}
				break;
			case LayerType::LT_GROUND:
				{
					layers = m_groundLayers;
				}
				break;
			case LayerType::LT_OBJECT:
				{
					layers = m_objectLayers;
				}			break;
			case LayerType::LT_POSTFX:
				{
					layers = m_postFXLayers;
				}
				break;
		}

		for (std::map<int, LayerInstanceData*>::iterator it = layers.begin(); it != layers.end(); it++)
		{
			if (it->second->zIndex < zMin)
				continue;
			else if (it->second->zIndex > zMax)
				return;
		
			int size = it->second->size;
			it->second->rendered = true;
		
			it->second->view.move(delta.x * it->second->scrollX, delta.y * it->second->scrollY);
			m_renderWindow->setView(it->second->view);
		
			RenderLayer(size, it->second);
		}
	}

	std::map<int, LayerInstanceData*> MapManager::GetLayers(LayerType layerType)
	{
		switch (layerType)
		{
		case LayerType::LT_BACKGROUND:
			return m_backgroundLayers;
			break;
		case LayerType::LT_FOREGROUND:
			return m_foregroundLayers;
			break;
		case LayerType::LT_GROUND:
			return m_groundLayers;
			break;
		case LayerType::LT_OBJECT:
			return m_objectLayers;
			break;
		case LayerType::LT_POSTFX:
			return m_postFXLayers;
			break;
		}
	}

	void MapManager::ClearMap()
	{
		for (std::map<int, LayerInstanceData*>::iterator it = m_backgroundLayers.begin(); it != m_backgroundLayers.end(); it++)
			delete it->second;

		for (std::map<int, LayerInstanceData*>::iterator it = m_groundLayers.begin(); it != m_groundLayers.end(); it++)
			delete it->second;

		for (std::map<int, LayerInstanceData*>::iterator it = m_objectLayers.begin(); it != m_objectLayers.end(); it++)
			delete it->second;

		for (std::map<int, LayerInstanceData*>::iterator it = m_foregroundLayers.begin(); it != m_foregroundLayers.end(); it++)
			delete it->second;

		for (std::map<int, LayerInstanceData*>::iterator it = m_postFXLayers.begin(); it != m_postFXLayers.end(); it++)
			delete it->second;

		m_backgroundLayers.clear();
		m_groundLayers.clear();
		m_objectLayers.clear();
		m_foregroundLayers.clear();
		m_postFXLayers.clear();
	}


	void MapManager::ResetRenderStatus()
	{
		for (std::map<int, LayerInstanceData*>::iterator it = m_backgroundLayers.begin(); it != m_backgroundLayers.end(); it++)
			it->second->rendered = false;
		
		for (std::map<int, LayerInstanceData*>::iterator it = m_groundLayers.begin(); it != m_groundLayers.end(); it++)
			it->second->rendered = false;
		
		for (std::map<int, LayerInstanceData*>::iterator it = m_objectLayers.begin(); it != m_objectLayers.end(); it++)
			it->second->rendered = false;
		
		for (std::map<int, LayerInstanceData*>::iterator it = m_foregroundLayers.begin(); it != m_foregroundLayers.end(); it++)
			it->second->rendered = false;
		
		for (std::map<int, LayerInstanceData*>::iterator it = m_postFXLayers.begin(); it != m_postFXLayers.end(); it++)
			it->second->rendered = false;
	}

	void MapManager::AddLayer(TileInstanceData*** layer, const char* name, int layerType, int size, int zIndex, float alpha, float scrollX, float scrollY, bool repeatX, bool repeatY)
	{
		LayerInstanceData* lid = new LayerInstanceData();		
		lid->tileInstanceData = layer;
		lid->layerType = layerType;
		lid->size = size;
		lid->zIndex = zIndex;
		lid->alpha = alpha;
		lid->scrollX = scrollX;
		lid->scrollY = scrollY;
		lid->repeatX = repeatX;
		lid->repeatY = repeatY;
		lid->name = std::string(name);
		lid->view = ViewContainerInstance().GetPrimaryView().view;

		switch (layerType)
		{
		case LayerType::LT_BACKGROUND:
			m_backgroundLayers[zIndex] = lid;
			break;
		case LayerType::LT_FOREGROUND:
			m_foregroundLayers[zIndex] = lid;
			break;
		case LayerType::LT_GROUND:
			m_groundLayers[zIndex] = lid;
			break;
		case LayerType::LT_OBJECT:
			m_objectLayers[zIndex] = lid;
			break;
		case LayerType::LT_POSTFX:
			m_postFXLayers[zIndex] = lid;
			break;
		}
	}
}