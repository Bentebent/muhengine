#ifndef SRC_GFX_VIEW_VIEW_HPP
#define SRC_GFX_VIEW_VIEW_HPP

#include <SFML/Graphics.hpp>

namespace gfx
{
	struct View
	{
		sf::View view;
		float oldX;
		float oldY;
		bool primary;
	};
}

#endif