#include "ViewContainer.hpp"

namespace gfx
{
	ViewContainer& ViewContainerInstance()
	{
		static ViewContainer vc;
		return vc;
	}

	ViewContainer::ViewContainer()
	{
		m_defaultView.view = sf::View(sf::Vector2f(320, 240), sf::Vector2f(640, 480));
		m_defaultView.oldX = 320;
		m_defaultView.oldY = 240;
	}

	ViewContainer::~ViewContainer()
	{

	}

	void ViewContainer::SetView(sf::View view, unsigned int id)
	{
		m_views[id].view = view;

		sf::Vector2f pos = view.getCenter();
		m_views[id].oldX = pos.x;
		m_views[id].oldY = pos.y;

	}

	void ViewContainer::SetViewPosition(float x, float y, int id)
	{
		m_views[id].oldX = m_views[id].view.getCenter().x;
		m_views[id].oldY = m_views[id].view.getCenter().y;

		m_views[id].view.setCenter(x, y);
	}
}