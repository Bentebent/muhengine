#ifndef SRC_GFX_VIEW_VIEWCONTAINER_HPP
#define SRC_GFX_VIEW_VIEWCONTAINER_HPP

#include <SFML/Graphics.hpp>

#include <map>
#include <View/View.hpp>
namespace gfx
{
	class ViewContainer
	{
	public:
		friend ViewContainer& ViewContainerInstance();

		void SetView(sf::View view, unsigned int id);
		void SetViewPosition(float x, float y, int id);

		void SetPrimaryView(unsigned int id)
		{
			std::map<int, View>::iterator it = m_views.find(id);

			if (it != m_views.end())
			{
				int primaryViewID = -1;
				GetPrimaryView(primaryViewID);

				m_views[primaryViewID].primary = false;
				m_views[id].primary = true;
			}
		}

		View GetPrimaryView()
		{
			for(std::map<int, View>::iterator it = m_views.begin(); it != m_views.end();  it++)
			{
				if (it->second.primary)
				{
					return it->second;
				}
			}
			return m_defaultView;
		}

		View GetPrimaryView(int& id)
		{
			for (std::map<int, View>::iterator it = m_views.begin(); it != m_views.end(); it++)
			{
				if (it->second.primary)
				{
					id = it->first;
					return it->second;
				}
			}
			return m_defaultView;
		}

		sf::View GetView(unsigned int id) 
		{
			std::map<int, View>::iterator it = m_views.find(id);

			if (it != m_views.end())
			{
				return m_views[id].view;
			}
			return m_defaultView.view;
		}

	private:
		ViewContainer();
		~ViewContainer();

		std::map<int, View> m_views;

		View m_defaultView;
	};

	ViewContainer& ViewContainerInstance();
}

#endif