#include <Rendering/GFXCore.hpp>
#include <Texture/TextureManager.hpp>
#include <Font/FontManager.hpp>
#include <Font/TextCacheManager.hpp>
#include <RenderJob/RenderJobManager.hpp>
#include <gfx/GFXInterface.hpp>
#include <DebugRender/DebugManager.hpp>
#include <View/ViewContainer.hpp>
#include <map/LayerType.hpp>

#include <Map/MapManager.hpp>

namespace gfx
{
	int Initialize(int screenWidth, int screenHeight, void* renderWindow)
	{
		GFXCoreInstance().Initialize((sf::RenderWindow*)renderWindow);
		return 0;
	}

	int Destroy()
	{
		GFXCoreInstance().Destroy();
		return 0;
	}

	int Render(float dt)
	{
		GFXCoreInstance().Render(dt);
		return 0;
	}

	int Execute(Bitmask bitmask, void* data)
	{
		RenderJobManagerInstance().AddRenderJob(bitmask, data);
		return 0;
	}
}

namespace gfx
{
	namespace content
	{
		int LoadTexture(unsigned int& id, const char* path)
		{
			TextureManagerInstance().LoadTexture(id, path);
			return 0;
		}

		int DeleteTexture(unsigned int id)
		{
			TextureManagerInstance().DeleteTexture(id);
			return 0;
		}


		void ReloadTextures()
		{
			TextureManagerInstance().ReloadTextures();
		}


		int LoadFont(unsigned int& id, const char* path)
		{
			FontManagerInstance().LoadFont(id, path);
			return 0;
		}

		void* GetFont(unsigned int id)
		{
			return FontManagerInstance().GetFont(id);
		}
	}

	namespace view
	{
		int SetView(void* view, int id)
		{
			ViewContainerInstance().SetView(*(sf::View*)view, id);
			return 0;
		}

		int SetViewPosition(float x, float y, int id)
		{
			ViewContainerInstance().SetViewPosition(x, y, id);
			return 0;
		}

		int GetView(void*& view, int id)
		{
			view = (void*)&ViewContainerInstance().GetView(id);
			return 0;
		}

		int SetPrimaryView(int id)
		{
			ViewContainerInstance().SetPrimaryView(id);
			return 0;
		}

		int GetPrimaryView(void*& view, int& id)
		{
			view = (void*)&ViewContainerInstance().GetPrimaryView(id).view;
			return 0;
		}
	}

	namespace text
	{
		int CreateCache(unsigned int& id, unsigned int width, unsigned int height)
		{
			TextCacheManagerInstance().CreateTextCache(id, width, height);
			return 0;
		}

		int UpdateCache(unsigned int& id, unsigned int width, unsigned int height)
		{
			TextCacheManagerInstance().UpdateTextCache(id, width, height);
			return 0;
		}

		int RemoveCache(unsigned int id)
		{
			TextCacheManagerInstance().DeleteTextCache(id);
			return 0;
		}
	}

	namespace map
	{
		int AddLayer(void** layer, const char* name, int layerType, int size, int zIndex, float alpha, float scrollX, float scrollY, bool repeatX, bool repeatY)
		{
			MapManagerInstance().AddLayer((gfx::TileInstanceData***)layer, name, layerType, size, zIndex, alpha, scrollX, scrollY, repeatX, repeatY);
			return 0;
		}

		int SetMapInfo(int tileWidth, int tileHeight, int tileCountX, int tileCountY, float tileScaleX, float tileScaleY)
		{
			MapManagerInstance().SetMapInfo(tileWidth, tileHeight, tileCountX, tileCountY, tileScaleX, tileScaleY);
			return 0;
		}

		int SetMapScale(int scaleX, int scaleY)
		{
			MapManagerInstance().SetMapScale(scaleX, scaleY);
			return 0;
		}

		int RemoveMap()
		{
			MapManagerInstance().ClearMap();
			return 0;
		}
	}

	namespace debug
	{
		int RenderText(float x, float y, const char* text, int size, float r, float g, float b, float a)
		{
			DebugManagerInstance().AddString(x, y, text, size, r, g, b, a);
			return 0;
		}

		int RenderCircle(float x, float y, float radius, float r, float g, float b, float a, bool gui)
		{
			DebugManagerInstance().AddCircle(x, y, radius, r, g, b, a, gui);
			return 0;
		}

		int RenderRectangle(float x, float y, float w, float h, float r, float g, float b, float a, bool gui)
		{
			DebugManagerInstance().AddRectangle(x, y, w, h, r, g, b, a, gui);
			return 0;
		}

		int RenderPolygon(float x, float y, float radius, int pointCount, float r, float g, float b, float a, bool gui)
		{
			DebugManagerInstance().AddPolygon(x, y, radius, pointCount, r, g, b, a, gui);
			return 0;
		}

	}
}