#ifndef SRC_SHARED_BITMASK_BITMASK_TYPES_HPP
#define SRC_SHARED_BITMASK_BITMASK_TYPES_HPP

#include <cstdint>

/*	Length of the bitmask to use */
typedef uint64_t Bitmask;
static const Bitmask mask_max = UINT64_MAX;
static const unsigned short bitmask_length = 64U;

typedef unsigned int BitmaskType;

#endif