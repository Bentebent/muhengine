#ifndef SRC_SHARED_BITMASK_BITMASK_DEFINITIONS_HPP
#define SRC_SHARED_BITMASK_BITMASK_DEFINITIONS_HPP

#include "bitmask_types.hpp"

/*!	Enumeration of bitmask types */
enum MaskType : BitmaskType
{
	COMMAND,
	LAYER,
	DEPTH,
	TYPE,
	TEXTURE,
	VIEW,
	NUM_TYPES
};

/*!	bitmask_lengths is an array containing information about the
length of a bitmask entry defined in the "type" enumeration above. */
static const unsigned short bitmask_lengths[MaskType::NUM_TYPES] =
{
	1U, // COMMAND
	6U, //Layer
	36U, // DEPTH
	5U, //Type
	13U, // TEXTURE
	3U, //VIEW
};


#endif