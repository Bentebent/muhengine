#ifndef SRC_SHARED_GFX_GFXINTERFACE_HPP
#define SRC_SHARED_GFX_GFXINTERFACE_HPP

#ifdef _WIN64
#ifdef GFX_DLL_EXPORT
#define DLL_API __declspec(dllexport)
#else
#define DLL_API __declspec(dllimport)
#endif
#else
#define DLL_API 
#endif

#include <bitmask/bitmask_types.hpp>


namespace gfx
{
	DLL_API int Initialize(int screenWidth, int screenHeight, void* renderWindow);
	DLL_API int Destroy();

	

	DLL_API int Execute(Bitmask bitmask, void* data);

	DLL_API int Render(float dt);
}

namespace gfx
{
	namespace content
	{
		DLL_API int LoadTexture(unsigned int& id, const char* path);
		DLL_API int DeleteTexture(unsigned int id);

		DLL_API int LoadFont(unsigned int& id, const char* path);

		DLL_API void* GetFont(unsigned int id);

		DLL_API void ReloadTextures();
	}

	namespace text
	{
		DLL_API int CreateCache(unsigned int& id, unsigned int width, unsigned int height);
		DLL_API int UpdateCache(unsigned int& id, unsigned int width, unsigned int height);
		DLL_API int RemoveCache(unsigned int id);
	}

	namespace view
	{
		DLL_API int SetView(void* view, int id);
		DLL_API int SetViewPosition(float x, float y, int id);

		DLL_API int GetView(void*& view, int id);

		DLL_API int SetPrimaryView(int id);
		DLL_API int GetPrimaryView(void*& view, int& id);
	}

	namespace map
	{
		DLL_API int AddLayer(void** layer, const char* name, int layerType, int size, int zIndex, float alpha, float scrollX, float scrollY, bool repeatX, bool repeatY);
		DLL_API int SetMapInfo(int tileWidth, int tileHeight, int tileCountX, int tileCountY, float tileScaleX, float tileScaleY);
		DLL_API int SetMapScale(int scaleX, int scaleY);
		DLL_API int	RemoveMap();
	}

	namespace debug
	{
		DLL_API int RenderText(float x, float y, const char* text, int size, float r, float g, float b, float a);

		DLL_API int RenderCircle(float x, float y, float radius, float r, float g, float b, float a, bool gui);
		DLL_API int RenderRectangle(float x, float y, float w, float h, float r, float g, float b, float a, bool gui);
		DLL_API int RenderPolygon(float x, float y, float radius, int pointCount, float r, float g, float b, float a, bool gui);
	}
}

#endif