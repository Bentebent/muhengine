#ifndef SRC_SHARED_GFX_GFXINSTANCEDATA_HPP
#define SRC_SHARED_GFX_GFXINSTANCEDATA_HPP

#include <glm/glm.hpp>
#include <SFML/Graphics.hpp>

#include <gfx/GFXBitmaskDefinitions.hpp>
#include <gfx/GFXShapeType.hpp>

namespace gfx
{
	struct InstanceData
	{
		unsigned int objectType;
		unsigned int subType;
		unsigned int view;
		unsigned int layer;
		void* data;
	};

	struct SpriteInstanceData
	{
		sf::Sprite* sprite;
	};

	struct TextInstanceData
	{
		sf::Text* text;
		sf::View view;
		bool updated;
		bool cached;
		int renderTextureID;
	};

	struct ShapeInstanceData
	{
		sf::Shape* shape;
		unsigned int shapeType;
	};

	struct TileInstanceData
	{
		//int guid;
		//int x;
		//int y;
		sf::IntRect* sourceRectangle;
		sf::RectangleShape* renderTile;
	};

	struct LayerDrawCall
	{
		sf::Vector2f position;
		int startX;
		int endX;
		int startY;
		int endY;
	};

	struct LayerInstanceData
	{
		int size;
		int zIndex;

		float alpha;

		int layerType;

		float scrollX;
		float scrollY;

		bool repeatX;
		bool repeatY;

		std::string name;

		bool rendered;
		bool active;

		TileInstanceData*** tileInstanceData;

		sf::View view;

		std::map<int, LayerDrawCall> renderingDrawCalls;
	};
}

#endif