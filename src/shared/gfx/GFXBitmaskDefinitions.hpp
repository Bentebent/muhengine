#ifndef SRC_SHARED_GFX_GFXBITMASKDEFINITIONS_HPP
#define SRC_SHARED_GFX_GFXBITMASKDEFINITIONS_HPP

namespace gfx
{
	namespace COMMAND_TYPES
	{
		static const unsigned int COMMAND = 1;
		static const unsigned int RENDER = 0;
	}

	namespace OBJECT_TYPES
	{
		static const unsigned int SPRITE = 5;
		static const unsigned int TEXTURED_SHAPE = 4;
		static const unsigned int SHAPE = 3;
		static const unsigned int TEXT = 2;
	}
}


#endif