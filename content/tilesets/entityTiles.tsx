<?xml version="1.0" encoding="UTF-8"?>
<tileset name="entityTiles" tilewidth="16" tileheight="16" tilecount="4">
 <image source="../maps/mockup/entityTiles.png" width="32" height="32"/>
 <tile id="0">
  <properties>
   <property name="script" value="cherry_one.lua"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="script" value="cherry_two.lua"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="script" value="flagpole.lua"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="script" value="flagpole.lua"/>
  </properties>
 </tile>
</tileset>
