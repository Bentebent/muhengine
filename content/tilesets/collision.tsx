<?xml version="1.0" encoding="UTF-8"?>
<tileset name="collision_tileset" tilewidth="16" tileheight="16" tilecount="16">
 <image source="../textures/collision_tileset.png" width="64" height="64"/>
 <tile id="0">
  <properties>
   <property name="leftTop" value="1"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="leftTop" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="rightBottom" value="1"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="leftBottom" value="1"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="rightBottom" value="1"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="leftTop" value="1"/>
  </properties>
 </tile>
 <tile id="7">
  <properties>
   <property name="rightBottom" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="8">
  <properties>
   <property name="leftTop" value="1"/>
   <property name="rightBottom" value="1"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="leftTop" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="leftTop" value="1"/>
   <property name="rightBottom" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="leftTop" value="1"/>
   <property name="rightBottom" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="leftTop" value="1"/>
   <property name="rightBottom" value="1"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="leftBottom" value="1"/>
   <property name="rightBottom" value="1"/>
   <property name="rightTop" value="1"/>
  </properties>
 </tile>
</tileset>
