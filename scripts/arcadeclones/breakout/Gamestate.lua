paddleTexture = LoadTexture("content/arcadeclones/breakout/paddle.png")  
ballTexture = LoadTexture("content/arcadeclones/breakout/ball.png")
fontid1    = LoadFont("content/fonts/upheavtt.ttf")

myBitmask = uint64(0)
myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 1)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", paddleTexture)

paddle = CreateLevelEntity()
AddRenderingComponent(paddle, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={32, 8}, sourceRectangle={0,0,64,16}})  
AddTransformationComponent(paddle, {rotation = 0, position = {320, 460}, scale = {1.0, 1.0}})

paddleMaxSpeed = 10
paddleSpeed = 0
paddleDirection = 0
paddleAcceleration = 70
paddleDeceleration = 70

ballBitmask = uint64(0)
ballBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
ballBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
ballBitmask = SetBitmaskValue(myBitmask, "LAYER", 1)
ballBitmask = SetBitmaskValue(myBitmask, "TEXTURE", ballTexture)

ball = CreateLevelEntity()
AddRenderingComponent(ball, {renderMe = true, bitmask = ballBitmask, color = {255, 255, 255, 255}, origin={8, 8}, sourceRectangle={0,0,16,16}})  
AddTransformationComponent(ball, {rotation = 0, position = {320, 440}, scale = {1.0, 1.0}})

ballSpeed = 0
ballDirectionX = 1
ballDirectionY = 1

bricks = {}
count = 0
for x = -4, 4 do
    for y = -2, 2 do
        local brick = {}
        brick.x = x * 64 + 320 + 5 * x
        brick.y = 128 + y * 16 + 5 * y
        brick.w = 64
        brick.h = 16
        brick.entity = CreateLevelEntity()
        AddRenderingComponent(brick.entity, {renderMe = true, bitmask = myBitmask, color = {0, 0, 255, 255}, origin={32, 8}, sourceRectangle={0,0,64,16}})  
        AddTransformationComponent(brick.entity, {rotation = 0, position = {brick.x, brick.y}, scale = {1.0, 1.0}})

        table.insert(bricks, brick)
    end
end

--Text test
textBitmask = uint64(0)
textBitmask = SetBitmaskValue(textBitmask, "COMMAND", 0)
textBitmask = SetBitmaskValue(textBitmask, "LAYER", 0)
textBitmask = SetBitmaskValue(textBitmask, "TYPE", 2)
textBitmask = SetBitmaskValue(textBitmask, "TEXTURE", fontid1)

scoreText = CreateLevelEntity()
AddRenderingComponent(scoreText, {renderMe = true, bitmask = textBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(scoreText, {rotation = 0, position = {10, 0}, scale = {1, 1}})
AddTextComponent(scoreText, {text="Score:", alignment=0, characterSize=20, fontID=fontid1, textBox={640, 480}, cacheMe=true})

scoreText = CreateLevelEntity()
AddRenderingComponent(scoreText, {renderMe = true, bitmask = textBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(scoreText, {rotation = 0, position = {90, 0}, scale = {1, 1}})
AddTextComponent(scoreText, {text="0", alignment=0, characterSize=20, fontID=fontid1, textBox={640, 480}, cacheMe=true})

score = 0

function CreateBall(paddleX, paddleY)
    ballBitmask = uint64(0)
    ballBitmask = SetBitmaskValue(ballBitmask, "COMMAND", 0)
    ballBitmask = SetBitmaskValue(ballBitmask, "TYPE", 5)
    ballBitmask = SetBitmaskValue(ballBitmask, "LAYER", 1)
    ballBitmask = SetBitmaskValue(ballBitmask, "TEXTURE", ballTexture)

    newBall = CreateLevelEntity()
    AddRenderingComponent(newBall, {renderMe = true, bitmask = ballBitmask, color = {255, 255, 255, 255}, origin={8, 8}, sourceRectangle={0,0,16,16}})  
    AddTransformationComponent(newBall, {rotation = 0, position = {paddleX, paddleY - 20}, scale = {1.0, 1.0}})
    return newBall
end

function Update(dt, owner)
    paddlePosition = GetPosition(paddle)
    ballPosition = GetPosition(ball)

    ControlPaddle(dt, paddlePosition)
    UpdateBall(dt, ballPosition, paddlePosition)
end

function ControlPaddle(dt, position)

    if SingleButtonPress("Space") and ballSpeed == 0 then
        ballSpeed = 5
        launchX = math.random(0, 100)

        if launchX < 50 then
            ballDirectionX = 1
        else
            ballDirectionX = -1
        end

        ballDirectionY = -1
    end

    if ButtonPressed("Left") then
        paddleDirection = -1
        paddleSpeed = paddleSpeed + paddleAcceleration * dt  * paddleDirection
    elseif ButtonPressed("Right") then
        paddleDirection = 1
        paddleSpeed = paddleSpeed + paddleAcceleration * dt * paddleDirection
    else
        if paddleSpeed > 0.5 or paddleSpeed < -0.5 then
            paddleSpeed = paddleSpeed - paddleDeceleration * dt * paddleDirection
        else
            paddleSpeed = 0
        end
    end

    if position[0] < 32 then
        position[0] = 32
        paddleSpeed = 0
    elseif position[0] > 608 then
        position[0] = 608
        paddleSpeed = 0
    end

    paddleSpeed = Clamp(paddleMaxSpeed, -paddleMaxSpeed, paddleSpeed)

    position[0] = position[0] + paddleSpeed

    SetPosition(paddle, position)
end

function UpdateBall(dt, ballPosition, paddlePosition)

    local destroyed = false
    local entryTime = 1
    local normalX = 0
    local normalY = 0
     
    entryTime, normalX, normalY = SweptAABB(ballPosition[0] - 8, ballPosition[1] - 8, 16, 16, ballSpeed * ballDirectionX, ballSpeed * ballDirectionY, paddlePosition[0] - 32, paddlePosition[1] - 8, 64, 16)
    local killMe = -1
    local killKey = -1
    local count = 0
    local posX = paddlePosition[0]
    local posY = paddlePosition[1]
    local playerHit = true

    for key,value in pairs(bricks) do 
        tempTime, nX, nY = SweptAABB(ballPosition[0] - 8, ballPosition[1] - 8, 16, 16, ballSpeed * ballDirectionX, ballSpeed * ballDirectionY, value.x - 32, value.y - 8, 64, 16)
        if tempTime < entryTime then
            killMe = value.entity
            killKey = key
            entryTime = tempTime
            normalX = nX
            normalY = nY
            posX = value.x
            posY = value.y
            playerHit = false
            score = score + 100
            SetText(scoreText, score)
        end 
    end

    remainingTime = 1 - entryTime

    if remainingTime > 0 then
        normalizedX, normalizedY = Normalize(ballDirectionX, ballDirectionY)

        ballPosition[0] = ballPosition[0] + ballSpeed * normalizedX * dt * entryTime
        ballPosition[1] = ballPosition[1] + ballSpeed * normalizedY * dt * entryTime

        dist = Distance(ballPosition[0], posX, ballPosition[1], posY)
       
        if math.abs(normalX) > 0.0001 then
            ballDirectionX = ballDirectionX * -1
        end
        if math.abs(normalY) > 0.0001 then
            ballDirectionY = ballDirectionY * -1
        end

        dist = dist / 64

        if dist > -0.3 and dist < 0.3 then
            dist = dist * -1
        end

        if ballDirectionX < 0 then
            ballDirectionX = ballDirectionX - dist
        else
            ballDirectionX = ballDirectionX + dist
        end

        normalizedX, normalizedY = Normalize(ballDirectionX, ballDirectionY)

        ballPosition[0] = ballPosition[0] + ballSpeed * normalizedX * dt * remainingTime * 2
        ballPosition[1] = ballPosition[1] + ballSpeed * normalizedY * dt * remainingTime * 2
        SetPosition(ball, ballPosition)

        if killMe > -1 then
            DestroyEntity(killMe)
            bricks[killKey] = nil
        end

    else
        if ballPosition[0] < 8 then
            ballDirectionX = 1
        elseif ballPosition[0] > 632 then
            ballDirectionX = -1
        end

        if ballPosition[1] < 8 then
            ballDirectionY = 1
        elseif ballPosition[1] > 472 then
            ballDirectionY = -1
            destroyed = true
            DestroyEntity(ball)
            ball = CreateBall(paddlePosition[0], paddlePosition[1])
            ballSpeed = 0
        end

        if not destroyed then
            if ballSpeed == 0 then
                ballPosition[0] = paddlePosition[0]
            else
                normalizedX, normalizedY = Normalize(ballDirectionX, ballDirectionY)
                ballPosition[0] = ballPosition[0] + ballSpeed * normalizedX
                ballPosition[1] = ballPosition[1] + ballSpeed * normalizedY
            end
       
            SetPosition(ball, ballPosition)
        end
    end
end

function BoxCollision(x1, y1, w1, h1, x2, y2, w2, h2)
    return math.abs(x1 - x2) * 2 < (w1 + w2) and math.abs(y1 - y2) * 2 < (h1 + h2)
end

function SweptAABB(x1, y1, w1, h1, vx1, vy1, x2, y2, w2, h2)
    local xInvEntry = 0
    local yInvEntry = 0
    local xInvExit = 0 
    local yInvExit = 0

    if vx1 > 0 then
        xInvEntry = x2 - (x1 + w1)
        xInvExit = (x2 + w2) - x1
    else
        xInvEntry = (x2 + w2) - x1
        xInvExit = x2 - (x1 + w1)
    end

    if vy1 > 0 then
        yInvEntry = y2 - (y1 + h1)
        yInvExit = (y2 + h2) - y1
    else
        yInvEntry = (y2 + h2) - y1
        yInvExit = y2 - (y1 + h1)
    end

    --find time of collision and time of leaving for each axis (if statement is to prevent divide by zero)
    local xEntry = 0
    local yEntry = 0
    local xExit = 0
    local yExit = 0

    if vx1 == 0.0 then
        xEntry = -5e+20
        xExit = 5e+20
    else
        xEntry = xInvEntry / vx1
        xExit = xInvExit / vx1
    end

    if vy1 == 0.0 then
        yEntry = -5e+20
        yExit = 5e+20
    else
        yEntry = yInvEntry / vy1
        yExit = yInvExit / vy1
    end

    --find the earliest/latest times of collision
    local entryTime = math.max(xEntry, yEntry)
    local exitTime = math.min(xExit, yExit)

    local normalx = 0
    local normaly = 0

    --if there was no collision
    if entryTime > exitTime or xEntry < 0.0 and yEntry < 0.0 or xEntry > 1.0 or yEntry > 1.0 then
        normalx = 0.0
        normaly = 0.0
        return 1.0, 0, 0
    else --calculate normal of collided surface
        if xEntry > yEntry then
            if xInvEntry < 0.0 then
                normalx = 1.0
                normaly = 0.0
	        else
                normalx = -1.0
                normaly = 0.0
            end
        else
            if yInvEntry < 0.0 then
                normalx = 0.0
                normaly = 1.0
	        else
                normalx = 0.0
		        normaly = -1.0
            end
        end
        return entryTime, normalx, normaly
    end
end

function Clamp(upper, lower, value)
    if value < lower then
        return lower
    elseif value > upper then
        return upper
    end

    return value
end

function Distance(x1, x2, y1, y2)
    local x = math.pow((x2 - x1), 2)
    local y = math.pow((y2 - y1), 2)
    return math.sqrt( (x + y) )
end

function Normalize(x, y)
    local length = math.sqrt(x * x + y * y)
    local xN = x / length
    local yN = y / length

    return xN, yN
end