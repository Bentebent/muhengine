function Normalize(x, y)
    local length = math.sqrt(x * x + y * y)
    local xN = x / length
    local yN = y / length

    return xN, yN
end

function CreatePlayer()
    local player = CreateLevelEntity()
    AddRenderingComponent(player, {renderMe = true, bitmask = shapeBitmask, color = {0, 0, 0, 0}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
    AddTransformationComponent(player, {rotation = 0, position = {320, 240}, scale = {1, 1}})
    AddShapeComponent(player, {shapeType=3}, {pointCount = 4, convexPoints={{0,-10}, {5,10}, {0,5}, {-5, 10}}, outlineThickness=1, outlineColor={255, 255, 255, 255}})
    return player
end

fontid1    = LoadFont("content/fonts/upheavtt.ttf")

--Text test
textBitmask = uint64(0)
textBitmask = SetBitmaskValue(textBitmask, "COMMAND", 0)
textBitmask = SetBitmaskValue(textBitmask, "LAYER", 0)
textBitmask = SetBitmaskValue(textBitmask, "TYPE", 2)
textBitmask = SetBitmaskValue(textBitmask, "TEXTURE", fontid1)

scoreText = CreateLevelEntity()
AddRenderingComponent(scoreText, {renderMe = true, bitmask = textBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(scoreText, {rotation = 0, position = {10, 0}, scale = {1, 1}})
AddTextComponent(scoreText, {text="Score:", alignment=0, characterSize=20, fontID=fontid1, textBox={640, 480}, cacheMe=true})

scoreText = CreateLevelEntity()
AddRenderingComponent(scoreText, {renderMe = true, bitmask = textBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(scoreText, {rotation = 0, position = {90, 0}, scale = {1, 1}})
AddTextComponent(scoreText, {text="0", alignment=0, characterSize=20, fontID=fontid1, textBox={640, 480}, cacheMe=true})

score = 0

startText = CreateLevelEntity()
AddRenderingComponent(startText, {renderMe = true, bitmask = textBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(startText, {rotation = 0, position = {190, 200}, scale = {1, 1}})
AddTextComponent(startText, {text="PRESS BACKSPACE TO START", alignment=0, characterSize=20, fontID=fontid1, textBox={640, 480}, cacheMe=true})

--Shape bitmask
shapeBitmask = uint64(0)
shapeBitmask = SetBitmaskValue(shapeBitmask, "COMMAND", 0)
shapeBitmask = SetBitmaskValue(shapeBitmask, "LAYER", 0)
shapeBitmask = SetBitmaskValue(shapeBitmask, "TYPE", 3)

--Player
player = nil-- CreatePlayer()
playerRotationSpeed = 150
playerSpeed = {x=0, y=0}
playerMaxSpeed = 1
playerAcceleration = 1
playerDeccelerationFactor = 0.25
playerDirection = { x=0, y=-1}
playerLives = 5
livesGUI = {}

--lives should be in gui
lifeBitmask = SetBitmaskValue(shapeBitmask, 4, 0)

for i=1, 5 do
    local life = CreateLevelEntity()
    AddRenderingComponent(life, {renderMe = true, bitmask = lifeBitmask, color = {0, 0, 0, 0}, origin={12, 12}, sourceRectangle={0,0,0,0}})
    AddTransformationComponent(life, {rotation = 0, position = { i * 20 + 10, 50}, scale = {1, 1}})
    AddShapeComponent(life, {shapeType=3}, {pointCount = 4, convexPoints={{0,-10}, {5,10}, {0,5}, {-5, 10}}, outlineThickness=1, outlineColor={255, 255, 255, 255}})
    table.insert(livesGUI, life)
end

--asteroids
asteroids = {}
asteroidMaxSpeed = 100
asteroidMinSpeed = 25
asteroidRotationMax = 100
asteroidRotationMin = 30
asteroidCount = 10

for i=0, asteroidCount do
    local asteroid = {}
   
    asteroid.lives = 2
    asteroid.radius = 20

    local dirX = math.random() + 0.1
    local dirY = math.random() + 0.1
    
    if math.random(0, 10) < 5 then
         dirX = dirX * -1
    end
       
    if math.random(0, 10) < 5 then
         dirY = dirY * -1
    end
       
    dirX, dirY = Normalize(dirX, dirY)
    
    asteroid.dirX = dirX
    asteroid.dirY = dirY

    asteroid.speed = math.random(asteroidMinSpeed, asteroidMaxSpeed)
    asteroid.rotationSpeed = math.random(asteroidRotationMin, asteroidRotationMax)

    if math.random(0, 10) < 5 then
         asteroid.rotationSpeed = asteroid.rotationSpeed * -1
    end

    local posX, posY = 320

    while posX > 100 and posX < 440 do
        posX = math.random(0, 640)
        posY = math.random(0, 480)
    end

    while posY > 100 and posY < 380 do
        posY = math.random(0, 480)
    end

    asteroid.entity = CreateLevelEntity()
    AddRenderingComponent(asteroid.entity, {renderMe = true, bitmask = shapeBitmask, color = {0, 0, 0, 0}, origin={15, -10}, sourceRectangle={0,0,0,0}})
    AddTransformationComponent(asteroid.entity, {rotation = 0, position = { posX, posY}, scale = {1, 1}})
    AddShapeComponent(asteroid.entity, {shapeType=3}, {pointCount = 7, convexPoints={{0, -30}, {30,-30}, {47,-15}, {30,-10}, {30,15}, {15,5}, {-10, 20}}, outlineThickness=1, outlineColor={255, 255, 255, 255}})
   
    --asteroid.collisionSphere = CreateLevelEntity()
    --AddRenderingComponent(asteroid.collisionSphere, {renderMe = true, bitmask = shapeBitmask, color = {0, 0, 0, 0}, origin={20, 20}, sourceRectangle={0,0,0,0}})  
    --AddTransformationComponent(asteroid.collisionSphere, {rotation = 0, position = { posX, posY}, scale = {1, 1}})
    --AddShapeComponent(asteroid.collisionSphere, {shapeType=0}, {radius=20, outlineThickness=1, outlineColor={255, 255, 255, 255}})

    table.insert(asteroids, asteroid)
end

bullets = {}
bulletSpeed = 500

function Update(dt, owner)
    if player then
        ControlPlayer(dt)
    else
        if SingleButtonPress("BackSpace") and playerLives > 0 then
            print(playerLives)
            player = CreatePlayer()
            playerSpeed = {x=0, y=0}
            SetRenderMe(startText, false)
        end
    end

    UpdateBullets(dt)
    MoveAsteroids(dt)

    if player then
        PlayerCollision(dt)
    end
end

function PlayerCollision(dt)
    local playerPos = GetPosition(player)
    for asteroidKey, asteroid in pairs(asteroids) do
        local asteroidPos = GetPosition(asteroid.entity)
        local result = SphereCollision(playerPos[0], playerPos[1], 10, asteroidPos[0], asteroidPos[1], asteroid.radius)

        if result then
            
            local head = table.remove(livesGUI, 1)
            DestroyEntity(head)
            head = nil
            DestroyEntity(player)
            playerLives = playerLives - 1
            player = nil
            SetRenderMe(startText, true)
            break
        end

    end
end

function CreateBullet(parentPosition, dirX, dirY)
    local bullet = {}
    bullet.entity = CreateLevelEntity()
    bullet.dirX = dirX
    bullet.dirY = dirY

    local creationOffset = 20

    AddRenderingComponent(bullet.entity, {renderMe = true, bitmask = shapeBitmask, color = {255, 255, 255, 255}, origin={1, 1}, sourceRectangle={0,0,0,0}})  
    AddTransformationComponent(bullet.entity, {rotation = 0, position = { parentPosition[0] + creationOffset * dirX, parentPosition[1] + creationOffset * dirY}, scale = {1, 1}})
    AddShapeComponent(bullet.entity, {shapeType=0}, {radius=1, outlineThickness=1, outlineColor={255, 255, 255, 255}})

    table.insert(bullets, bullet)

end

function CreateAsteroid(position, lives)

    if lives <= 0 then
        do return end
    end
    local asteroid = {}
   
    asteroid.lives = lives
    local dirX = math.random() + 0.1
    local dirY = math.random() + 0.1
    
    if math.random(0, 10) < 5 then
         dirX = dirX * -1
    end
       
    if math.random(0, 10) < 5 then
         dirY = dirY * -1
    end
       
    dirX, dirY = Normalize(dirX, dirY)
    
    asteroid.dirX = dirX
    asteroid.dirY = dirY

    asteroid.speed = math.random(asteroidMinSpeed, asteroidMaxSpeed)
    asteroid.rotationSpeed = math.random(asteroidRotationMin, asteroidRotationMax)

    if math.random(0, 10) < 5 then
         asteroid.rotationSpeed = asteroid.rotationSpeed * -1
    end

    local posX = position[0]
    local posY = position[1]

    asteroid.entity = CreateLevelEntity() 
    AddTransformationComponent(asteroid.entity, {rotation = 0, position = { posX, posY}, scale = {1, 1}})

    if lives == 1 then
        asteroid.radius = 10
        AddRenderingComponent(asteroid.entity, {renderMe = true, bitmask = shapeBitmask, color = {0, 0, 0, 0}, origin={-5, 0}, sourceRectangle={0,0,0,0}})  
        AddShapeComponent(asteroid.entity, {shapeType=3}, {pointCount = 7, convexPoints={{-10, 0}, {10,0}, {0,-10}, {-10,-10}, {-20,0}, {-25, 5}, {-20, 10}}, outlineThickness=1, outlineColor={255, 255, 255, 255}})
    end

    table.insert(asteroids, asteroid)
end

function UpdateBullets(dt)

    local removeList = {}

    for bulletKey, bullet in pairs(bullets) do
        local pos = GetPosition(bullet.entity)
        local outOfBounds = false
        pos[0] = pos[0] + bulletSpeed * bullet.dirX * dt
        pos[1] = pos[1] + bulletSpeed * bullet.dirY * dt

        SetPosition(bullet.entity, pos)

         if pos[0] < -20 then
            outOfBounds = true
        elseif pos[0] > 660 then
            outOfBounds = true
        end

        if pos[1] < -20 then
            outOfBounds = true
        elseif pos[1] > 500 then
            outOfBounds = true
        end

        for asteroidKey, asteroid in pairs(asteroids) do
            local asteroidPos = GetPosition(asteroid.entity)
            local result = SphereCollision(pos[0], pos[1], 1, asteroidPos[0], asteroidPos[1], asteroid.radius)

            if result then
                local removeItem = {}
                removeItem.bullet = bulletKey
                removeItem.asteroid = asteroidKey 
                table.insert(removeList, removeItem)
            end
        end

        if outOfBounds then
            local removeItem = {}
            removeItem.bullet = bulletKey
            removeItem.asteroid = nil 
            table.insert(removeList, removeItem)
        end
    end

    for key, removeItem in pairs(removeList) do

        if bullets[removeItem.bullet] then
            DestroyEntity(bullets[removeItem.bullet].entity)
            bullets[removeItem.bullet] = nil
        end

        if removeItem.asteroid then
            if asteroids[removeItem.asteroid] then
                score = score + 100 * asteroids[removeItem.asteroid].lives
                local newLife = asteroids[removeItem.asteroid].lives - 1
                CreateAsteroid(GetPosition(asteroids[removeItem.asteroid].entity), newLife)
                CreateAsteroid(GetPosition(asteroids[removeItem.asteroid].entity), newLife)
                DestroyEntity(asteroids[removeItem.asteroid].entity)
                asteroids[removeItem.asteroid] = nil
                SetText(scoreText, score)
            end
        end
    end
end

function MoveAsteroids(dt)
    for key, asteroid in pairs(asteroids) do

        local pos = GetPosition(asteroid.entity)
        local rotation = GetRotation(asteroid.entity)

        pos[0] = pos[0] + asteroid.speed * asteroid.dirX * dt
        pos[1] = pos[1] + asteroid.speed * asteroid.dirY * dt

        rotation = rotation + asteroid.rotationSpeed * dt

        MoveBack(pos)

        SetPosition(asteroid.entity, pos)
        --SetPosition(asteroid.collisionSphere, pos)
        SetRotation(asteroid.entity, rotation)
       
    end

end

function ControlPlayer(dt, owner)
    
    local rotation = GetRotation(player)
    local position = GetPosition(player)

    if ButtonPressed("Right") then
        rotation = rotation + playerRotationSpeed * dt
    elseif ButtonPressed("Left") then
        rotation = rotation - playerRotationSpeed * dt
    end

    local rotationRad = math.rad(rotation)

    local dirX = math.cos(rotationRad) * playerDirection.x - math.sin(rotationRad) * playerDirection.y
    local dirY = math.sin(rotationRad) * playerDirection.x + math.cos(rotationRad) * playerDirection.y

    dirX, dirY = Normalize(dirX, dirY)

    if ButtonPressed("Up") then
        playerSpeed.x = playerSpeed.x + dirX * playerAcceleration * dt
        playerSpeed.y = playerSpeed.y + dirY * playerAcceleration * dt
    else

        if dirX > 0 and playerSpeed.x > 0 then
            playerSpeed.x = playerSpeed.x - playerAcceleration * dt * playerDeccelerationFactor
            playerSpeed.x = Clamp(playerSpeed.x, 0, playerSpeed.x) 
        elseif dirX < 0 and playerSpeed.x < 0 then
            playerSpeed.x = playerSpeed.x + playerAcceleration * dt * playerDeccelerationFactor
            playerSpeed.x = Clamp(0, playerSpeed.x, playerSpeed.x) 
        end

        if dirY > 0 and playerSpeed.y > 0 then
            playerSpeed.y = playerSpeed.y - playerAcceleration * dt * playerDeccelerationFactor
            playerSpeed.y = Clamp(playerSpeed.y, 0, playerSpeed.y) 
        elseif dirY < 0 and playerSpeed.y < 0 then
            playerSpeed.y = playerSpeed.y + playerAcceleration * dt * playerDeccelerationFactor
            playerSpeed.y = Clamp(0, playerSpeed.y, playerSpeed.y)
        end
    end

    playerSpeed.x = Clamp(playerMaxSpeed, -playerMaxSpeed, playerSpeed.x)
    playerSpeed.y = Clamp(playerMaxSpeed, -playerMaxSpeed, playerSpeed.y)

    position[0] = position[0] + playerSpeed.x 
    position[1] = position[1] + playerSpeed.y 

    MoveBack(position)

    SetRotation(player, rotation)
    SetPosition(player, position)

    if SingleButtonPress("Space") then
       CreateBullet(position, dirX, dirY)
    end
end

function MoveBack(position)
    if position[0] < -20 then
        position[0] = 659
    elseif position[0] > 660 then
        position[0] = -19
    end

     if position[1] < -20 then
        position[1] = 499
    elseif position[1] > 500 then
        position[1] = -19
    end
end

function BoxCollision(x1, y1, w1, h1, x2, y2, w2, h2)
    return math.abs(x1 - x2) * 2 < (w1 + w2) and math.abs(y1 - y2) * 2 < (h1 + h2)
end

function SphereCollision(x1, y1, r1, x2, y2, r2)
    local dist = Distance(x1, x2, y1, y2)
    local minDist = r1 + r2

    return dist < minDist
end

function SweptAABB(x1, y1, w1, h1, vx1, vy1, x2, y2, w2, h2)
    local xInvEntry = 0
    local yInvEntry = 0
    local xInvExit = 0 
    local yInvExit = 0

    if vx1 > 0 then
        xInvEntry = x2 - (x1 + w1)
        xInvExit = (x2 + w2) - x1
    else
        xInvEntry = (x2 + w2) - x1
        xInvExit = x2 - (x1 + w1)
    end

    if vy1 > 0 then
        yInvEntry = y2 - (y1 + h1)
        yInvExit = (y2 + h2) - y1
    else
        yInvEntry = (y2 + h2) - y1
        yInvExit = y2 - (y1 + h1)
    end

    --find time of collision and time of leaving for each axis (if statement is to prevent divide by zero)
    local xEntry = 0
    local yEntry = 0
    local xExit = 0
    local yExit = 0

    if vx1 == 0.0 then
        xEntry = -5e+20
        xExit = 5e+20
    else
        xEntry = xInvEntry / vx1
        xExit = xInvExit / vx1
    end

    if vy1 == 0.0 then
        yEntry = -5e+20
        yExit = 5e+20
    else
        yEntry = yInvEntry / vy1
        yExit = yInvExit / vy1
    end

    --find the earliest/latest times of collision
    local entryTime = math.max(xEntry, yEntry)
    local exitTime = math.min(xExit, yExit)

    local normalx = 0
    local normaly = 0

    --if there was no collision
    if entryTime > exitTime or xEntry < 0.0 and yEntry < 0.0 or xEntry > 1.0 or yEntry > 1.0 then
        normalx = 0.0
        normaly = 0.0
        return 1.0, 0, 0
    else --calculate normal of collided surface
        if xEntry > yEntry then
            if xInvEntry < 0.0 then
                normalx = 1.0
                normaly = 0.0
	        else
                normalx = -1.0
                normaly = 0.0
            end
        else
            if yInvEntry < 0.0 then
                normalx = 0.0
                normaly = 1.0
	        else
                normalx = 0.0
		        normaly = -1.0
            end
        end
        return entryTime, normalx, normaly
    end
end

function Clamp(upper, lower, value)
    if value < lower then
        return lower
    elseif value > upper then
        return upper
    end

    return value
end

function Distance(x1, x2, y1, y2)
    local x = math.pow((x2 - x1), 2)
    local y = math.pow((y2 - y1), 2)
    return math.sqrt( (x + y) )
end

