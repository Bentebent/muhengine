dir = 1
speed = 500
flipped = true
function Update(dt, owner)
    pos = GetPosition(owner)
    
    --if pos[0] > 3200 or pos[0] < 0 then
    --    dir = dir * -1
    --end
    --
    --if pos[0] > 3200 then
    --    SetFlippedX(owner, true)
    --end
    --
    --if pos[0] < 0 then
    --    SetFlippedX(owner, false)
    --end

    if ButtonPressed("Right") then
        pos[0] = pos[0] + speed * dir * dt
         SetFlippedX(owner, false)
    end

     if ButtonPressed("Left") then
        pos[0] = pos[0] + speed * -dir * dt
        SetFlippedX(owner, true)
    end

     if ButtonPressed("Down") then
        pos[1] = pos[1] + speed * dir * dt
    end

     if ButtonPressed("Up") then
        pos[1] = pos[1] + speed * -dir * dt
    end

    --print(pos[0])

    SetViewPosition(1, pos[0], pos[1])
    SetPosition(owner, pos)
end