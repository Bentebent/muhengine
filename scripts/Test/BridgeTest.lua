SetView(0, 320, 240, 640, 480, 0, 0, 1, 1)
SetView(1, 0, 240, 640, 480, 0, 0, 1, 1)
SetPrimaryView(1)

textureid0 = LoadTexture("content/textures/344.jpg")  
textureid1 = LoadTexture("content/textures/aggydaggy.png")  
fontid0    = LoadFont("content/fonts/arial.ttf")
fontid1    = LoadFont("content/fonts/ARCADECLASSIC.ttf")
textureid2 = LoadAnimation("content/animations/sheetTest.anim")

myBitmask = uint64(0)
myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 1)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", textureid1)
myBitmask = SetBitmaskValue(myBitmask, "VIEW", 1)

--Sprite test
for x = 0, 1 do
    for y = 0, 1 do
        myEntity = CreateLevelEntity()
        AddTransformationComponent(myEntity, {rotation = 0, position = {160 + 320 * x, 120 + 240 * y}, scale = {0.5, 0.5}})
        AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={250, 250}, sourceRectangle={0,0,500,500}}) 
    end
end

--Animated sprite test
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 0)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", textureid2)
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={101, 136}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {0, 240}, scale = {1, 1}})
AddScriptComponent(myEntity, {priority = 1, functionName="Update", callingFunction=1, file="scripts/Test/AnimationTest.lua"})
AddAnimationComponent(myEntity)
PlayAnimation(myEntity, "greyguy_walk", true, false)

--Script test 
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", textureid0)
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={441, 244}, sourceRectangle={0,0,882,488}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {320, 240}, scale = {0.3, 0.3}})
AddScriptComponent(myEntity, {priority = 0, functionName="Update", callingFunction=1, file="scripts/Test/APITest.lua"})


--Text test
myBitmask = uint64(0)
myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 0)
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 2)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", fontid1)
myBitmask = SetBitmaskValue(myBitmask, "VIEW", 0)


myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={0, 0}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {0, 0}, scale = {1, 1}})
AddTextComponent(myEntity, {text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", alignment=0, characterSize=12, fontID=fontid1, textBox={640, 480}, cacheMe=true})


--Shape bitmask
myBitmask = uint64(0)
myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 0)
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 3)
myBitmask = SetBitmaskValue(myBitmask, "VIEW", 1)

--textured shapes
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 4)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", textureid1)

--Circle shape test
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={100, 100}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {100, 100}, scale = {1, 1}})
AddShapeComponent(myEntity, {shapeType=0}, {radius=100, outlineThickness=10, outlineColor={0, 255, 255, 255}})

--Rectangle shape test
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 0, 255, 255}, origin={50, 50}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {200, 200}, scale = {1, 1}})
AddShapeComponent(myEntity, {shapeType=1}, {rectangleSize={50,50}, outlineThickness=10, outlineColor={0, 0, 255, 255}})

--Polygon shape test
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {0, 0, 255, 255}, origin={50, 50}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {400, 400}, scale = {1, 1}})
AddShapeComponent(myEntity, {shapeType=2}, {pointCount = 3, radius = 50, outlineThickness=10, outlineColor={0, 255, 0, 255}})

--Convex shape test
myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 149, 149, 41}, origin={50, 50}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {300, 300}, scale = {1, 1}})
AddShapeComponent(myEntity, {shapeType=3}, {pointCount = 5, outlineThickness=10, outlineColor={0, 0, 0, 255}, convexPoints={{10,11}, {150,10}, {120, 90}, {30, 100}, {0, 50}}})




