dirX = 1
dirY = 1
scaleDir = 1
colorDir = 1

function Update(dt, owner)
    pos = GetPosition(owner)
    scale = GetScale(owner)
    rotation = GetRotation(owner)
    color = GetColour(owner)

    pos[0] = pos[0] + 1 * dirX
    if pos[0] > 530 or pos[0] < 110 then
        dirX = dirX * -1
    end

    pos[1] = pos[1] + 1 * dirY
    if pos[1] > 400 or pos[1] < 110 then
        dirY = dirY * -1
    end

    color[0] = color[0] + math.random(1, 2) * colorDir
    color[1] = color[1] + math.random(1, 2) * colorDir
    color[2] = color[2] + math.random(1, 2) * colorDir
    color[3] = color[3] + math.random(1, 2) * colorDir
    if color[3] > 255 or color[3] < 0 then
        colorDir = colorDir * -1
    end
    
    scale[0] = scale[0] + 0.001 * scaleDir
    scale[1] = scale[1] + 0.001 * scaleDir
    if scale[0] > 1 or scale[0] < 0.2 then
        scaleDir = scaleDir * -1
    end
   
    rotation = rotation + 0.5
    
    SetPosition(owner, pos)
    SetScale(owner, scale)
    SetRotation(owner, rotation)
    SetColour(owner, color)
   

    if SingleButtonPress("A") then
        print("A has been pressed.")
    end

    if ButtonPressed("B") then
        print("B is being held down.")
    end

    --debug test
    RenderDebugText(320, 240, "Hello Debug!", 12, 255, 255, 255, 255)

   
end