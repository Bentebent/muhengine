SetView(0, 320, 240, 320, 240, 0, 0, 1, 1)
SetPrimaryView(0)


--LoadMap("content/maps/mockup/test.tmx")

LoadMap("content/maps/mockup/mixels.tmx")
--LoadMap("content/maps/mockup/fliptest.tmx")
--LoadMap("content/maps/rewritemap/rewrite.tmx")

jackAnimationsTexture = LoadAnimation("content/animations/jack_basic.anim")

myBitmask = uint64(0)
myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
myBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
myBitmask = SetBitmaskValue(myBitmask, "LAYER", 5)
myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", jackAnimationsTexture)
myBitmask = SetBitmaskValue(myBitmask, "VIEW", 0)

scaledSize = GetScaledTileSize()
x = 56 * scaledSize[0]
y = 52 * scaledSize[1]

myEntity = CreateLevelEntity()
AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={8, 12}, sourceRectangle={0,0,0,0}})  
AddTransformationComponent(myEntity, {rotation = 0, position = {x, y}, scale = {1.0, 1.0}})
AddScriptComponent(myEntity, {priority = 1, functionName="Update", callingFunction=1, file="scripts/mockup/jack.lua"})
AddCollisionComponent(myEntity, {collisionType="rectangle", offset={0.5, 8.5}, size={10.0, 4.0}})
AddAnimationComponent(myEntity)
PlayAnimation(myEntity, "jack_idle_down", true, false)

--for key,value in pairs(test) do print(key,value) end


--myEntity = CreateLevelEntity()
--AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={8, 12}, sourceRectangle={0,0,0,0}})  
--AddTransformationComponent(myEntity, {rotation = 0, position = {2850, 240}, scale = {3, 3}})
--AddAnimationComponent(myEntity)
--PlayAnimation(myEntity, "jack_idle_down", true, false)

--local entity = loadfile("scripts/entities/flagpole.lua")
--entity()
--LoadEntity(78, 29, 5)
