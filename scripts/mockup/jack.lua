walkSpeed = 75
moveDir = -1

function Update(dt, owner)
    Movement(dt, owner)
end

function Movement(dt, owner)
        pos = GetPosition(owner)

    if ButtonPressed("Up") then

        local newPos = {};
        newPos[0] =  pos[0];
        newPos[1] = pos[1] - walkSpeed * dt;

        local col = MapCollision(owner, 0, -1, newPos);
        
        if col == 0 then
            pos[1] = pos[1] - walkSpeed * dt;
        else
            pos = SlideMe(owner, col, pos, dt)
        end

        if not IsAnimationPlaying(owner, "jack_walk_up") then
            PlayAnimation(owner, "jack_walk_up", true, false) 
        end
        moveDir = 0

    elseif ButtonPressed("Down") then
        local newPos = {};
        newPos[0] =  pos[0];
        newPos[1] = pos[1] + walkSpeed * dt;

        local col = MapCollision(owner, 0, 1, newPos);
        
        if col == 0 then
            pos[1] = pos[1] + walkSpeed * dt;
        else
            pos = SlideMe(owner, col, pos, dt)
        end

        if not IsAnimationPlaying(owner, "jack_walk_down") then
            PlayAnimation(owner, "jack_walk_down", true, false) 
        end

        moveDir = 1

    elseif ButtonPressed("Left") then

        local newPos = {};
        newPos[0] =  pos[0] - walkSpeed * dt;
        newPos[1] = pos[1];

        local col = MapCollision(owner, -1, 0, newPos);
        
        if col == 0 then
            pos[0] = pos[0] - walkSpeed * dt;
        else
            pos = SlideMe(owner, col, pos, dt)
        end

        if not IsAnimationPlaying(owner, "jack_walk_left") then
            PlayAnimation(owner, "jack_walk_left", true, false) 
        end

        SetFlippedX(owner, false)
        moveDir = 2

    elseif ButtonPressed("Right") then
        local newPos = {};
        newPos[0] = pos[0] + walkSpeed * dt;
        newPos[1] = pos[1];
        
        local col = MapCollision(owner, 1, 0, newPos);
        
        if col  == 0 then
            pos[0] = pos[0] + walkSpeed * dt;
        else
            pos = SlideMe(owner, col, pos, dt)
        end

        if not IsAnimationPlaying(owner, "jack_walk_left") then
            PlayAnimation(owner, "jack_walk_left", true, false) 
        end
        SetFlippedX(owner, true)
        moveDir = 3
    else
        if moveDir == 0 then
            PlayAnimation(owner, "jack_idle_up", true, false)
        elseif moveDir == 1 then
            PlayAnimation(owner, "jack_idle_down", true, false)
        elseif moveDir == 2 then
            PlayAnimation(owner, "jack_idle_left", true, false)
        elseif moveDir == 3 then
            PlayAnimation(owner, "jack_idle_left", true, false)
        end
    end
    SetPosition(owner, pos)
    SetViewPosition(0, pos[0], pos[1], true)

    offset = GetCollisionOffset(owner)
    size = GetCollisionSize(owner)

	--test = GetPrimaryView()
	--test = SFView:new()
	--print(test:getSize()[0])
	--print(test:getCenter()[0])
	--print(test)

    --RenderDebugRectangle(pos[0] + offset[0] - size[0] / 2, pos[1] + offset[1] - size[1] / 2, size[0], size[1], 255, 255, 255, 100) 
end

function SlideMe(owner, col, pos, dt)

    if col == 2 then
        local newPos = {};
        newPos[0] =  pos[0] - walkSpeed * dt;
        newPos[1] = pos[1];

        local newCol = MapCollision(owner, -1, 0, newPos);

        if newCol == 0 then
            return newPos
        end
    elseif col == 3 then
        local newPos = {};
        newPos[0] =  pos[0] + walkSpeed * dt;
        newPos[1] = pos[1];

        local newCol = MapCollision(owner, 1, 0, newPos);

        if newCol == 0 then
            return newPos
        end

    elseif col == 4 then
        local newPos = {};
        newPos[0] =  pos[0];
        newPos[1] = pos[1] + walkSpeed * dt;

        local newCol = MapCollision(owner, 0, 1, newPos);

        if newCol == 0 then
            return newPos
        end

    elseif col == 5 then
        local newPos = {};
        newPos[0] =  pos[0];
        newPos[1] = pos[1] - walkSpeed * dt;

        local newCol = MapCollision(owner, 0, -1, newPos);

        if newCol == 0 then
            return newPos
        end
    else
        return pos
    end

    return pos
end