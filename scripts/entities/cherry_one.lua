function LoadEntity(tileX, tileY, layer)

    texture = LoadTexture("content/textures/cherry_one.png")

    myBitmask = uint64(0)
    myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
    myBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
    myBitmask = SetBitmaskValue(myBitmask, "LAYER", layer)
    myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", texture)
    myBitmask = SetBitmaskValue(myBitmask, "VIEW", 0)
    
    scaledSize = GetScaledTileSize()
    x = tileX * scaledSize[0]-- + scaledSize[0] / 2.0
    y = tileY * scaledSize[1]-- + scaledSize[1]

    print(tileX, tileY)

    myEntity = CreateLevelEntity()
    AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={22, 58}, sourceRectangle={0,0,45,58}})  
    AddTransformationComponent(myEntity, {rotation = 0, position = {x, y}, scale = {1.0, 1.0}})
    SetCollision(tileX * 2 * scaledSize[0] / 2.0 - scaledSize[0] / 2, tileY * 2 * scaledSize[1] / 2.0 - scaledSize[1] / 2, 8, 8, true)
end