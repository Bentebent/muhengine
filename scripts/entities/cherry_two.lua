function LoadEntity(tileX, tileY, layer)
    texture = LoadTexture("content/textures/cherry_two.png")

    myBitmask = uint64(0)
    myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
    myBitmask = SetBitmaskValue(myBitmask, "TYPE", 5)
    myBitmask = SetBitmaskValue(myBitmask, "LAYER", layer)
    myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", texture)
    myBitmask = SetBitmaskValue(myBitmask, "VIEW", 0)

    scaledSize = GetScaledTileSize()
    x = tileX * scaledSize[0]-- + scaledSize[0] / 2
    y = tileY * scaledSize[1]-- + scaledSize[1]

    myEntity = CreateLevelEntity()
    AddRenderingComponent(myEntity, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={27, 52}, sourceRectangle={0,0,54,57}})  
    AddTransformationComponent(myEntity, {rotation = 0, position = {x, y}, scale = {1, 1}})

     SetCollision(tileX * 2 * scaledSize[0] / 2.0 - scaledSize[0] / 2.0, tileY * 2 * scaledSize[1] / 2.0, 8, 8, true)
     SetCollision(tileX * 2 * scaledSize[0] / 2.0, tileY * 2 * scaledSize[1] / 2.0, 8, 8, true)
     SetCollision(tileX * 2 * scaledSize[0] / 2.0 + scaledSize[0] / 2.0, tileY * 2 * scaledSize[1] / 2.0, 8, 8, true)
end