--I am a local variable
b = "I am a local variable"
c = 0;
print("B has been loaded")

myBool = true
dir = 1
scaleDir = 1
colorDir = 1
rotation = 0

function doit()
    --print( "Script B" )
	--print(a)
	--print(b)
	--print(c)

	e = CreateEntity()
	AddTestComponent(e, { a=1337, b=7331, foo={9000, 9001, 9002} })
	SetFoo(e, {1, 2, 3})

    if myBool then
       textureid = LoadTexture("content/textures/344.jpg")       
       myBitmask = uint64(0)
       myBitmask = SetBitmaskValue(myBitmask, "COMMAND", 0)
       myBitmask = SetBitmaskValue(myBitmask, "LAYER", 5)
       myBitmask = SetBitmaskValue(myBitmask, "TYPE", 0)
       myBitmask = SetBitmaskValue(myBitmask, "TEXTURE", textureid)
       AddRenderingComponent(4, {renderMe = true, bitmask = myBitmask, color = {255, 255, 255, 255}, origin={441, 244}, sourceRectangle={0,0,882,488}})  
       AddTransformationComponent(4, {rotation = 0, position = {320, 240}, scale = {0.3, 0.3}})
    end
    
    pos = GetPosition(4)
    pos[0] = pos[0] + 1 * dir
    if pos[0] > 530 or pos[0] < 110 then
        dir = dir * -1
    end
    color = GetColour(4)
    

    color[0] = color[0] + math.random(1, 2) * colorDir
    color[1] = color[1] + math.random(1, 2) * colorDir
    color[2] = color[2] + math.random(1, 2) * colorDir
    color[3] = color[3] + math.random(1, 2) * colorDir
    if color[3] > 255 or color[3] < 0 then
        colorDir = colorDir * -1
    end
   

    scale = GetScale(4)

    scale[0] = scale[0] + 0.001 * scaleDir
    scale[1] = scale[1] + 0.001 * scaleDir

    if scale[0] > 1 or scale[0] < 0.2 then
        scaleDir = scaleDir * -1
    end


    rotation = rotation + 0.5
    
    SetPosition(4, pos)
    SetScale(4, scale)
    SetRotation(4, rotation)
    SetColour(4, color)

    myBool = false;
	--foo = GetFoo(e)
	--sleep(1000000);

	--AddComponent(e, "TestComponent")
    --textureid = LoadTexture("mypath")

    --myBitmask = SetBitmaskValue(myBitmask, BitmaskType, value)
    --AddScriptComponent(myEntity, {priorities = {0, 1, 2, 3}, functionNames={"doit", "update", "foo", "hurf"}, callingFunctions={0, 1, 0, 0}, files={"scriptB", "scriptA", "myScript", "haha"}})

    --AddScriptComponent(myEntity, {priority = 0, functionName="doit", callingFunction=0, file="scriptB"}, 
    --{priority = 0, functionName="doit", callingFunction=0, file="scriptB"}, {priority = 0, functionName="doit", callingFunction=0, file="scriptB"})

	--DestroyEntity(e)
	--e.CreateEntity()
end